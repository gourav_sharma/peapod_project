package com.PeapodPackage.peapod;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import se.tonyivanov.android.ajax.Request;
import se.tonyivanov.android.ajax.Transport;

import com.Peapod.Connections.ImageLoader;
import com.Peapod.Connections.StorePref;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

public class UserProfileScreen extends Activity implements OnClickListener {

	private ImageButton doneBtn;
	private ImageView imgForConnect, pass, pea_image, view_profile_img;
	private ImageView firstImg, secondImg, thirdImg, forthImg;
	String image, name, userID, bio;
	ImageLoader imgLoader;
	double dist;
	String activeTime;
	TextView nameTxt;
	Boolean status = false;
	TextView txtForResult;
	Boolean imgStatus = false;
	TextView txtForActiveTime, txtForBio, txtForDistance;
	StorePref pref;
	String friendId;
	int correctAns = 0;
	private ArrayList<String> listForOPtion1 = new ArrayList<String>();
	private ArrayList<String> listForOPtion2 = new ArrayList<String>();
	private ArrayList<String> listForAnswerStatus = new ArrayList<String>();
	private ArrayList<String> listForAnswer = new ArrayList<String>();

	private ArrayList<String> listForUserImages = new ArrayList<String>();
	private ArrayList<String> listForUserImagesId = new ArrayList<String>();

	private ArrayList<String> listForShowUserImages = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.user_profile_screen);

		doneBtn = (ImageButton) findViewById(R.id.done_btn);
		doneBtn.setOnClickListener(this);
		view_profile_img = (ImageView) findViewById(R.id.view_profile_img);
		view_profile_img.setOnClickListener(this);
		imgForConnect = (ImageView) findViewById(R.id.imgForconnect);
		imgForConnect.setOnClickListener(this);
		pass = (ImageView) findViewById(R.id.pass);
		pass.setOnClickListener(this);
		pref = StorePref.getInstance(getApplicationContext());
		userID = pref.getUserid();
		friendId = getIntent().getStringExtra("keyForFriendId");
		nameTxt = (TextView) findViewById(R.id.txt1);
		txtForActiveTime = (TextView) findViewById(R.id.txtForActiveTime);
		txtForBio = (TextView) findViewById(R.id.txtForBio);
		txtForDistance = (TextView) findViewById(R.id.txtDistance);
		imgLoader = new ImageLoader(getApplicationContext());
		pea_image = (ImageView) findViewById(R.id.pea_image);
		firstImg = (ImageView) findViewById(R.id.firstImage);
		secondImg = (ImageView) findViewById(R.id.secondImage);
		thirdImg = (ImageView) findViewById(R.id.thirdImage);
		forthImg = (ImageView) findViewById(R.id.forthImage);
		image = getIntent().getStringExtra("keyForImage");
		name = getIntent().getStringExtra("keyForName");
		bio = getIntent().getStringExtra("keyForBio");
		dist = getIntent().getDoubleExtra("keyForDist", 0);
		activeTime = getIntent().getStringExtra("keyForActiveTime");
		txtForDistance.setText("" + dist + " miles");
		txtForActiveTime.setText("In the pod " + activeTime);

		listForUserImages = getIntent().getStringArrayListExtra(
				"keyForListImages");
		listForUserImagesId = getIntent().getStringArrayListExtra(
				"keyForListImagesId");

		for (int i = 0; i < listForUserImagesId.size(); i++) {
			if (friendId.equals(listForUserImagesId.get(i))) {
				listForShowUserImages.add(listForUserImages.get(i));
				imgStatus = true;
			}
		}

		if (imgStatus == true) {
			imgLoader.DisplayImage(listForShowUserImages.get(0), firstImg);
			imgLoader.DisplayImage(listForShowUserImages.get(1), secondImg);
			imgLoader.DisplayImage(listForShowUserImages.get(2), thirdImg);
			imgLoader.DisplayImage(listForShowUserImages.get(3), forthImg);
		}

		Log.e("get lists image data", "url and id of images:-  "
				+ listForUserImages + "    id:---  " + listForUserImagesId);

		imgLoader.DisplayImage(image, pea_image);
		nameTxt.setText(name);
		txtForBio.setText("Bio: " + bio);
		getResult();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.done_btn:
			finish();
			break;

		case R.id.pass:
			passRequest();
			break;

		case R.id.imgForconnect:
			AcceptFriendRequest();
			break;

		case R.id.view_profile_img:
			initiatePopupWindow2();
			break;

		case R.id.view_profile_imgpop:
			pwindo2.dismiss();
			break;

		default:
			break;
		}
	}

	private PopupWindow pwindo2;

	private void initiatePopupWindow2() {
		try {
			// We need to get the instance of the LayoutInflater
			LayoutInflater inflater = (LayoutInflater) UserProfileScreen.this
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View layout = inflater.inflate(R.layout.view_result_popup,
					(ViewGroup) findViewById(R.id.popup));
			// layout.getBackground().setAlpha(210);
			DisplayMetrics metrics = this.getResources().getDisplayMetrics();
			int width = metrics.widthPixels;
			int height = metrics.heightPixels;
			pwindo2 = new PopupWindow(layout, width, height, true);
			pwindo2.showAtLocation(layout, Gravity.CENTER_HORIZONTAL, 0, 0);

			txtForResult = (TextView) layout.findViewById(R.id.txtForResult);

			ImageView imgforclose = (ImageView) layout
					.findViewById(R.id.view_profile_imgpop);
			imgforclose.setOnClickListener(this);

			ListView resultList = (ListView) layout
					.findViewById(R.id.listForResult);

			resultList.setAdapter(new ResultListAdapter(this));
			txtForResult.setText("" + correctAns + "/" + listForAnswer.size());
			// final HttpResponseListener listner = null;

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public class ResultListAdapter extends BaseAdapter {

		Activity activity;
		LayoutInflater inflater;

		public ResultListAdapter(Activity a) {
			activity = a;
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listForOPtion1.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View vi = convertView;
			if (convertView == null)
				vi = inflater.inflate(R.layout.quiz_manager_list_layout, null);

			TextView textForCount = (TextView) vi
					.findViewById(R.id.txtForSerialNo);

			final EditText textForOption1 = (EditText) vi
					.findViewById(R.id.txtForOption1);
			final EditText textForOption2 = (EditText) vi
					.findViewById(R.id.txtForOption2);

			ImageView chkBox = (ImageView) vi.findViewById(R.id.imgForCheckbox);
			ImageView chkBox2 = (ImageView) vi
					.findViewById(R.id.imgForCheckbox2);
			final ImageView chk = (ImageView) vi.findViewById(R.id.imgForCheck);
			final ImageView chk2 = (ImageView) vi
					.findViewById(R.id.imgForCheck2);
			chkBox.setVisibility(View.GONE);
			chkBox2.setVisibility(View.GONE);
			chk.setVisibility(View.GONE);
			chk2.setVisibility(View.GONE);
			textForOption1.setEnabled(false);
			textForOption2.setEnabled(false);

			if (listForAnswer.get(position)
					.equals(listForOPtion1.get(position))) {
				if (listForAnswerStatus.get(position).equals("correct")) {
					chk.setVisibility(View.VISIBLE);
					chk2.setVisibility(View.GONE);
					chk.setImageResource(R.drawable.right);
				} else {
					chk.setVisibility(View.VISIBLE);
					chk2.setVisibility(View.GONE);
					chk.setImageResource(R.drawable.close);
				}
			} else {
				if (listForAnswerStatus.get(position).equals("wrong")) {

					Log.d("wrong condition",
							"" + listForAnswerStatus.get(position));
					chk.setVisibility(View.GONE);
					chk2.setVisibility(View.VISIBLE);
					chk2.setImageResource(R.drawable.close);
				} else {
					chk.setVisibility(View.GONE);
					chk2.setVisibility(View.VISIBLE);
					chk2.setImageResource(R.drawable.right);
				}

			}
			textForOption1.setText(listForOPtion1.get(position));
			textForOption2.setText(listForOPtion2.get(position));
			textForCount.setText("" + (position + 1) + ". ");

			return vi;
		}

	}

	public void getResult() {

		String url = "http://peapodapp.com/webservices/androidapi.php?type=listquestion_results&userid="
				+ userID + "&freindid=" + friendId;
		System.out.println("url,,,," + url);
		Request result = (Request) new Request(url) {

			@Override
			protected void onSuccess(Transport transport) {

				try {
					JSONObject response = transport.getResponseJson();
					System.out.println("response here" + response);
					if (response == null)
						Toast.makeText(getApplicationContext(),
								"No response from server", Toast.LENGTH_LONG)
								.show();
					else {
						listForAnswer.clear();
						listForAnswerStatus.clear();
						listForOPtion1.clear();
						listForOPtion2.clear();
						JSONArray getArray = response.getJSONArray("list");
						Log.d("lenght", "" + getArray.length());
						for (int i = 0; i < getArray.length(); i++) {
							JSONObject data = getArray.optJSONObject(i);

							String option1 = data.optString("option1");
							String option2 = data.optString("option2");
							String answerStaus = data
									.optString("answer_status");
							String answer = data.optString("answer");

							if (answerStaus.equals("correct")) {
								correctAns++;
							}

							listForAnswerStatus.add(answerStaus);
							listForAnswer.add(answer);
							listForOPtion1.add(option1);
							listForOPtion2.add(option2);

							Log.d("0000000000000000000", "" + listForAnswer
									+ listForOPtion1 + listForOPtion2);

						}

					}

				} catch (Exception e) {
					// TODO: handle exception
				}

			}

		}.execute("GET");
	}

	public void passRequest() {

		String url = "http://peapodapp.com/webservices/androidapi.php?type=rejectfreindrequest&userid="
				+ userID + "&freindid=" + friendId;
		System.out.println("url passs resuest" + url);

		Request pass = (Request) new Request(url) {
			@Override
			protected void onSuccess(Transport transport) {
				try {
					JSONObject response = transport.getResponseJson();
					System.out.println("response here" + response);
					if (response == null) {
						Toast.makeText(getApplicationContext(),
								"No response from server", Toast.LENGTH_LONG)
								.show();
					} else {
						JSONObject get1 = response.optJSONObject("Response");
						// JSONObject get = get1.optJSONObject("returnCode");
						String success = get1.getString("resultText");
						System.out.println("success" + success);
						if (success.equals("Success")) {
							Log.d("9999999999999999", "passed");
							Intent home = new Intent(getApplicationContext(),
									HomeScreen.class);
							startActivity(home);
							finish();
						}
					}

				} catch (Exception e) {
					// TODO: handle exception
				}
			}

			@Override
			protected void onFailure(Transport transport) {
				// TODO Auto-generated method stub
				super.onFailure(transport);
			}

			@Override
			protected void onError(IOException ex) {
				// TODO Auto-generated method stub
				super.onError(ex);
			}
		}.execute("GET");
		pass.accept(Request.CTYPE_JSON);
		pass.setContentType(Request.CTYPE_JSON);

	}

	public void AcceptFriendRequest() {

		String url = "http://peapodapp.com/webservices/androidapi.php?type=acceptfreindrequest&userid="
				+ userID + "&freindid=" + friendId;

		Request accept = (Request) new Request(url) {

			@Override
			protected void onSuccess(Transport transport) {

				try {
					JSONObject response = transport.getResponseJson();
					if (response == null) {
						Toast.makeText(getApplicationContext(),
								"No response From Server", Toast.LENGTH_LONG)
								.show();
					} else {

						JSONObject get = response.getJSONObject("Response");
						String success = get.optString("resultText");
						if (success.equals("Success")) {
							Intent connect = new Intent(
									getApplicationContext(),
									Found_a_pea_activity.class);
							connect.putExtra("keyForFriendImage", image);
							startActivity(connect);
							finish();
						} else {
							Toast.makeText(getApplicationContext(),
									"Error occur Try Again", Toast.LENGTH_LONG)
									.show();
						}

					}
				} catch (Exception e) {
					// TODO: handle exception
				}

			}

			@Override
			protected void onFailure(Transport transport) {
				// TODO Auto-generated method stub
				super.onFailure(transport);
			}

			@Override
			protected void onError(IOException ex) {
				// TODO Auto-generated method stub
				super.onError(ex);
			}
		}.execute("GET");
		accept.accept(Request.CTYPE_JSON);
		accept.setContentType(Request.CTYPE_JSON);

	}

	@Override
	public void onBackPressed() {

	}

}
