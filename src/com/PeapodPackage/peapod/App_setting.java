package com.PeapodPackage.peapod;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONException;
import org.json.JSONObject;
import com.Peapod.Connections.ConnectionStatus;
import com.Peapod.Connections.HttpGetRespone;
import com.Peapod.Connections.HttpResponseListener;
import com.Peapod.Connections.StorePref;
import com.Peapod.Connections.TransparentProgressDialog;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.android.AsyncFacebookRunner.RequestListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

@SuppressWarnings("deprecation")
public class App_setting extends Activity implements OnClickListener,
		HttpResponseListener {

	private TextView about, privacy, tandc, deleteAccount, logout;
	private Button backBtn;
	StorePref pref;
	String status, userid;
	Boolean isConnected;
	TransparentProgressDialog pdialog;
	AlertDialog alertDialog;
	ConnectionStatus statusis;
	String delete_url;
	private static String APP_ID = "288532761335021";
	private Facebook facebook = new Facebook(APP_ID);
	private AsyncFacebookRunner mAsyncRunner;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.app_setting);

		about = (TextView) findViewById(R.id.about);
		privacy = (TextView) findViewById(R.id.privacyPolocy);
		tandc = (TextView) findViewById(R.id.termandcond);
		logout = (TextView) findViewById(R.id.logout);
		backBtn = (Button) findViewById(R.id.back_btn);
		deleteAccount = (TextView) findViewById(R.id.deleteAccount);
		pdialog = new TransparentProgressDialog(this, R.drawable.loader);
		deleteAccount.setOnClickListener(this);
		backBtn.setOnClickListener(this);
		about.setOnClickListener(this);
		privacy.setOnClickListener(this);
		tandc.setOnClickListener(this);
		logout.setOnClickListener(this);
		statusis = new ConnectionStatus(this);
		mAsyncRunner = new AsyncFacebookRunner(facebook);
		pref = StorePref.getInstance(this);
		userid = pref.getUserid();
		Log.e("userid = ", "" + userid);
		isConnected = statusis.isConnected();
		delete_url = "http://www.peapodapp.com/webservices/androidapi.php?type=delaccount&userid=";

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.about:
			Intent about_page = new Intent(App_setting.this, About.class);
			startActivity(about_page);
			break;

		case R.id.privacyPolocy:
			Intent privacy_page = new Intent(App_setting.this,
					PrivacyPolicy.class);
			startActivity(privacy_page);
			break;
		case R.id.termandcond:
			Intent tandc_page = new Intent(App_setting.this,
					Term_and_Conditions.class);
			startActivity(tandc_page);
			break;

		case R.id.back_btn:
			finish();
			break;

		case R.id.deleteAccount:
			AlertDialog.Builder alertbox = new AlertDialog.Builder(
					App_setting.this);
			alertbox.setTitle("Confirm Message");
			// alertbox.setIcon(R.drawable.warnings);
			alertbox.setMessage("Do You Want To Delete Your Account?");
			alertbox.setPositiveButton("Yes",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface arg0, int arg1) {
							logoutFromFacebook();
							hitApi();
						}
					});
			alertbox.setNegativeButton("No",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface arg0, int arg1) {
							// pdialog.dismiss();
							// finish();
						}
					});
			alertbox.show();
			break;

		case R.id.logout:

			AlertDialog.Builder alertbox1 = new AlertDialog.Builder(
					App_setting.this);
			alertbox1.setTitle("Confirm Message");
			// alertbox.setIcon(R.drawable.warnings);
			alertbox1.setMessage("Want To Logout?");
			alertbox1.setPositiveButton("Yes",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface arg0, int arg1) {
							fb_logout();
						}
					});
			alertbox1.setNegativeButton("No",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface arg0, int arg1) {
							// pdialog.dismiss();
							// finish();
						}
					});
			alertbox1.show();

			break;

		default:
			break;
		}

	}

	public void hitApi() {
		if (isConnected) {
			pdialog.show();
			try {
				new HttpGetRespone(delete_url + userid, this, 1).execute();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {

			if (pdialog.isShowing())
				pdialog.dismiss();
			alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle("StashApp");
			alertDialog.setMessage("No Internet Connection Available");
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			alertDialog.show();
		}
	}

	@Override
	public void response(String response, int page_id) {

		if (page_id == 1) {
			if (pdialog.isShowing()) {
				pdialog.dismiss();

			}
			try {
				JSONObject jsonResponse = new JSONObject(response);
				JSONObject obj = jsonResponse.getJSONObject("Response");
				JSONObject obj2 = obj.getJSONObject("returnCode");

				status = obj2.optString("result");
				Log.e("status = ", "" + status);
				if (status.equals("0")) {

					pref.setUserid(null);
					pref.setImageUrl(null);
					pref.setUserAge(null);
					pref.setUserBio(null);
					pref.setUserName(null);
					Intent login_page = new Intent(App_setting.this,
							LoginScreen.class);
					startActivity(login_page);
					finish();

				} else {
					if (pdialog.isShowing()) {
						pdialog.dismiss();

					}
				}
			} catch (JSONException e) {
				if (pdialog.isShowing()) {
					pdialog.dismiss();
				}
				e.printStackTrace();
			}
		}
	}

	@Override
	public void error(String error) {
		// TODO Auto-generated method stub
	}

	public void fb_logout() {
		try {

			pref.setUserid(null);
			pref.setImageUrl(null);
			pref.setUserAge(null);
			pref.setUserBio(null);
			pref.setUserName(null);
			Intent back = new Intent(App_setting.this, LoginScreen.class);
			startActivity(back);
			finish();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(getApplicationContext(), "" + e, 5000).show();
		}
	}

	/**
	 * Function to Logout user from Facebook
	 * */
	public void logoutFromFacebook() {
		mAsyncRunner.logout(this, new RequestListener() {
			@Override
			public void onComplete(String response, Object state) {
				Log.d("Logout from Facebook", response);
				if (Boolean.parseBoolean(response) == true) {
					runOnUiThread(new Runnable() {

						@Override
						public void run() {

						}

					});

				}
			}

			@Override
			public void onIOException(IOException e, Object state) {
			}

			@Override
			public void onFileNotFoundException(FileNotFoundException e,
					Object state) {
			}

			@Override
			public void onMalformedURLException(MalformedURLException e,
					Object state) {
			}

			@Override
			public void onFacebookError(FacebookError e, Object state) {
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		facebook.authorizeCallback(requestCode, resultCode, data);
	}

	@Override
	public void onBackPressed() {

	}
}
