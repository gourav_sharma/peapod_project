package com.PeapodPackage.peapod;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.Peapod.Connections.ImageLoaderRound;
import com.Peapod.Connections.StorePref;
import com.Peapod.Connections.TransparentProgressDialog;
import com.Peapod.SqliteDb.FriendListSearch;
import se.tonyivanov.android.ajax.Request;
import se.tonyivanov.android.ajax.Transport;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import android.widget.Toast;

public class Peapod_chat_screen extends Activity implements OnClickListener,
		OnItemClickListener, OnQueryTextListener {

	private TransparentProgressDialog pdialog;
	private Button back_btn;
	private ListView list_for_pea_chat;
	private TextView txt_for_pea_name, txt_for_pea_status;
	private ImageView peaChatImage, chat_img;
	ArrayList<FriendListSearch> filterList;
	ArrayList<String> listForrecieverId = new ArrayList<String>();
	ArrayList<String> listForUserName = new ArrayList<String>();
	ArrayList<String> listForUserImage = new ArrayList<String>();
	ArrayList<String> listForMessage = new ArrayList<String>();
	ImageLoaderRound image_loader_round;
	ArrayList<FriendListSearch> mStringFilterList;
	StorePref pref;
	static String userid;
	final static String url = "http://peapodapp.com/webservices/androidapi.php?type=recentchatlist&userid=";

	FriendListSearch obj_friend_list_search;
	ArrayList<FriendListSearch> friend_list_search_arrayList = new ArrayList<FriendListSearch>();
	ListAdapter adp;
	private SearchView search_view_friend_from_list;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.peapod_chat_screen);

		pdialog = new TransparentProgressDialog(this, R.drawable.loader);
		image_loader_round = new ImageLoaderRound(getApplicationContext());
		pref = StorePref.getInstance(getApplicationContext());
		userid = pref.getUserid();
		back_btn = (Button) findViewById(R.id.back_btn);
		back_btn.setOnClickListener(this);
		search_view_friend_from_list = (SearchView) findViewById(R.id.search_view_friend_from_list);
		search_view_friend_from_list.setOnQueryTextListener(this);

		chat_img = (ImageView) findViewById(R.id.chat_img);
		chat_img.setOnClickListener(this);

		list_for_pea_chat = (ListView) findViewById(R.id.list_for_chat_matches);

		list_for_pea_chat.setOnItemClickListener(this);
		getRecentChatList();
		pdialog.show();
	}

	public class ListAdapter extends BaseAdapter implements Filterable {

		private Activity activity;
		private LayoutInflater inflater = null;

		ArrayList<FriendListSearch> friend_list_search_arrayList;
		ValueFilter valueFilter;

		public ListAdapter(Activity a,
				ArrayList<FriendListSearch> friend_list_search_arrayList) {
			activity = a;
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			this.friend_list_search_arrayList = friend_list_search_arrayList;
			mStringFilterList = friend_list_search_arrayList;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return friend_list_search_arrayList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return friend_list_search_arrayList.indexOf(getItem(position));
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			View vi = convertView;
			if (convertView == null)
				vi = inflater.inflate(R.layout.peapod_chat_screen_list_layout,
						null);

			txt_for_pea_name = (TextView) vi
					.findViewById(R.id.txt_for_pea_name);
			txt_for_pea_status = (TextView) vi
					.findViewById(R.id.txt_for_pea_status);
			peaChatImage = (ImageView) vi.findViewById(R.id.pea_chat_img);

			FriendListSearch FriendlistSearch = friend_list_search_arrayList
					.get(position);

			txt_for_pea_name.setText(" " + FriendlistSearch.getFriendName());
			txt_for_pea_status.setText("" + FriendlistSearch.getLastMessage());
			image_loader_round.DisplayImage("" + FriendlistSearch.getImage(),
					peaChatImage);
			return vi;
		}

		private class ValueFilter extends Filter {
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults results = new FilterResults();

				if (constraint != null && constraint.length() > 0) {
					filterList = new ArrayList<FriendListSearch>();
					for (int i = 0; i < mStringFilterList.size(); i++) {
						if ((mStringFilterList.get(i).getFriendName()
								.toUpperCase()).contains(constraint.toString()
								.toUpperCase())) {

							FriendListSearch FriendListSearch = new FriendListSearch(
									mStringFilterList.get(i).getImage(),
									mStringFilterList.get(i).getFriendName(),
									mStringFilterList.get(i).getLastMessage(),
									mStringFilterList.get(i).getConnectDate(),
									mStringFilterList.get(i).getID());

							filterList.add(FriendListSearch);
						}
					}
					results.count = filterList.size();
					results.values = filterList;

					list_for_pea_chat
							.setOnItemClickListener(new OnItemClickListener() {

								@Override
								public void onItemClick(AdapterView<?> arg0,
										View arg1, int position, long arg3) {

									Intent private_chat_screen_page = new Intent(
											Peapod_chat_screen.this,
											Private_chat_screen.class);

									private_chat_screen_page
											.putExtra("keyForImage", filterList
													.get(position).getImage());
									private_chat_screen_page.putExtra(
											"keyForName",
											filterList.get(position)
													.getFriendName());
									private_chat_screen_page.putExtra(
											"keyForFrndId",
											filterList.get(position).getID());
									startActivity(private_chat_screen_page);

								}
							});

				} else {
					results.count = mStringFilterList.size();
					results.values = mStringFilterList;
				}
				return results;

			}

			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				friend_list_search_arrayList = (ArrayList<FriendListSearch>) results.values;
				notifyDataSetChanged();
			}

		}

		@Override
		public Filter getFilter() {
			if (valueFilter == null) {
				valueFilter = new ValueFilter();
			}
			return valueFilter;
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.back_btn:
			Intent home = new Intent(getApplicationContext(), HomeScreen.class);
			startActivity(home);
			finish();
			break;

		case R.id.chat_img:
			Intent friendList = new Intent(getApplicationContext(),
					FriendsList.class);
			startActivity(friendList);

			break;

		default:
			break;
		}

	}

	@Override
	public void onBackPressed() {

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

		Intent private_chat_screen_page = new Intent(Peapod_chat_screen.this,
				Private_chat_screen.class);

		private_chat_screen_page.putExtra("keyForImage",
				friend_list_search_arrayList.get(position).getImage());
		private_chat_screen_page.putExtra("keyForName",
				friend_list_search_arrayList.get(position).getFriendName());
		private_chat_screen_page.putExtra("keyForFrndId",
				friend_list_search_arrayList.get(position).getID());
		startActivity(private_chat_screen_page);
		finish();

	}

	public void getRecentChatList() {

		Log.d("APIurllllllll", url + userid);

		Request recentList = (Request) new Request(url + userid) {

			@Override
			protected void onComplete(Transport transport) {

				JSONObject response = transport.getResponseJson();

				if (pdialog.isShowing()) {
					pdialog.dismiss();
				}

				if (response == null) {
					Toast.makeText(getApplicationContext(),
							"no response from server", Toast.LENGTH_SHORT)
							.show();

				} else {
					try {
						JSONArray data = response.getJSONArray("list");

						if (data.length() == 0) {
							Toast.makeText(getApplicationContext(),
									"No Data Available", Toast.LENGTH_SHORT)
									.show();

						} else {

							listForMessage.clear();
							listForrecieverId.clear();
							listForUserImage.clear();
							listForUserName.clear();
							friend_list_search_arrayList.clear();
							for (int j = 0; j < data.length(); j++) {
								JSONObject getData = data.getJSONObject(j);
								String recievreId = getData
										.optString("receiverid");
								String lastMessage = getData
										.optString("message");

								Log.e("list:- ", "id: " + recievreId
										+ ", message: " + lastMessage);

								listForrecieverId.add(recievreId);
								listForMessage.add(lastMessage);

								JSONObject userDetail = getData
										.optJSONObject("userdetails");

								String user_name = userDetail
										.optString("fullname");
								String image = userDetail
										.optString("profilepic");

								listForUserName.add(user_name);
								listForUserImage.add(image);
								Log.e("list:- ", "name: " + listForUserName
										+ ", image: " + listForUserImage);

								obj_friend_list_search = new FriendListSearch(
										image, user_name, lastMessage, "",
										recievreId);

								friend_list_search_arrayList.add(j,
										obj_friend_list_search);

								if (pdialog.isShowing()) {
									pdialog.dismiss();
								}

							}

							adapter();
						}
					} catch (Exception e) {
						e.printStackTrace();
						if (pdialog.isShowing()) {
							pdialog.dismiss();
						}
					}

				}
			}

			@Override
			protected void onFailure(Transport transport) {
				Toast.makeText(getApplicationContext(), "Error: " + transport,
						Toast.LENGTH_SHORT).show();
				if (pdialog.isShowing()) {
					pdialog.dismiss();
				}
			}

			@Override
			protected void onError(IOException ex) {
				Toast.makeText(getApplicationContext(), "Error: " + ex,
						Toast.LENGTH_SHORT).show();
				if (pdialog.isShowing()) {
					pdialog.dismiss();
				}
			}

		}.execute("GET");
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		// TODO Auto-generated method stub
		adp.getFilter().filter(newText);
		return false;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		// TODO Auto-generated method stub
		return false;
	}

	public void adapter() {

		adp = new ListAdapter(Peapod_chat_screen.this,
				friend_list_search_arrayList);
		list_for_pea_chat.setAdapter(adp);

	}
}
