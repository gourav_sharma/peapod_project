package com.Peapod.SqliteDb;

import android.util.Log;

public class FriendListSearch {

	String image_name, friend_name, last_message, connect_date, id;
	int flag;

	public FriendListSearch(String image_name, String friend_name,
			String last_message, String connect_date, String id) {
		this.image_name = image_name;
		this.friend_name = friend_name;
		this.last_message = last_message;
		this.connect_date = connect_date;
		this.id = id;
	}

	public String getImage() {
		return image_name;
	}

	public void setImage(String image_name) {
		this.image_name = image_name;
		Log.e("image:= ", image_name);
	}

	public String getFriendName() {
		return friend_name;
	}

	public void setFriendName(String friend_name) {
		this.friend_name = friend_name;
	}

	public String getLastMessage() {
		return last_message;
	}

	public void setLastMessage(String last_message) {
		this.last_message = last_message;
	}

	public String getConnectDate() {
		return connect_date;
	}

	public void setConnectDate(String connect_date) {
		this.connect_date = connect_date;
	}

	public String getID() {
		return id;
	}

	public void setid(String id) {
		this.id = id;
	}
}
