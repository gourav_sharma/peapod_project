package com.PeapodPackage.peapod;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONObject;

import com.Peapod.Connections.ImageLoader;
import com.Peapod.Connections.ImageLoaderRound;
import com.Peapod.Connections.StorePref;

import se.tonyivanov.android.ajax.Request;
import se.tonyivanov.android.ajax.Transport;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Request_Activity extends Activity implements OnClickListener,
		OnItemClickListener {

	private Button back_btn;
	private ListView list_for_pea_chat;
	private TextView txt_for_pea_name, txt_for_pea_score,
			txt_for_pea_view_profile;
	StorePref pref;
	private String userID;
	private ImageView peaChatImage;
	ImageLoaderRound imgLoader;
	double my_lat, my_long;
	String timeStatus;

	private ArrayList<String> listForName = new ArrayList<String>();
	private ArrayList<Integer> listForDob = new ArrayList<Integer>();
	private ArrayList<String> listForProfilePic = new ArrayList<String>();
	private ArrayList<String> listForRequesTime = new ArrayList<String>();
	private ArrayList<String> listForFriendId = new ArrayList<String>();
	private ArrayList<String> listForTimeStatus = new ArrayList<String>();
	private ArrayList<String> listForBio = new ArrayList<String>();
	private ArrayList<String> listForUserImages = new ArrayList<String>();
	private ArrayList<String> listForUserImagesId = new ArrayList<String>();
	private ArrayList<Double> listForUserDistance = new ArrayList<Double>();
	private ArrayList<String> listForShowActiveTime = new ArrayList<String>();

	private ArrayList<String> listForCorrectAns = new ArrayList<String>();
	private ArrayList<String> listForTotalQues = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.request_activity);

		back_btn = (Button) findViewById(R.id.back_btn);
		back_btn.setOnClickListener(this);
		imgLoader = new ImageLoaderRound(getApplicationContext());
		pref = StorePref.getInstance(getApplicationContext());
		userID = pref.getUserid();

		list_for_pea_chat = (ListView) findViewById(R.id.list_for_chat_matches);
		getRequestList();
		list_for_pea_chat.setOnItemClickListener(this);

		my_lat = pref.getMyLat();
		my_long = pref.getMyLong();
	}

	public class ListAdapter extends BaseAdapter {

		private Activity activity;
		private LayoutInflater inflater = null;

		public ListAdapter(Activity a) {
			activity = a;
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listForName.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {

			View vi = convertView;
			if (convertView == null)
				vi = inflater.inflate(R.layout.request_activity_list_layout,
						null);

			TextView txt_for_Time_request = (TextView) vi
					.findViewById(R.id.txt_for_Time_request);

			txt_for_pea_name = (TextView) vi
					.findViewById(R.id.txt_for_pea_name);
			txt_for_pea_score = (TextView) vi
					.findViewById(R.id.txt_for_pea_score);
			txt_for_pea_view_profile = (TextView) vi
					.findViewById(R.id.txt_for_View_profile);
			txt_for_pea_view_profile.setText("View profile");
			peaChatImage = (ImageView) vi.findViewById(R.id.pea_chat_img);

			txt_for_pea_name.setText("" + listForName.get(position));
			txt_for_pea_score.setText("" + listForName.get(position)
					+ " Scored " + listForCorrectAns.get(position) + "/"
					+ listForTotalQues.get(position));

			imgLoader.DisplayImage(listForProfilePic.get(position),
					peaChatImage);

			// peaChatImage.setBackgroundResource(images[position]);
			txt_for_Time_request.setText("" + listForRequesTime.get(position)); // /
																				// set
																				// ago
																				// time
																				// and
																				// status

			txt_for_pea_view_profile.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent user_profile = new Intent(getApplicationContext(),
							UserProfileScreen.class);

					user_profile.putExtra("keyForName",
							listForName.get(position));
					user_profile.putExtra("keyForImage",
							listForProfilePic.get(position));
					user_profile.putExtra("keyForFriendId",
							listForFriendId.get(position));
					user_profile.putExtra("keyForBio", listForBio.get(position));
					user_profile.putStringArrayListExtra("keyForListImages",
							listForUserImages);
					user_profile.putStringArrayListExtra("keyForListImagesId",
							listForUserImagesId);
					user_profile.putExtra("keyForDist",
							listForUserDistance.get(position));
					user_profile.putExtra("keyForActiveTime",
							listForShowActiveTime.get(position));
					startActivity(user_profile);
				}
			});
			return vi;
		}
	}

	public void adapter() {

		ListAdapter adp = new ListAdapter(Request_Activity.this);
		list_for_pea_chat.setAdapter(adp);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.back_btn:
			finish();
			break;

		default:
			break;
		}

	}

	@Override
	public void onBackPressed() {

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

		// Intent private_chat_screen_page = new Intent(Request_Activity.this,
		// Private_chat_screen.class);
		// startActivity(private_chat_screen_page);

	}

	public void getRequestList() {

		String url = "http://peapodapp.com/webservices/androidapi.php?type=getfreindrequest&userid="
				+ userID;
		Request r = (Request) new Request(url) {

			@Override
			protected void onSuccess(Transport transport) {
				try {
					JSONObject response = transport.getResponseJson();
					Log.d("response", "" + response);
					if (response == null) {
						// if (pdialog.isShowing()) {
						// pdialog.dismiss();
						//
						// }
						Toast.makeText(getApplicationContext(), "Server Error",
								5000).show();
					} else {
						// if (pdialog.isShowing()) {
						// pdialog.dismiss();
						// }
						JSONArray jsonMainNode = response.optJSONArray("list");
						for (int i = 0; i < jsonMainNode.length(); i++) {
							JSONObject jsonChildNode = jsonMainNode
									.getJSONObject(i);

							String name = jsonChildNode.optString("fullname");
							// String dob = jsonChildNode.optString("dob");
							String rDate = jsonChildNode
									.optString("requestdate");
							String pic = jsonChildNode.optString("profilepic");
							String F_id = jsonChildNode.optString("id");
							String currentTime = jsonChildNode
									.optString("displaydate");
							String bio = jsonChildNode.optString("bio");
							String created = jsonChildNode.optString("created");
							JSONArray imgArray = jsonChildNode
									.getJSONArray("images");

							String correct_answer = jsonChildNode
									.optString("correct_answer");
							String total_ques = jsonChildNode
									.optString("total_ques");

							listForTotalQues.add(total_ques);
							listForCorrectAns.add(correct_answer);

							double lat = jsonChildNode.optDouble("lat");
							double longi = jsonChildNode.optDouble("longt");

							double dist = distance(my_lat, my_long, lat, longi,
									'k');

							Log.e("  distance ----   ", " distance ---  "
									+ dist);
							listForUserDistance.add(dist);

							for (int j = 0; j < imgArray.length(); j++) {

								JSONObject getUserImages = imgArray
										.getJSONObject(j);
								String imgUrl = getUserImages
										.optString("imageurl");
								String imgId = getUserImages
										.optString("userid");
								listForUserImages.add(imgUrl);
								listForUserImagesId.add(imgId);
							}
							Log.e("request user image url", " ------ urls:-  "
									+ listForUserImages);
							String mainChapterNum = name.substring(0,
									name.indexOf(" "));
							Log.d(",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,", ""
									+ mainChapterNum);
							// String cDateInString = currentTime;

							Calendar c = Calendar.getInstance();
							Date a = c.getTime();
							SimpleDateFormat formatter = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss");
							Date date = formatter.parse(created);

							// String dateInString = rDate;

							String di = getTimeDiff(a, date);

							String[] activTimeArray = di.split(",");
							Log.e("----------------", " created:-   " + date
									+ "      " + a + "   diff:-----   "
									+ getTimeDiff(a, date) + "  di:- "
									+ activTimeArray[0] + "   "
									+ activTimeArray[1]

							// + "  min:-  " + min
							// + "  hour:--  " + hour + "  days:-- "
							// + days

							);

							Integer hours = Integer.parseInt(activTimeArray[0]);
							Integer mins = Integer.parseInt(activTimeArray[1]);

							// if (hours == 0) {
							// listForShowActiveTime.add(mins + " mins ago");
							// } else if (hours > 24) {
							// hours = hours / 24;
							// listForShowActiveTime.add(hours + " days ago");
							// } else if (hours > 169) {
							// hours = hours / 168;
							// listForShowActiveTime.add(hours + " week ago");
							// } else {
							// listForShowActiveTime.add(hours + " hours ago");
							// Log.d(" in last else:- ", " hours:-  " + hours
							// + "   mins:- " + mins);
							// }
							if (hours == 0) {
								listForShowActiveTime.add(mins + " mins ago");
							} else if (hours >= 24 && hours < 168) {
								int days = hours / 24;
								listForShowActiveTime.add(days + " days ago");
							} else if (hours >= 168 && hours < 720) {
								int week = hours / 168;
								listForShowActiveTime.add(week + " week ago");
							} else {
								listForShowActiveTime.add(hours + " hours ago");
							}

							Date reqDate = formatter.parse(rDate);

							String di2 = getTimeDiff(a, reqDate);

							String[] ReqTimeArray = di2.split(",");
							Log.e("----------------", " requuest-   " + reqDate
									+ "      " + a + "   diff:-----   "
									+ getTimeDiff(a, reqDate) + "  hour:- "
									+ ReqTimeArray[0] + " min:-  "
									+ ReqTimeArray[1]

							// + "  min:-  " + min
							// + "  hour:--  " + hour + "  days:-- "
							// + days

							);

							Integer Reqhours = Integer
									.parseInt(ReqTimeArray[0]);
							Integer Resmins = Integer.parseInt(ReqTimeArray[1]);

							if (Reqhours == 0) {
								listForRequesTime.add(Resmins + "mins ago");
							} else if (Reqhours > 23) {
								Reqhours = Reqhours / 24;
								listForRequesTime.add(Reqhours + " days ago");
							} else {
								listForRequesTime.add(Reqhours + " hours ago");
							}

							// String[] d = currentTime.split(", ");
							// Log.e("date array ", "  " + d[0]);
							// for (int j = 0; j < d.length; j++) {
							//
							// String time = d[0];
							// if (time.equals("0")) {
							//
							// } else {
							// String displayTime = d[i];
							// listForRequesTime.add(displayTime);
							//
							// }
							//
							// }

							// try {
							//
							// Date date = formatter.parse(dateInString);
							// System.out.println(date);
							// System.out.println(formatter.format(date));
							//
							// Date cdate = formatter.parse(cDateInString);
							//
							// // Calendar c = Calendar.getInstance();
							// // SimpleDateFormat sdf = new SimpleDateFormat(
							// // "mm-dd-yyyy hh:mm:ss");
							// // String strDate = sdf.format(c.getTime());
							// //
							// // Date crntDate = formatter.parse(strDate);
							// //
							// // Log.e("difference of two date:--    ", ""
							// // + getTimeDiff(crntDate, date));
							// // Log.e("current date:-  ", "" + strDate);
							//
							// long timeDiff = getTimeDiff(cdate, date);
							//
							// long min = TimeUnit.MICROSECONDS
							// .toMinutes(timeDiff);
							// long hour = TimeUnit.MICROSECONDS
							// .toHours(timeDiff);
							// long days = TimeUnit.MICROSECONDS
							// .toDays(timeDiff);
							//
							// long displayTime;
							// if (min < 60) {
							// displayTime = min;
							// timeStatus = "min ago";
							// } else if (hour < 24) {
							// displayTime = hour;
							// timeStatus = "hour ogo";
							// } else if (days < 30) {
							// displayTime = days;
							// timeStatus = "days ago";
							// } else {
							// displayTime = (long) (days / 30.41);
							// timeStatus = "months ago";
							// }
							//
							// Log.e("tme days in gettimediff:-  ", " days:-"
							// + days + " hour:- " + hour + " min:- "
							// + min);
							//
							// listForRequesTime.add(displayTime);
							// listForTimeStatus.add(timeStatus);
							//
							// } catch (ParseException e) {
							// e.printStackTrace();
							// }

							// String newDate = null;
							// Calendar c = Calendar.getInstance();
							// System.out
							// .println("Current time => " + c.getTime());
							// SimpleDateFormat df = new
							// SimpleDateFormat("yyyy");
							// String currentDate = df.format(c.getTime());
							// // String givenDateString = dob;
							// SimpleDateFormat sdf = new SimpleDateFormat(
							// "dd/mm/yyyy");
							// // try {
							// // Date mDate = sdf.parse(givenDateString);
							// // java.util.Date tmpDate = sdf
							// // .parse(givenDateString);
							// // SimpleDateFormat formatTo = new
							// SimpleDateFormat(
							// // "yyyy");
							// // System.out
							// //
							// .println("Date in  ===========================:: "
							// // + mDate);
							// // newDate = formatTo.format(tmpDate);
							// // int age = Integer.parseInt(currentDate)
							// // - Integer.parseInt(newDate);
							// //
							// Log.d("...........................................",
							// // "" + age);
							// // listForDob.add(age);
							// // } catch (ParseException e) {
							// // e.printStackTrace();
							// // }
							//
							// // listForDob.add(20);
							// String givenDateString2 = rDate;
							// SimpleDateFormat sdf2 = new SimpleDateFormat(
							// "yyyy-mm-dd hh:mm:ss");
							// try {
							// Date mDate = sdf2.parse(givenDateString2);
							// // int timeInMilliseconds = mDate.getDate();
							// // long month = mDate.getTime();
							//
							// java.util.Date tmpDate = sdf2
							// .parse(givenDateString2);
							// SimpleDateFormat formatTo = new SimpleDateFormat(
							// "M");
							// System.out
							// .println("Date in  ===========================:: "
							// + mDate);
							//
							// int currentHour = c.get(Calendar.MINUTE);
							//
							// String newDate2 = formatTo.format(tmpDate);
							//
							// int newTime = currentHour
							// - Integer.parseInt(newDate2);
							//
							// getTimeDiff(mDate, tmpDate);
							// Log.e("time difference here:----  ", ""
							// + getTimeDiff(mDate, tmpDate));
							//
							// System.out
							// .println("change format  ===========================:: "
							// + newDate2
							// + "................"
							// + currentHour);
							// Log.d(".....................Date...", ""
							// + newTime);
							//
							// listForRequesTime.add(newTime);//
							// lisrFordate.add(newDate);
							// } catch (ParseException e) {
							// e.printStackTrace();
							// }

							listForFriendId.add(F_id);
							listForName.add(mainChapterNum);
							listForProfilePic.add(pic);
							listForBio.add(bio);
						}
						adapter();
					}
				} catch (Exception e) {

					// if (pdialog.isShowing()) {
					// pdialog.dismiss();
					//
					// }
					e.printStackTrace();
				}
			}

			@Override
			protected void onError(IOException ex) {
				// TODO Auto-generated method stub

			}

			@Override
			protected void onFailure(Transport transport) {
				// TODO Auto-generated method stub

			}
		}.execute("GET");
		r.accept(Request.CTYPE_JSON);
		r.setContentType(Request.CTYPE_JSON);
	}

	public String getTimeDiff(Date dateOne, Date dateTwo) {
		String diff;
		long timeDiff = Math.abs(dateOne.getTime() - dateTwo.getTime());
		diff = String.format(
				"%d,%d",
				TimeUnit.MILLISECONDS.toHours(timeDiff),
				TimeUnit.MILLISECONDS.toMinutes(timeDiff)
						- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
								.toHours(timeDiff)));
		return diff;
	}

	private double distance(double lat1, double lon1, double lat2, double lon2,
			char unit) {
		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2))
				+ Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2))
				* Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		if (unit == 'K') {
			dist = dist * 1.609344;
		} else if (unit == 'N') {
			dist = dist * 0.8684;
		}
		dist = dist * 0.621371;
		dist = Math.round(dist * 100.0) / 100.0;
		return (dist);
	}

	private double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
	/* :: This function converts radians to decimal degrees : */
	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
	private double rad2deg(double rad) {
		return (rad * 180 / Math.PI);
	}

}
