package com.PeapodPackage.peapod;

import com.Peapod.Connections.ImageLoader;
import com.Peapod.Connections.ImageLoaderRound;
import com.Peapod.Connections.StorePref;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class Found_a_pea_activity extends Activity implements OnClickListener {

	private ImageView search, chat, userImage, friendImage;
	String friendImageUrl, userImageUrl;
	ImageLoaderRound imageloader;
	StorePref pref;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.found_a_pea_activity);

		userImage = (ImageView) findViewById(R.id.firstImage);
		friendImage = (ImageView) findViewById(R.id.secondImage);

		search = (ImageView) findViewById(R.id.search);
		search.setOnClickListener(this);
		chat = (ImageView) findViewById(R.id.chat);
		chat.setOnClickListener(this);
		imageloader = new ImageLoaderRound(getApplicationContext());
		pref = StorePref.getInstance(getApplicationContext());
		userImageUrl = pref.getImageUrl();
		friendImageUrl = getIntent().getStringExtra("keyForFriendImage");
		imageloader.DisplayImage(userImageUrl, userImage);
		imageloader.DisplayImage(friendImageUrl, friendImage);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.search:
			Intent home = new Intent(getApplicationContext(), HomeScreen.class);
			startActivity(home);
			finish();
			break;

		case R.id.chat:
			Intent chat_screen = new Intent(getApplicationContext(),
					Peapod_chat_screen.class);
			startActivity(chat_screen);
			finish();
			break;

		default:
			break;
		}
	}

	@Override
	public void onBackPressed() {

	}

}
