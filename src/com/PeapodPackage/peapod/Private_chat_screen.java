package com.PeapodPackage.peapod;

import java.io.IOException;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

import se.tonyivanov.android.ajax.Request;
import se.tonyivanov.android.ajax.Transport;

import com.Peapod.Connections.ImageLoaderRound;
import com.Peapod.Connections.StorePref;
import com.Peapod.SqliteDb.DataBaseSampleActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Private_chat_screen extends Activity implements OnClickListener {

	private ImageButton backBtn;
	private RelativeLayout profile_logo;
	ImageView chat_top;
	static String frndId = null;
	ImageLoaderRound imgLoader;
	TextView pea_name;
	String image, name;
	EditText edt_for_msd_send;
	Button btn_for_msg_send;
	String message, userid;
	ListView listForShowChat;
	StorePref pref;
	ArrayList<String> listFrorecieveMsg = new ArrayList<String>();
	ArrayList<String> listFroSenderId = new ArrayList<String>();
	ArrayList<Integer> listFrorecieveId = new ArrayList<Integer>();
	ArrayList<Integer> listForId = new ArrayList<Integer>();
	ArrayList<String> listFroDate = new ArrayList<String>();

	// array list for db
	ArrayList<Integer> id = new ArrayList<Integer>();
	ArrayList<String> senderid = new ArrayList<String>();
	ArrayList<String> recvid = new ArrayList<String>();
	ArrayList<String> db_message = new ArrayList<String>();
	ArrayList<String> date = new ArrayList<String>();

	private Handler handler = new Handler();
	DataBaseSampleActivity db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.private_chat);

		pref = StorePref.getInstance(getApplicationContext());
		userid = pref.getUserid();
		edt_for_msd_send = (EditText) findViewById(R.id.edt_for_msd_send);
		btn_for_msg_send = (Button) findViewById(R.id.btn_for_msg_send);
		btn_for_msg_send.setOnClickListener(this);
		listForShowChat = (ListView) findViewById(R.id.listForShowChat);
		db = new DataBaseSampleActivity(getApplicationContext());

		backBtn = (ImageButton) findViewById(R.id.back_btn);
		backBtn.setOnClickListener(this);

		profile_logo = (RelativeLayout) findViewById(R.id.profile_logo);
		profile_logo.setOnClickListener(this);

		frndId = getIntent().getStringExtra("keyForFrndId");

		imgLoader = new ImageLoaderRound(getApplicationContext());
		chat_top = (ImageView) findViewById(R.id.chat_top);
		pea_name = (TextView) findViewById(R.id.pea_name);

		image = getIntent().getStringExtra("keyForImage");
		name = getIntent().getStringExtra("keyForName");
		imgLoader.DisplayImage(image, chat_top);
		pea_name.setText(name);

		edt_for_msd_send.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				btn_for_msg_send.setClickable(true);

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				btn_for_msg_send.setClickable(false);

			}

			@Override
			public void afterTextChanged(Editable s) {
				btn_for_msg_send.setClickable(true);

			}
		});
		// deleteChatTable();
		recieveMessage();
		getMessagesFromSqlite();
	}

	@Override
	public void onBackPressed() {

	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.back_btn:
			Intent back = new Intent(Private_chat_screen.this,
					Peapod_chat_screen.class);
			startActivity(back);
			finish();
			break;

		case R.id.profile_logo:
			Intent profile = new Intent(getApplicationContext(),
					ProfileNotification.class);
			profile.putExtra("keyForFrndId", frndId);
			startActivity(profile);
			break;
		case R.id.btn_for_msg_send:
			message = edt_for_msd_send.getText().toString();
			String chk = edt_for_msd_send.getText().toString();

			if (chk.equals("") || chk.equals(null) || chk.length() <= 0) {

			} else {

				new Thread(new Runnable() {
					@Override
					public void run() {
						// TODO Auto-generated method stub
						sendMessage();
					}
				}).start();

				// message = null;
			}
			break;

		default:
			break;
		}

	}

	public class ChatListAdapter extends BaseAdapter {
		Activity activity;
		LayoutInflater inFlater;

		public ChatListAdapter(Activity a) {
			activity = a;
			inFlater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return id.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View vi = convertView;
			if (convertView == null)
				vi = inFlater.inflate(R.layout.recieve_message_layout, null);
			TextView recieveMsgTxt = (TextView) vi
					.findViewById(R.id.messageRecTxt);
			TextView sendMsgTxt = (TextView) vi
					.findViewById(R.id.messageSendTxt);
			recieveMsgTxt.setVisibility(View.GONE);
			sendMsgTxt.setVisibility(View.GONE);

			if (senderid.get(position).toString().equals(userid)) {
				sendMsgTxt.setVisibility(View.VISIBLE);
				recieveMsgTxt.setVisibility(View.GONE);
				sendMsgTxt.setText("" + db_message.get(position));
			} else {
				sendMsgTxt.setVisibility(View.GONE);
				recieveMsgTxt.setVisibility(View.VISIBLE);
				recieveMsgTxt.setText("" + db_message.get(position));
			}
			// listForShowChat
			// .smoothScrollToPosition(listFrorecieveMsg.size() - 1);
			return vi;

		}
	}

	public void sendMessage() {
		// message = edt_for_msd_send.getText().toString();
		String url = "http://peapodapp.com/webservices/androidapi.php?type=sendmessage&senderid="
				+ userid + "&receiverid=" + frndId + "&message=" + message;
		url = url.replace(" ", "%20");
		// Log.e("send message:- ", "" + url);

		if (!message.equals("") || !message.equals(null)) {

			Request send = (Request) new Request(url) {

				@Override
				protected void onPreExecute() {
					// TODO Auto-generated method stub
					btn_for_msg_send.setClickable(false);
				}

				@Override
				protected void onSuccess(Transport transport) {
					JSONObject response = transport.getResponseJson();
					JSONObject returnCode = response.optJSONObject("Response");
					JSONObject result = returnCode.optJSONObject("returnCode");
					String success = result.optString("resultText");
					if (success.equals("Success")) {
						edt_for_msd_send.setText("");
						message = "";
						Log.e("send", "send");
					} else {

					}

				}

				@Override
				protected void onPostExecute(Transport transport) {
					// TODO Auto-generated method stub
					edt_for_msd_send.setText("");
					message = "";
					btn_for_msg_send.setClickable(true);
				}

				@Override
				protected void onError(IOException ex) {
					Toast.makeText(getApplicationContext(), "Error: " + ex,
							Toast.LENGTH_SHORT).show();
				}

				@Override
				protected void onFailure(Transport transport) {
					Toast.makeText(getApplicationContext(),
							"Error: " + transport, Toast.LENGTH_SHORT).show();
				}

			}.execute("GET");
		}

	}

	public void recieveMessage() {

		String url = "http://peapodapp.com/webservices/androidapi.php?type=recentchat&senderid="
				+ userid + "&receiverid=" + frndId;
		url = url.replace(" ", "%20");
		// Log.e("url here:-  ", "url here:-  " + url);
		Request recieve = (Request) new Request(url) {

			@Override
			protected void onSuccess(Transport transport) {
				try {
					JSONObject response = transport.getResponseJson();
					JSONArray messagwArray = response.getJSONArray("list");
					Integer size = messagwArray.length();
					// pref.setMessageLenght(size);
					listFrorecieveMsg.clear();
					listFroSenderId.clear();
					listForId.clear();
					listFrorecieveId.clear();
					listFroDate.clear();
					for (int i = 0; i < messagwArray.length(); i++) {
						JSONObject readData = messagwArray.optJSONObject(i);
						String get_message = readData.optString("message");
						String senderId = readData.optString("senderid");
						Integer id = readData.optInt("id");
						Integer recv_id = readData.optInt("receiverid");
						String date = readData.optString("craeted");
						listFrorecieveMsg.add(get_message);
						listFroSenderId.add(senderId);
						listFroDate.add(date);
						listFrorecieveId.add(recv_id);
						listForId.add(id);
					}
					Log.e("recv", "recv");
					if (listFroSenderId.size() != 0) {
						String db_message = "", db_sender_id = "", db_date = "";
						Integer db_id, db_recvid;
						db_date = listFroDate.get(listForId.size() - 1);
						db_id = listForId.get(listForId.size() - 1);
						db_message = listFrorecieveMsg
								.get(listForId.size() - 1);
						db_recvid = listFrorecieveId.get(listForId.size() - 1);
						db_sender_id = listFroSenderId
								.get(listForId.size() - 1);

						if (size != pref.getMessageLength()) {
							Log.e("size: ", "size= " + size + " stored size: "
									+ pref.getMessageLength());
							db.open();
							pref.setMessageLenght(size);
							db.insert(db_id, db_sender_id, db_recvid,
									db_message, db_date);
							Log.e("date ", "" + db_date);
							Log.e("message ", "" + db_message);
							Log.e("sender ", "" + db_sender_id);
							Log.e("id ", "" + db_id);
							Log.e("recv id ", "" + db_recvid);
							db.close();
							getMessagesFromSqlite();
						}

					}
					new Thread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							recieveMessage();
							// Log.e("receive", "receive");
						}
					}).start();

				} catch (Exception e) {
					// TODO: handle exception
					Log.e("error:- ", "" + e);

				}

			}

			@Override
			protected void onError(IOException ex) {
				Toast.makeText(getApplicationContext(), "Error: " + ex,
						Toast.LENGTH_SHORT).show();
			}

			@Override
			protected void onFailure(Transport transport) {
				Toast.makeText(getApplicationContext(), "Error: " + transport,
						Toast.LENGTH_SHORT).show();
			}

		}.execute("GET");

	}

	public void getMessagesFromSqlite() {

		id.clear();
		senderid.clear();
		recvid.clear();
		db_message.clear();
		date.clear();

		db.open();
		Cursor cur = db.getChatMessages(userid, frndId);
		cur.moveToFirst();
		while (!cur.isAfterLast()) {
			id.add(cur.getInt(0));
			senderid.add(cur.getString(1));
			recvid.add(cur.getString(2));
			db_message.add(cur.getString(3));
			date.add(cur.getString(4));
			cur.moveToNext();
		}
		listForShowChat
				.setAdapter(new ChatListAdapter(Private_chat_screen.this));
		Log.e("Sqlite ", " Sqlite ");
		Log.e("id ", "sender id:- " + senderid);
		Log.e("id ", "recv id:- " + recvid);
		Log.e("sqlite message:= ", "message:- " + db_message);
		// Log.e("id ", "date:- " + date);

		db.close();

		// new Thread(new Runnable() {
		//
		// @Override
		// public void run() {
		// // TODO Auto-generated method stub
		// listForShowChat.setAdapter(new ChatListAdapter(
		// Private_chat_screen.this));
		// Log.e("adapter ", " adapter ");
		// }
		// }).start();

	}

	public void deleteChatTable() {
		db.open();
		db.deleteEmpList(userid);
		db.close();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
