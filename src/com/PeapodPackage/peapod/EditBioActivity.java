package com.PeapodPackage.peapod;

import java.net.URLEncoder;

import org.json.JSONObject;

import com.Peapod.Connections.ConnectionStatus;
import com.Peapod.Connections.HttpGetRespone;
import com.Peapod.Connections.HttpResponseListener;
import com.Peapod.Connections.StorePref;
import com.Peapod.Connections.TransparentProgressDialog;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class EditBioActivity extends Activity implements OnClickListener,
		HttpResponseListener {

	private Button btnForCancel;
	private Button btnForUpdate;
	private EditText edtForUserBio;
	StorePref pref;
	ConnectionStatus statusis;
	AlertDialog alertDialog;
	boolean isConnected;
	TransparentProgressDialog pdialog;
	String userBio;
	String userID;
	String updatedUserBio;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_bio_activity);

		btnForCancel = (Button) findViewById(R.id.btnForCancel);
		btnForCancel.setOnClickListener(this);
		btnForUpdate = (Button) findViewById(R.id.btnForOk);
		btnForUpdate.setOnClickListener(this);
		edtForUserBio = (EditText) findViewById(R.id.edtForBioEdit);

		pref = StorePref.getInstance(getApplicationContext());
		userBio = pref.getUserBio();
		userID = pref.getUserid();
		edtForUserBio.setText(userBio);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnForOk:
			try {
				if (TextUtils.isEmpty(edtForUserBio.getText().toString())) {
					edtForUserBio
							.setError(Html
									.fromHtml("<font color='red'>Enter your detail to update</font>"));
				} else {
					updatedUserBio = URLEncoder.encode(
							edtForUserBio.getText().toString(), "utf-8")
							.replace("+", " ");
				}
				if (updatedUserBio.equals("") || updatedUserBio.equals(null)) {
					Log.e("eorror", "no data");
				} else {
					btnForUpdate.setClickable(true);
					updateBio();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;

		case R.id.btnForCancel:
			Intent back = new Intent(getApplicationContext(), MyProfile.class);
			startActivity(back);
			finish();
			break;

		default:
			break;
		}
	}

	@SuppressWarnings("deprecation")
	public void updateBio() {
		updatedUserBio = updatedUserBio.replace("%0A", " ");
		// hit Admin Question Web API
		statusis = new ConnectionStatus(this);
		pdialog = new TransparentProgressDialog(this, R.drawable.loader);
		pdialog.show();
		isConnected = statusis.isConnected();
		if (isConnected) {
			try {
				new HttpGetRespone(
						"http://peapodapp.com/webservices/androidapi.php?type=updated_profileabout&userid="
								+ userID
								+ "&bio="
								+ updatedUserBio.replace(" ", "%20"), this, 1)
						.execute();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {

			if (pdialog.isShowing())
				pdialog.dismiss();
			alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle("Peapod");
			alertDialog.setMessage("No Internet Connection Available");
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			alertDialog.show();
		}
	}

	@Override
	public void response(String response, int page_id) {
		if (page_id == 1) {
			if (pdialog.isShowing()) {
				pdialog.dismiss();
			}
			try {

				JSONObject jsonResponse = new JSONObject(response);
				JSONObject response2 = jsonResponse.getJSONObject("list");
				System.out.println("response = " + response2);
				JSONObject response3 = response2.getJSONObject("returnCode");
				String success = response3.optString("resultText");
				Log.e("responce string", "" + success);
				if (success.equals("Success")) {
					Toast.makeText(getApplicationContext(), "Save successfuly",
							5000).show();
					pref.setUserBio(updatedUserBio);
					Intent back = new Intent(getApplicationContext(),
							MyProfile.class);
					startActivity(back);
					finish();
				}

			} catch (Exception e) {
				if (pdialog.isShowing()) {
					pdialog.dismiss();

				}
				e.printStackTrace();
			}

		}

	}

	@Override
	public void error(String error) {
		// TODO Auto-generated method stub

	}
}
