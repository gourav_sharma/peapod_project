package com.PeapodPackage.peapod;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import com.Peapod.Connections.TransparentProgressDialog;

import se.tonyivanov.android.ajax.Request;
import se.tonyivanov.android.ajax.Transport;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class contactpeapod extends Activity {

	Button back, submit;

	EditText name, email, subject, message;

	TransparentProgressDialog pdialog;

	String url = "http://peapodapp.com/webservices/androidapi.php?type=contact_peapod&email=vg@gmail.com&subject=hehheiheh&message=demo%20test&name=ubububxbsb";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contactpeapod);

		pdialog = new TransparentProgressDialog(this, R.drawable.loader);
		name = (EditText) findViewById(R.id.editTextusername);
		email = (EditText) findViewById(R.id.editTextemail);
		subject = (EditText) findViewById(R.id.editTextsubject);
		message = (EditText) findViewById(R.id.editTextmessage);

		submit = (Button) findViewById(R.id.buttonsubmit);
		back = (Button) findViewById(R.id.back_btn);
		back.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent home = new Intent(contactpeapod.this, Setting.class);
				startActivity(home);
				finish();
			}
		});

		submit.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				pdialog.show();
				Submit();

			}
		});
	}

	public void Submit() {

		String username = name.getText().toString();
		String emaill = email.getText().toString();
		String subjectt = subject.getText().toString();
		String messagee = message.getText().toString();

		if (!username.equals("")) {

			if (!emaill.equals("")) {

				if (!subjectt.equals("")) {

					if (!messagee.equals("")) {

						String Url = "http://peapodapp.com/webservices/androidapi.php?type=contact_peapod&email="
								+ emaill
								+ "&subject="
								+ subjectt
								+ "&message="
								+ messagee + "&name=" + username;

						Log.d("URLLLLL", Url);

						Request sendImageName = (Request) new Request(Url) {

							@Override
							protected void onComplete(Transport transport) {
								JSONObject response = transport
										.getResponseJson();

								if (response == null) {

									Toast.makeText(getApplicationContext(),
											"No response from server",
											Toast.LENGTH_SHORT).show();
									if (pdialog.isShowing()) {
										pdialog.dismiss();
									}
								} else {

									try {
										JSONObject lst = response
												.getJSONObject("Response");

										JSONObject rcode = lst
												.getJSONObject("returnCode");

										String success = rcode
												.getString("resultText");

										if (success.equals("Success")) {
											Toast.makeText(
													getApplicationContext(),
													"Data uploaded successfully.",
													Toast.LENGTH_SHORT).show();

											if (pdialog.isShowing()) {
												pdialog.dismiss();
											}

										} else {
											Toast.makeText(
													getApplicationContext(),
													"Image upload fail.",
													Toast.LENGTH_SHORT).show();
											if (pdialog.isShowing()) {
												pdialog.dismiss();
											}
										}
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

								}

							}

							@Override
							protected void onError(IOException ex) {

								Log.e("On error:- ", " " + ex);
								if (pdialog.isShowing()) {
									pdialog.dismiss();
								}
							}

							@Override
							protected void onFailure(Transport transport) {
								Log.e("On  failure:- ", " " + transport);
								if (pdialog.isShowing()) {
									pdialog.dismiss();
								}
							}

						}.execute("GET");

					} else {
						Toast.makeText(getApplicationContext(),
								"Enter Message First ", 1200).show();
					}
				} else {
					Toast.makeText(getApplicationContext(),
							"Enter Subject First ", 1200).show();
				}
			} else {
				Toast.makeText(getApplicationContext(), "Enter Email First ",
						1200).show();
			}
		} else {
			Toast.makeText(getApplicationContext(), "Enter UserName First ",
					1200).show();
		}

	}
}
