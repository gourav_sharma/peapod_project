package com.PeapodPackage.peapod;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import se.tonyivanov.android.ajax.Request;
import se.tonyivanov.android.ajax.Transport;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.Peapod.Connections.HttpGetRespone;
import com.Peapod.Connections.HttpResponseListener;
import com.Peapod.Connections.ImageLoader;
import com.Peapod.Connections.ImageLoaderForBigImages;
import com.Peapod.Connections.StorePref;
import com.Peapod.Connections.TransparentProgressDialog;

public class MyProfile extends Activity implements OnClickListener,
		HttpResponseListener {

	private ImageButton back_btn;
	TransparentProgressDialog pdialog;
	StorePref pref;
	String imageUrl;
	String user_name, user_bday, file_name_for_server;
	String newDate;
	String userBio, userId;
	Integer gray_light, userAge;
	ImageView imgProfile, imgForEditBio, imgForEditPic;
	ImageLoaderForBigImages imgLoader;
	TextView txtForProfileNameAndAge, txtForbio;
	String currentDate;
	private static final int PICK_IMAGE = 322;
	final static int PICK_Camera_IMAGE = 2;
	Uri imageUri = null;
	static String imagepath = null;
	Bitmap bitmap;
	private int serverResponseCode = 0;

	private final int SELECT_FILE = 1;
	private final int REQUEST_CAMERA = 0;

	private ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_profile);

		gray_light = getApplicationContext().getResources().getColor(
				R.color.gray_light);

		pdialog = new TransparentProgressDialog(getApplicationContext(),
				R.drawable.loader);

		pdialog = new TransparentProgressDialog(this, R.drawable.loader);
		back_btn = (ImageButton) findViewById(R.id.back_btn);
		back_btn.setOnClickListener(this);

		imgProfile = (ImageView) findViewById(R.id.imgForProfilePic);
		imgForEditBio = (ImageView) findViewById(R.id.imgForEditBio);
		imgForEditPic = (ImageView) findViewById(R.id.imgForEditPic);
		imgForEditBio.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					// Toast.makeText(getApplicationContext(), "text", 5000)
					// .show();
					imgForEditBio.setBackgroundColor(gray_light);
					Intent edit = new Intent(getApplicationContext(),
							EditBioActivity.class);
					startActivity(edit);
					finish();
					break;

				case MotionEvent.ACTION_UP:
					imgForEditBio.setBackgroundColor(Color.TRANSPARENT);
					break;

				default:
					break;
				}

				return true;
			}
		});

		imgForEditPic.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					imgForEditPic.setBackgroundColor(gray_light);
					selectImage();
					break;

				case MotionEvent.ACTION_UP:
					imgForEditPic.setBackgroundColor(Color.TRANSPARENT);
					break;

				default:
					break;
				}

				return true;
			}
		});

		pref = StorePref.getInstance(getApplicationContext());
		imageUrl = pref.getImageUrl();
		user_bday = pref.getUserAge();
		user_name = pref.getUserName();
		userBio = pref.getUserBio();
		userId = pref.getUserid();

		Log.e("image url", "" + imageUrl);
		Log.e("user bday", "" + user_bday + " user bday");

		imgLoader = new ImageLoaderForBigImages(getApplicationContext());
		imgLoader.DisplayImage(imageUrl, imgProfile);
		txtForProfileNameAndAge = (TextView) findViewById(R.id.txtForProfileNameAndAge);
		txtForbio = (TextView) findViewById(R.id.txtForbio);
		txtForbio.setText(userBio);

		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());
		SimpleDateFormat df = new SimpleDateFormat("yyyy");
		currentDate = df.format(c.getTime());
		String givenDateString = user_bday;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
		try {
			Date mDate = sdf.parse(givenDateString);
			java.util.Date tmpDate = sdf.parse(givenDateString);
			SimpleDateFormat formatTo = new SimpleDateFormat("yyyy");
			System.out.println("Date in  ===========================:: "
					+ mDate);
			newDate = formatTo.format(tmpDate);
			userAge = Integer.parseInt(currentDate) - Integer.parseInt(newDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		if (userAge == null) {
			txtForProfileNameAndAge.setText("" + user_name);
		} else {
			txtForProfileNameAndAge.setText("" + user_name + ", " + userAge);
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.back_btn:
			finish();
			break;

		default:
			break;
		}

	}

	private void selectImage() {

		final CharSequence[] options = { "Take Photo", "Choose from Gallery",
				"Cancel" };
		AlertDialog.Builder builder = new AlertDialog.Builder(MyProfile.this);
		builder.setTitle("Add Photo!");
		builder.setItems(options, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				if (options[item].equals("Take Photo")) {

					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

					File f = new File(android.os.Environment
							.getExternalStorageDirectory(), "temp.jpg");
					intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
					startActivityForResult(intent, REQUEST_CAMERA);

				} else if (options[item].equals("Choose from Gallery")) {
					try {

						Intent intent = new Intent(
								Intent.ACTION_PICK,
								android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

						intent.putExtra("crop", "true");
						intent.putExtra("outputX", 200);
						intent.putExtra("outputY", 200);
						intent.putExtra("aspectX", 1);
						intent.putExtra("aspectY", 1);
						intent.putExtra("scale", true);
						// pickImageIntent.putExtra(MediaStore.EXTRA_OUTPUT,
						// uriWhereToStore);
						intent.putExtra("outputFormat",

						Bitmap.CompressFormat.JPEG.toString());

						intent.setType("image/*");

						startActivityForResult(
								Intent.createChooser(intent, "Select File"),
								SELECT_FILE);

					} catch (Exception e) {
						Toast.makeText(getApplicationContext(), e.getMessage(),
								Toast.LENGTH_LONG).show();
						Log.e(e.getClass().getName(), e.getMessage(), e);
					}

				} else if (options[item].equals("Cancel")) {

					dialog.dismiss();
				}
			}
		});
		builder.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if (requestCode == REQUEST_CAMERA) {

				File f = new File(Environment.getExternalStorageDirectory()
						.toString());
				for (File temp : f.listFiles()) {
					if (temp.getName().equals("temp.jpg")) {
						f = temp;
						break;
					}
				}
				try {
					Bitmap bm;
					BitmapFactory.Options btmapOptions = new BitmapFactory.Options();

					bm = BitmapFactory.decodeFile(f.getAbsolutePath(),
							btmapOptions);

					// bm = Bitmap.createScaledBitmap(bm, 70, 70, true);
					pdialog.show();

					Drawable drawable = new BitmapDrawable(getResources(), bm);
					imgProfile.setImageDrawable(drawable);
					// imgProfile.setImageBitmap(bm);

					String path = android.os.Environment
							.getExternalStorageDirectory()
							+ File.separator
							+ "Peapodapp";
					f.delete();

					File wallpaperDirectory = new File(path);
					wallpaperDirectory.mkdirs();
					OutputStream fOut = null;
					File file = new File(wallpaperDirectory,
							String.valueOf(System.currentTimeMillis()) + ".jpg");
					imagepath = file.getAbsolutePath();

					try {
						fOut = new FileOutputStream(file);
						bm.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
						fOut.flush();
						fOut.close();
					} catch (FileNotFoundException e) {
						e.printStackTrace();

					} catch (IOException e) {
						e.printStackTrace();

					} catch (Exception e) {
						e.printStackTrace();

					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				uploadImageToServer();
			} else if (requestCode == SELECT_FILE) {

				Uri selectedImageUri = data.getData();

				Log.d("uri", "" + selectedImageUri);

				String tempPath = getPath(selectedImageUri);
				Bitmap bm;
				BitmapFactory.Options btmapOptions = new BitmapFactory.Options();
				bm = BitmapFactory.decodeFile(tempPath, btmapOptions);
				pdialog.show();

				imgProfile.setImageBitmap(bm);

				imagepath = getRealPathFromURI(selectedImageUri);

				uploadImageToServer();

			}
		}
	}

	public void uploadImageToServer() {
		if (imagepath == "") {
			Toast.makeText(getApplicationContext(), "Invalid Image path",
					Toast.LENGTH_SHORT).show();
		} else {
			final UploadToServer upload = new UploadToServer(MyProfile.this);
			new Thread(new Runnable() {
				public void run() {

					if (upload.uploadFile(imagepath) == 200) {
						String imagepath = upload.getImageName();
						// upadteImageName(imagepath);

						if (pdialog.isShowing()) {
							pdialog.dismiss();
						}

						// Toast.makeText(getApplicationContext(), "Sucess",
						// 1000)
						// .show();

						Log.d("sucessssssssssssssssss", imagepath);

						file_name_for_server = imagepath;
						// saveImageName(imagepath);
						// runOnUiThread(new Runnable() {
						// public void run() {
						// // String msg =
						// //
						// "File Upload Completed.\n\n See uploaded file here : \n\n"
						// // + " F:/wamp/wamp/www/uploads";
						// // messageText.setText(msg);
						// // Toast.makeText(MyProfile.this,
						// // "File Upload Complete.", Toast.LENGTH_SHORT)
						// // .show();
						//
						new HttpGetRespone(
								"http://www.peapodapp.com/webservices/androidapi.php?type=update_profilepic&userid="
										+ userId
										+ "&profilepic="
										+ "http://peapodapp.com/webservices/images/"
										+ file_name_for_server, MyProfile.this,
								1).execute();
						// Log.d("",
						// ""
						// +
						// "http://www.peapodapp.com/webservices/androidapi.php?type=update_profilepic&userid="
						// + userId
						// + "&profilepic="
						// + "http://peapodapp.com/webservices/images/"
						// + file_name_for_server);
						//
						// refreshActivity();
						// }
						// });
						//
					}
				}
			}).start();
		}
	}

	public void saveImageName(String imageName) {
		String url = "http://www.peapodapp.com/webservices/androidapi.php?type=update_profilepic&userid="
				+ userId
				+ "&profilepic="
				+ "http://peapodapp.com/webservices/images/" + imageName;
		// String validUrl = url.replace("", "%20");
		System.out.println("url here:-upload   " + url);

		Request sendImageName = (Request) new Request(url) {

			@Override
			protected void onComplete(Transport transport) {
				JSONObject response = transport.getResponseJson();

				if (response == null) {

					Toast.makeText(getApplicationContext(),
							"No response from server", Toast.LENGTH_SHORT)
							.show();
					if (pdialog.isShowing()) {
						pdialog.dismiss();
					}
				} else {

					try {
						JSONObject lst = response.getJSONObject("list");

						JSONObject rcode = lst.getJSONObject("returnCode");

						String success = rcode.getString("resultText");

						if (success.equals("Success")) {
							Toast.makeText(getApplicationContext(),
									"Image uploaded successfully.",
									Toast.LENGTH_SHORT).show();

							if (pdialog.isShowing()) {
								pdialog.dismiss();
							}

						} else {
							Toast.makeText(getApplicationContext(),
									"Image upload fail.", Toast.LENGTH_SHORT)
									.show();
							if (pdialog.isShowing()) {
								pdialog.dismiss();
							}
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

			}

			@Override
			protected void onError(IOException ex) {

				Log.e("On error:- ", " " + ex);
				if (pdialog.isShowing()) {
					pdialog.dismiss();
				}
			}

			@Override
			protected void onFailure(Transport transport) {
				Log.e("On  failure:- ", " " + transport);
				if (pdialog.isShowing()) {
					pdialog.dismiss();
				}
			}

		}.execute("GET");

	}

	private String getRealPathFromURI(Uri contentURI) {
		String result;
		Cursor cursor = getContentResolver().query(contentURI, null, null,
				null, null);
		if (cursor == null) { // Source is Dropbox or other similar local file
								// path
			result = contentURI.getPath();
		} else {
			cursor.moveToFirst();
			int idx = cursor
					.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
			result = cursor.getString(idx);
			cursor.close();
		}
		return result;
	}

	public String getPath(Uri uri) {
		// String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, null, null, null, null);
		if (cursor != null) {
			// HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
			// THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		} else
			return null;
	}

	public void decodeFile(String filePath) {
		// Decode image size
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(filePath, o);

		// The new size we want to scale to
		final int REQUIRED_SIZE = 1024;

		// Find the correct scale value. It should be the power of 2.
		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
				break;
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		// Decode with inSampleSize
		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		bitmap = BitmapFactory.decodeFile(filePath, o2);
		// bitmap = Bitmap.createScaledBitmap(bitmap, 100, 100, true);
		imgProfile.setImageBitmap(bitmap);
	}

	/****************************************************** upload file *****************************************************************/

	public int uploadFile(String sourceFileUri) {

		String fileName = sourceFileUri;

		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;

		File sourceFile = new File(sourceFileUri);

		String name = sourceFile.getName(); // get the name of file
		file_name_for_server = name;
		Log.e("name upload file .............", "" + name);// name uploading
															// file

		if (!sourceFile.isFile()) {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}

			// Log.e("uploadFile", "Source File not exist :" + imagepath);

			runOnUiThread(new Runnable() {
				public void run() {
					// messageText.setText("Source File not exist :" +
					// imagepath);
				}
			});

			return 0;

		} else {
			try {

				// open a URL connection to the Servlet
				FileInputStream fileInputStream = new FileInputStream(
						sourceFile);
				URL url = new URL(
						"http://peapodapp.com/webservices/androidapi.php?type=uploadimage");

				// Open a HTTP connection to the URL
				conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true); // Allow Inputs
				conn.setDoOutput(true); // Allow Outputs
				conn.setUseCaches(false); // Don't use a Cached Copy
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Connection", "Keep-Alive");
				conn.setRequestProperty("ENCTYPE", "multipart/form-data");
				conn.setRequestProperty("Content-Type",
						"multipart/form-data;boundary=" + boundary);
				conn.setRequestProperty("uploaded_file", fileName);

				dos = new DataOutputStream(conn.getOutputStream());

				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
						+ fileName + "\"" + lineEnd);

				dos.writeBytes(lineEnd);

				// create a buffer of maximum size
				bytesAvailable = fileInputStream.available();

				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];

				// read file and write it into form...
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				while (bytesRead > 0) {

					dos.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				}

				// send multipart form data necesssary after file data...
				dos.writeBytes(lineEnd);
				dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

				// Responses from the server (code and message)
				serverResponseCode = conn.getResponseCode();
				// String serverResponseMessage = conn.getResponseMessage();
				// Log.v("log_tag................name",
				// System.currentTimeMillis()
				// + ".jpg");
				// Log.i("uploadFile", "HTTP Response is : "
				// + serverResponseMessage + ": " + serverResponseCode);

				if (serverResponseCode == 200) {

					runOnUiThread(new Runnable() {
						public void run() {
							// String msg =
							// "File Upload Completed.\n\n See uploaded file here : \n\n"
							// + " F:/wamp/wamp/www/uploads";
							// messageText.setText(msg);
							// Toast.makeText(MyProfile.this,
							// "File Upload Complete.", Toast.LENGTH_SHORT)
							// .show();

							new HttpGetRespone(
									"http://www.peapodapp.com/webservices/androidapi.php?type=update_profilepic&userid="
											+ userId
											+ "&profilepic="
											+ "http://peapodapp.com/webservices/images/"
											+ file_name_for_server,
									MyProfile.this, 1).execute();
							Log.d("",
									""
											+ "http://www.peapodapp.com/webservices/androidapi.php?type=update_profilepic&userid="
											+ userId
											+ "&profilepic="
											+ "http://peapodapp.com/webservices/images/"
											+ file_name_for_server);
						}
					});
				}

				// close the streams //
				fileInputStream.close();
				dos.flush();
				dos.close();

			} catch (MalformedURLException ex) {
				if (progressDialog.isShowing()) {
					progressDialog.dismiss();
				}
				ex.printStackTrace();

				runOnUiThread(new Runnable() {
					public void run() {
						// messageText
						// .setText("MalformedURLException Exception : check script url.");
						Toast.makeText(MyProfile.this, "MalformedURLException",
								Toast.LENGTH_SHORT).show();
					}
				});

				Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
			} catch (Exception e) {

				if (progressDialog.isShowing()) {
					progressDialog.dismiss();
				}

				e.printStackTrace();

				runOnUiThread(new Runnable() {
					public void run() {
						// messageText.setText("Got Exception : see logcat ");
						Toast.makeText(MyProfile.this,
								"Got Exception : see logcat ",
								Toast.LENGTH_SHORT).show();
					}
				});
				Log.e("Upload file to server Exception",
						"Exception : " + e.getMessage(), e);
			}
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			return serverResponseCode;

		} // End else block
	}

	/**************************************************** upload file end **********************************************************/

	@Override
	public void onBackPressed() {

	}

	@Override
	public void response(String response, int page_id) {
		System.out.print("Recponse:- " + response);
		if (page_id == 1) {
			if (pdialog.isShowing()) {
				pdialog.dismiss();
			}
			try {
				JSONObject jsonResponse = new JSONObject(response);
				JSONObject jsonResponse2 = jsonResponse.getJSONObject("list");
				JSONObject jsonResponse3 = jsonResponse2
						.getJSONObject("returnCode");

				String status = jsonResponse3.optString("result");
				if (status.equals("0")) {
					Toast.makeText(getApplicationContext(),
							"Profile Picture Updated Successfully", 5000)
							.show();

					pref.setImageUrl("http://peapodapp.com/webservices/images/"
							+ file_name_for_server);
					// Intent backIntent = new Intent(
					// Edit_loyality_card_user.this,
					// Myloyality_cards.class);
					// startActivity(backIntent);
					// finish();
				} else {
					if (pdialog.isShowing()) {
						pdialog.dismiss();
					}
				}
			} catch (Exception e) {
				if (pdialog.isShowing()) {
					pdialog.dismiss();
				}
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	@Override
	public void error(String error) {
		// TODO Auto-generated method stub

	}

	public void refreshActivity() {
		Intent refresh = new Intent(MyProfile.this, MyProfile.class);
		startActivity(refresh);
		finish();
	}

}
