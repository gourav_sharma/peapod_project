package com.PeapodPackage.peapod;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONObject;

import com.Peapod.Connections.ConnectionStatus;
import com.Peapod.Connections.HttpGetRespone;
import com.Peapod.Connections.HttpResponseListener;
import com.Peapod.Connections.ImageLoader;
import com.Peapod.Connections.StorePref;
import com.Peapod.Connections.TransparentProgressDialog;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class HomeScreen extends Activity implements OnClickListener,
		HttpResponseListener {

	private ImageButton imgForSetting;
	private ImageView chat_img, pea_image, quiz, logo2, firstimg, secondimg,
			thirdimg, forthimg;
	String getImageUrl, userId, name, image, userIdForQuestion, userBio,
			timeStatus;
	int requestsentStatus;
	public ImageLoader imageLoader;
	SharedPreferences fb_pref;
	StorePref pref;
	private TextView txtForname, txtForBio, txtForInterest, txtDistance,
			txtForActiveTime;
	long displayTime;
	double myLat, myLong, frndLat, frndLong;

	ConnectionStatus statusis;
	AlertDialog alertDialog;
	boolean isConnected;
	TransparentProgressDialog pdialog;
	ImageView imgForPass;
	int requestCounter = 0;
	int getQuestionCount, getSettingCount;
	Boolean status = false;

	Dialog dialog;
	// Session session;

	private ArrayList<String> listForImagesUrlOfFriends = new ArrayList<String>();

	Animation blinkAnimation;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_screen);

		imgForSetting = (ImageButton) findViewById(R.id.setting_btn);
		imgForSetting.setOnClickListener(this);
		imgForPass = (ImageView) findViewById(R.id.pass);
		imgForPass.setOnClickListener(this);
		chat_img = (ImageView) findViewById(R.id.chat_img);
		chat_img.setOnClickListener(this);
		logo2 = (ImageView) findViewById(R.id.logo2);
		logo2.setOnClickListener(this);

		dialog = new Dialog(HomeScreen.this);

		firstimg = (ImageView) findViewById(R.id.firstImage);
		secondimg = (ImageView) findViewById(R.id.secondImage);
		thirdimg = (ImageView) findViewById(R.id.thirdImage);
		forthimg = (ImageView) findViewById(R.id.forthImage);
		txtForBio = (TextView) findViewById(R.id.txtForBio);
		txtForActiveTime = (TextView) findViewById(R.id.txtForActiveTime);
		txtForInterest = (TextView) findViewById(R.id.txtForInterst);
		txtDistance = (TextView) findViewById(R.id.txtDistance);

		// imageLoader.DisplayImage(
		// listForImagesUrlOfFriends.get(0), firstimg);
		// imageLoader.DisplayImage(
		// listForImagesUrlOfFriends.get(1), secondimg);
		// imageLoader.DisplayImage(
		// listForImagesUrlOfFriends.get(2), thirdimg);
		// imageLoader.DisplayImage(
		// listForImagesUrlOfFriends.get(3), forthimg);

		blinkAnimation = AnimationUtils.loadAnimation(getApplicationContext(),
				R.anim.blink);

		quiz = (ImageView) findViewById(R.id.quiz);
		quiz.setOnClickListener(this);

		pea_image = (ImageView) findViewById(R.id.pea_image);
		// getImageUrl = getIntent().getStringExtra("keyForImageUrl");
		imageLoader = new ImageLoader(getApplicationContext());

		pref = StorePref.getInstance(this);
		// getImageUrl = pref.getImageUrl();
		userId = pref.getUserid();
		Log.e("user id ---------------------------------", "" + userId);
		// pea_image.setOnClickListener(this);
		//
		// imageLoader.DisplayImage(getImageUrl, pea_image);
		// Log.e("url of image", "" + getImageUrl);

		txtForname = (TextView) findViewById(R.id.txt1);

		pdialog = new TransparentProgressDialog(this, R.drawable.loader);

		statusis = new ConnectionStatus(this);
		pdialog = new TransparentProgressDialog(this, R.drawable.loader);
		pdialog.show();
		isConnected = statusis.isConnected();
		if (isConnected) {
			try {

				new HttpGetRespone(
						"http://peapodapp.com/webservices/androidapi.php?type=getprofile&userid="
								+ userId, this, 2).execute();

				Log.d("urllllllllllllllllllllll",
						"http://peapodapp.com/webservices/androidapi.php?type=getprofile&userid="
								+ userId);

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {

			if (pdialog.isShowing())
				pdialog.dismiss();
			alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle("Warning");
			alertDialog.setMessage("No Internet Connection Available");
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			alertDialog.show();
		}

		/* make the API call */
		// new Request(session, "/163320833713197/photos", null, HttpMethod.GET,
		// new Request.Callback() {
		// public void onCompleted(Response response) {
		// Log.e("----------------------------response",
		// "--------------" + response);
		// }
		// }).executeAsync();
		firstimg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (listForImagesUrlOfFriends.isEmpty()) {
					pea_image.setImageResource(R.drawable.logoicon);
				} else {
					imageLoader.DisplayImage(listForImagesUrlOfFriends.get(0),
							pea_image);
				}
				// String a = listForImagesUrlOfFriends.get(0).toString();
				//
				// if (a != null && a.length() > 0) {
				//
				// imageLoader.DisplayImage(listForImagesUrlOfFriends.get(0),
				// pea_image);
				//
				// } else {
				// pea_image.setBackgroundResource(R.drawable.logoicon);
				// }
			}
		});
		secondimg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (listForImagesUrlOfFriends.isEmpty()) {
					pea_image.setImageResource(R.drawable.logoicon);
				} else {
					imageLoader.DisplayImage(listForImagesUrlOfFriends.get(1),
							pea_image);

				}
			}
		});
		thirdimg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (listForImagesUrlOfFriends.isEmpty()) {
					pea_image.setImageResource(R.drawable.logoicon);
				} else {
					imageLoader.DisplayImage(listForImagesUrlOfFriends.get(2),
							pea_image);
				}
			}
		});
		forthimg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (listForImagesUrlOfFriends.isEmpty()) {
					pea_image.setImageResource(R.drawable.logoicon);
				} else {
					imageLoader.DisplayImage(listForImagesUrlOfFriends.get(3),
							pea_image);
				}
			}
		});

	}

	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.setting_btn:
			Intent setting_page = new Intent(HomeScreen.this, Setting.class);
			startActivity(setting_page);
			finish();
			break;

		case R.id.chat_img:
			Intent peapod_chat_page = new Intent(HomeScreen.this,
					Peapod_chat_screen.class);
			startActivity(peapod_chat_page);
			break;

		// case R.id.pea_image:
		// Intent found_pea = new Intent(HomeScreen.this,
		// Found_a_pea_activity.class);
		// startActivity(found_pea);
		// break;

		case R.id.quiz:
			if (requestsentStatus == 0) {
				Intent quiz_screen_start = new Intent(HomeScreen.this,
						Quiz_screen.class);
				quiz_screen_start.putExtra("keyForUserID", userIdForQuestion);
				startActivity(quiz_screen_start);
				finish();
			}

			break;

		case R.id.pass:
			if (status == false) {
				pdialog.show();
				isConnected = statusis.isConnected();
				if (isConnected) {
					try {
						new HttpGetRespone(
								"http://peapodapp.com/webservices/androidapi.php?type=getuserlists&userid="
										+ userId + "&freindid="
										+ userIdForQuestion, this, 1).execute();
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {

					if (pdialog.isShowing())
						pdialog.dismiss();
					alertDialog = new AlertDialog.Builder(this).create();
					alertDialog.setTitle("Warning");
					alertDialog.setMessage("No Internet Connection Available");
					alertDialog.setButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
								}
							});
					alertDialog.show();
				}
			}

			break;

		case R.id.logo2:
			Intent request_screen = new Intent(this, Request_Activity.class);
			startActivity(request_screen);
			break;

		default:
			break;
		}
	}

	@Override
	public void onBackPressed() {
		// if (dialog.isShowing()) {
		// dialog.setCancelable(false);
		// }

	}

	@Override
	public void response(String response, int page_id) {
		System.out.print("page  response here:- " + response);
		if (page_id == 1) {
			if (pdialog.isShowing()) {
				pdialog.dismiss();
			}
			try {
				JSONObject jsonResponse = new JSONObject(response);
				JSONArray jsonMainNode = jsonResponse.optJSONArray("list");
				for (int i = 0; i < jsonMainNode.length(); i++) {
					JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);

					name = jsonChildNode.optString("fullname");
					image = jsonChildNode.optString("profilepic");
					userIdForQuestion = jsonChildNode.optString("id");
					requestCounter = jsonChildNode.optInt("countrequests");
					userBio = jsonChildNode.optString("bio");
					frndLat = jsonChildNode.optDouble("lat");
					frndLong = jsonChildNode.optDouble("longt");
					String createdTime = jsonChildNode.optString("created");
					String currentTime = jsonChildNode.optString("displaydate");
					requestsentStatus = jsonChildNode.optInt("requestsent");

					if (requestCounter != 0) {
						logo2.setAnimation(blinkAnimation);
					}

					if (requestsentStatus != 0) {
						quiz.setBackgroundResource(R.drawable.quiz_disable);
					} else {
						quiz.setBackgroundResource(R.drawable.quiz_button);
					}

					JSONArray imagesArray = jsonChildNode
							.optJSONArray("images");

					SimpleDateFormat formatter = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					String dateInString = createdTime;

					Date date = formatter.parse(dateInString);
					Date cadte = formatter.parse(currentTime);

					String difff = getTimeDiff(cadte, date);

					String[] ReqTimeArray = difff.split(",");

					Integer Reqhours = Integer.parseInt(ReqTimeArray[0]);
					Integer Resmins = Integer.parseInt(ReqTimeArray[1]);

					Log.e(" ckeck time diff:-  ", "  current time:-   "
							+ currentTime + "  created time:- " + createdTime);

					if (Reqhours == 0) {
						txtForActiveTime.setText("In the pod " + Resmins
								+ " mins ago");
					} else if (Reqhours >= 24 && Reqhours < 168) {
						int days = Reqhours / 24;
						txtForActiveTime.setText("In the pod " + days
								+ " days ago");
					}

					else if (Reqhours >= 168 && Reqhours < 720) {
						int week = Reqhours / 168;
						txtForActiveTime.setText("In the pod " + week
								+ " week ago");
					} else {
						txtForActiveTime.setText("In the pod " + Reqhours
								+ " hours ago");
					}

					Log.e(" ckeck time diff2:-  ", "  Reqhours:-   " + Reqhours
							+ "  Resmins:- " + Resmins);

					if (imagesArray.length() == 0) {
						firstimg.setImageResource(R.drawable.logoicon);
						secondimg.setImageResource(R.drawable.logoicon);
						thirdimg.setImageResource(R.drawable.logoicon);
						forthimg.setImageResource(R.drawable.logoicon);
					}

					else if (imagesArray.length() == 1) {
						firstimg.setImageResource(R.drawable.logoicon);
						secondimg.setImageResource(R.drawable.logoicon);
						thirdimg.setImageResource(R.drawable.logoicon);
					} else if (imagesArray.length() == 2) {
						firstimg.setImageResource(R.drawable.logoicon);
						secondimg.setImageResource(R.drawable.logoicon);
					} else if (imagesArray.length() == 3) {
						firstimg.setImageResource(R.drawable.logoicon);
						;
					}

					else {
						listForImagesUrlOfFriends.clear();
						for (int j = 0; j < imagesArray.length(); j++) {
							JSONObject getImageUrl = imagesArray
									.optJSONObject(j);
							String ImageUrl = getImageUrl.optString("imageurl");

							listForImagesUrlOfFriends.add(ImageUrl);
							System.out.println("image list here:- "
									+ listForImagesUrlOfFriends);
						}

						imageLoader.DisplayImage(
								listForImagesUrlOfFriends.get(0), firstimg);
						imageLoader.DisplayImage(
								listForImagesUrlOfFriends.get(1), secondimg);
						imageLoader.DisplayImage(
								listForImagesUrlOfFriends.get(2), thirdimg);
						imageLoader.DisplayImage(
								listForImagesUrlOfFriends.get(3), forthimg);
					}

				}

				String str = name;
				int i = str.indexOf(" ");
				if (i != -1) {
					str = str.substring(0, i);
				}
				txtForname.setText(str);
				txtForBio.setText("Bio: " + userBio);
				imageLoader.DisplayImage(image, pea_image);
				txtDistance.setText(""
						+ distance(myLat, myLong, frndLat, frndLong, 'k')
						+ " miles");

			} catch (Exception e) {
				e.printStackTrace();
				if (pdialog.isShowing()) {
					pdialog.dismiss();
				}
			}
		}

		if (page_id == 2) {
			if (pdialog.isShowing()) {
				pdialog.dismiss();
			}
			try {
				JSONObject jsonResponse = new JSONObject(response);
				JSONArray jsonMainNode = jsonResponse.optJSONArray("list");
				for (int i = 0; i < jsonMainNode.length(); i++) {
					JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
					String profile_pic = jsonChildNode.optString("profilepic");
					String bioData = jsonChildNode.optString("bio");
					getQuestionCount = jsonChildNode.optInt("userquestions");
					getSettingCount = jsonChildNode.optInt("settings");
					myLat = jsonChildNode.optDouble("lat");
					myLong = jsonChildNode.optDouble("longt");

					pref.setImageUrl(profile_pic);
					pref.setUserBio(bioData);
					pref.setMyLat((float) myLat);
					pref.setMyLong((float) myLong);

				}
				if (getQuestionCount < 5

				|| getSettingCount < 5) {
					Log.d("popup show:- ", "Questions:- " + getQuestionCount
							+ "Setting:- " + getSettingCount);
					status = true;

					if (pdialog.isShowing()) {
						pdialog.dismiss();
					}
					// Create custom dialog object

					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					dialog.setContentView(R.layout.home_setting_popup);
					dialog.setCanceledOnTouchOutside(false);
					dialog.setCancelable(false);
					// Set dialog title
					// dialog.setTitle("Peapod");

					TextView txtForpopup = (TextView) dialog
							.findViewById(R.id.txtForpopup);

					txtForpopup
							.setText(""
									+ "Click on Quiz Manager and Visibility to get users.");

					Button btnForWindowClose = (Button) dialog
							.findViewById(R.id.btnForWindowClose);

					btnForWindowClose.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							dialog.dismiss();
						}
					});

					Button btnForVisibility = (Button) dialog
							.findViewById(R.id.btnForVisibility);
					if (getSettingCount == 5) {
						btnForVisibility.setVisibility(View.GONE);
						txtForpopup.setText(""
								+ "Click on Quiz Manager to get users.");
					}
					btnForVisibility.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {

							Intent visibility = new Intent(
									getApplicationContext(),
									Visibility_Setting.class);
							startActivity(visibility);
							finish();

						}
					});

					Button btnForQm = (Button) dialog
							.findViewById(R.id.btnForQm);
					if (getQuestionCount >= 5) {
						btnForQm.setVisibility(View.GONE);
						txtForpopup.setText(""
								+ "Click on Visibility to get users.");
					}
					btnForQm.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							Intent qm = new Intent(getApplicationContext(),
									Quiz_manager.class);
							startActivity(qm);
							finish();
						}
					});

					dialog.show();
				} else {
					status = false;
					try {
						new HttpGetRespone(
								"http://peapodapp.com/webservices/androidapi.php?type=getuserlists&userid="
										+ userId + "&freindid=" + userId, this,
								1).execute();

						// txtDistance.setText(""
						// + distance(myLat, myLong, frndLat, frndLong,
						// 'k'));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
				if (pdialog.isShowing()) {
					pdialog.dismiss();
				}
			}
		}

	}

	public void refreshActivity() {
		Intent refresh = new Intent(HomeScreen.this, HomeScreen.class);
		startActivity(refresh);
		finish();
	}

	@Override
	public void error(String error) {
		// TODO Auto-generated method stub

	}

	private double distance(double lat1, double lon1, double lat2, double lon2,
			char unit) {
		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2))
				+ Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2))
				* Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		if (unit == 'K') {
			dist = dist * 1.609344;
		} else if (unit == 'N') {
			dist = dist * 0.8684;
		}
		dist = dist * 0.621371;
		dist = Math.round(dist * 100.0) / 100.0;
		return (dist);
	}

	private double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
	/* :: This function converts radians to decimal degrees : */
	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
	private double rad2deg(double rad) {
		return (rad * 180 / Math.PI);
	}

	public String getTimeDiff(Date dateOne, Date dateTwo) {
		String diff;
		long timeDiff = Math.abs(dateOne.getTime() - dateTwo.getTime());
		diff = String.format(
				"%d,%d",
				TimeUnit.MILLISECONDS.toHours(timeDiff),
				TimeUnit.MILLISECONDS.toMinutes(timeDiff)
						- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
								.toHours(timeDiff)));
		return diff;
	}
}