package com.Peapod.Connections;

import org.json.JSONObject;

import com.Peapod.Connections.HttpResponseListener;

import android.os.AsyncTask;
import android.util.Log;

public class HttpGetRespone extends AsyncTask<String, Integer, String> {
	int page;
	HttpResponseListener listener;
	String url;
	String status;

	public HttpGetRespone(String url, HttpResponseListener lisener, int i) {
		this.page = i;
		this.listener = lisener;
		this.url = url;
		// http://www.citemenu.com/Loyalty/mobileservice.php?action=get_my_stash_list&userid=
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		JsonParser parser = new JsonParser();
		JSONObject response = parser.getJSONfromUrl(url);
		String getResponse = response.toString();
		Log.e("REsPONSE", "" + getResponse);
		// status=response.optString("success");

		return getResponse;
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		listener.response(result, page);
		Log.e("page:- ", "" + page);

		super.onPostExecute(result);
	}

}
