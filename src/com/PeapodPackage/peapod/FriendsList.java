package com.PeapodPackage.peapod;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import se.tonyivanov.android.ajax.Request;
import se.tonyivanov.android.ajax.Transport;

import com.Peapod.Connections.ImageLoaderRound;
import com.Peapod.Connections.StorePref;
import com.Peapod.Connections.TransparentProgressDialog;
import com.Peapod.SqliteDb.FriendListSearch;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import android.widget.Toast;

public class FriendsList extends Activity implements OnClickListener,
		OnItemClickListener, OnQueryTextListener {

	private TransparentProgressDialog pdialog;
	private StorePref pref;
	FriendListSearch obj_friend_list_search;
	ArrayList<FriendListSearch> friend_list_search_arrayList = new ArrayList<FriendListSearch>();
	ListAdapter adp;
	String userid;
	private SearchView search_view_for_search;
	private Button back_btn;
	ListView list_for_pea_chat;
	private EditText Edttxt;
	ArrayList<FriendListSearch> filterList;
	private TextView txt_for_pea_name, txt_for_pea_status;
	private ImageView peaChatImage;
	private ImageLoaderRound imgLoader;
	ArrayList<FriendListSearch> mStringFilterList;
	private ArrayList<String> listForId = new ArrayList<String>();
	private ArrayList<String> listForName = new ArrayList<String>();
	private ArrayList<String> listForImage = new ArrayList<String>();
	private ArrayList<String> listForFrndId = new ArrayList<String>();

	private String[] pea_status = { "Haha, yeah i thought th...",
			"Haha, yeah i thought th...", "Haha, yeah i thought th...",
			"Haha, yeah i thought th...", "Haha, yeah i thought th...",
			"Haha, yeah i thought th...", "Haha, yeah i thought th...",
			"Haha, yeah i thought th...", "Haha, yeah i thought th...",
			"Haha, yeah i thought th...", "Haha, yeah i thought th...",
			"Haha, yeah i thought th...", "Haha, yeah i thought th...",
			"Haha, yeah i thought th...", "Haha, yeah i thought th...",
			"Haha, yeah i thought th...", "Haha, yeah i thought th...",
			"Haha, yeah i thought th...", "Haha, yeah i thought th...",
			"Haha, yeah i thought th...", "Haha, yeah i thought th...",
			"Haha, yeah i thought th...", "Haha, yeah i thought th...",
			"Haha, yeah i thought th..." };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.friends_list);

		pdialog = new TransparentProgressDialog(this, R.drawable.loader);
		back_btn = (Button) findViewById(R.id.back_btn);
		back_btn.setOnClickListener(this);

		Edttxt = (EditText) findViewById(R.id.txt);

		list_for_pea_chat = (ListView) findViewById(R.id.list_for_chat_matches);
		list_for_pea_chat.setOnItemClickListener(this);

		search_view_for_search = (SearchView) findViewById(R.id.search_view_for_search);
		search_view_for_search.setOnQueryTextListener(this);
		imgLoader = new ImageLoaderRound(getApplicationContext());

		pref = StorePref.getInstance(getApplicationContext());
		userid = pref.getUserid();
		getFriendsList();
		pdialog.show();

	}

	public void adapter() {

		adp = new ListAdapter(FriendsList.this, friend_list_search_arrayList);
		list_for_pea_chat.setAdapter(adp);

	}

	public class ListAdapter extends BaseAdapter implements Filterable {

		private Activity activity;
		private LayoutInflater inflater = null;

		ArrayList<FriendListSearch> friend_list_search_arrayList;

		ValueFilter valueFilter;

		public ListAdapter(Activity a,
				ArrayList<FriendListSearch> friend_list_search_arrayList) {
			activity = a;
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			this.friend_list_search_arrayList = friend_list_search_arrayList;
			mStringFilterList = friend_list_search_arrayList;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return friend_list_search_arrayList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return friend_list_search_arrayList.indexOf(getItem(position));
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			View vi = convertView;
			if (convertView == null)
				vi = inflater.inflate(R.layout.peapod_chat_screen_list_layout,
						null);

			txt_for_pea_name = (TextView) vi
					.findViewById(R.id.txt_for_pea_name);
			txt_for_pea_status = (TextView) vi
					.findViewById(R.id.txt_for_pea_status);
			peaChatImage = (ImageView) vi.findViewById(R.id.pea_chat_img);

			FriendListSearch FriendlistSearch = friend_list_search_arrayList
					.get(position);

			txt_for_pea_name.setText(" " + FriendlistSearch.getFriendName());
			txt_for_pea_status.setText("" + FriendlistSearch.getLastMessage());
			imgLoader.DisplayImage("" + FriendlistSearch.getImage(),
					peaChatImage);
			return vi;
		}

		private class ValueFilter extends Filter {
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults results = new FilterResults();

				if (constraint != null && constraint.length() > 0) {
					filterList = new ArrayList<FriendListSearch>();
					for (int i = 0; i < mStringFilterList.size(); i++) {
						if ((mStringFilterList.get(i).getFriendName()
								.toUpperCase()).contains(constraint.toString()
								.toUpperCase())) {

							FriendListSearch FriendListSearch = new FriendListSearch(
									mStringFilterList.get(i).getImage(),
									mStringFilterList.get(i).getFriendName(),
									mStringFilterList.get(i).getLastMessage(),
									mStringFilterList.get(i).getConnectDate(),
									mStringFilterList.get(i).getID());

							filterList.add(FriendListSearch);
						}
					}
					results.count = filterList.size();
					results.values = filterList;

					list_for_pea_chat
							.setOnItemClickListener(new OnItemClickListener() {

								@Override
								public void onItemClick(AdapterView<?> arg0,
										View arg1, int position, long arg3) {

									Intent private_chat_screen_page = new Intent(
											FriendsList.this,
											Private_chat_screen.class);

									private_chat_screen_page
											.putExtra("keyForImage", filterList
													.get(position).getImage());
									private_chat_screen_page.putExtra(
											"keyForName",
											filterList.get(position)
													.getFriendName());
									private_chat_screen_page.putExtra(
											"keyForFrndId",
											filterList.get(position).getID());
									startActivity(private_chat_screen_page);

								}
							});

				} else {
					results.count = mStringFilterList.size();
					results.values = mStringFilterList;
				}
				return results;

			}

			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				friend_list_search_arrayList = (ArrayList<FriendListSearch>) results.values;
				notifyDataSetChanged();
			}

		}

		@Override
		public Filter getFilter() {
			if (valueFilter == null) {
				valueFilter = new ValueFilter();
			}
			return valueFilter;
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.back_btn:
			finish();
			break;

		default:
			break;
		}

	}

	public void getFriendsList() {

		String url = "http://peapodapp.com/webservices/androidapi.php?type=getcontacts&userid="
				+ userid;

		Request getList = (Request) new Request(url) {

			@Override
			protected void onSuccess(Transport transport) {

				JSONObject response = transport.getResponseJson();
				Log.e("response here:- ", "Response:- " + response);
				try {
					if (response == null) {
						Toast.makeText(getApplicationContext(),
								"No response from server", Toast.LENGTH_LONG)
								.show();
						if (pdialog.isShowing()) {
							pdialog.dismiss();
						}
					} else {
						JSONArray getArray = response.optJSONArray("list");
						listForFrndId.clear();
						listForId.clear();
						listForImage.clear();
						listForName.clear();

						for (int i = 0; i < getArray.length(); i++) {
							JSONObject getdata = getArray.optJSONObject(i);

							String name = getdata.optString("fullname");
							String id = getdata.optString("freindid");
							String image = getdata.optString("profilepic");
							String friendId = getdata.optString("freindid");
							String last_message = getdata.optString("message");
							String connect_date = getdata.optString("join");
							// String status = getdata.optString("");

							listForFrndId.add(friendId);
							listForId.add(id);
							listForImage.add(image);
							listForName.add(name);

							obj_friend_list_search = new FriendListSearch(
									image, name, last_message, connect_date, id);
							friend_list_search_arrayList.add(i,
									obj_friend_list_search);

						}
						adapter();
						Edttxt.setHint("Search " + listForFrndId.size()
								+ " Matches");
						if (pdialog.isShowing()) {
							pdialog.dismiss();
						}
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					if (pdialog.isShowing()) {
						pdialog.dismiss();
					}
				}

			}

			@Override
			protected void onFailure(Transport transport) {
				// TODO Auto-generated method stub
				Log.e("on failur: ", "on failur:- " + transport);
				if (pdialog.isShowing()) {
					pdialog.dismiss();
				}
			}

			@Override
			protected void onError(IOException ex) {
				// TODO Auto-generated method stub
				ex.printStackTrace();
				if (pdialog.isShowing()) {
					pdialog.dismiss();
				}
			}
		}.execute("GET");
	}

	@Override
	public void onBackPressed() {

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

		Intent private_chat_screen_page = new Intent(FriendsList.this,
				Private_chat_screen.class);

		private_chat_screen_page.putExtra("keyForImage",
				friend_list_search_arrayList.get(position).getImage());
		private_chat_screen_page.putExtra("keyForName",
				friend_list_search_arrayList.get(position).getFriendName());
		private_chat_screen_page.putExtra("keyForFrndId",
				friend_list_search_arrayList.get(position).getID());
		startActivity(private_chat_screen_page);

	}

	@Override
	public boolean onQueryTextChange(String newText) {
		// TODO Auto-generated method stub
		adp.getFilter().filter(newText);
		return false;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		// TODO Auto-generated method stub

		return false;
	}

}
