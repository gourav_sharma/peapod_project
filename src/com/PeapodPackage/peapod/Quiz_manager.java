package com.PeapodPackage.peapod;

import java.io.IOException;
import java.security.PublicKey;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import se.tonyivanov.android.ajax.Request;
import se.tonyivanov.android.ajax.Transport;

import com.Peapod.Connections.ConnectionStatus;
import com.Peapod.Connections.HttpGetRespone;
import com.Peapod.Connections.HttpResponseListener;
import com.Peapod.Connections.StorePref;
import com.Peapod.Connections.TransparentProgressDialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Quiz_manager extends Activity implements OnClickListener,
		HttpResponseListener {

	private ListView listViewForQuiz;
	public GridView gridForChk;
	private RelativeLayout benchmark;
	int bnchmrk;
	int bnchValue;
	StorePref pref;
	TextView txtForOptionOne, txtForOptionTwo, txtForBenchMark;

	private Button new_form;
	private ImageButton back_arrow;
	private Button popup_cancel, btnForNext, edit_btn;
	Animation animMove;
	ConnectionStatus statusis;
	Button btnForCloseWindow;
	Boolean status = true;
	public gridAdapter adapter;
	AlertDialog alertDialog;
	CheckBox chkForAll;
	boolean isConnected;
	TransparentProgressDialog pdialog;
	private String option_one;
	private String option_two;
	private String answer;
	int benchMarkValue;

	private String option_one_for_list;
	private String option_two_for_list;
	private String answer_for_list;
	private String id_for_list;

	private String userID;
	int green;
	private ArrayList<String> listForListCategory = new ArrayList<String>();
	private ArrayList<String> listForOptionOne = new ArrayList<String>();
	private ArrayList<String> listForOptionTwo = new ArrayList<String>();
	private ArrayList<String> listForListAnswer = new ArrayList<String>();
	private ArrayList<String> listForListId = new ArrayList<String>();
	private ArrayList<Integer> listForListSno = new ArrayList<Integer>();
	private ArrayList<String> listForCategoryId = new ArrayList<String>();
	private ArrayList<String> listForCategoryIdAdd = new ArrayList<String>();

	private String adminQuestionURL = "http://peapodapp.com/webservices/androidapi.php?type=adminquestions&catid=";
	private String listCategoryURL = "http://peapodapp.com/webservices/androidapi.php?type=listcategory";
	private String add_question_answerURL = "http://peapodapp.com/webservices/androidapi.php?type=addquestions&userid=";
	private String userQuestionListURL = "http://peapodapp.com/webservices/androidapi.php?type=userquestionslist&userid=";

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.quiz_manager);

		green = getApplicationContext().getResources().getColor(R.color.green);

		edit_btn = (Button) findViewById(R.id.edit_btn);
		edit_btn.setOnClickListener(this);
		listViewForQuiz = (ListView) findViewById(R.id.list_for_quiz);
		txtForBenchMark = (TextView) findViewById(R.id.txtForBenchMark);
		benchmark = (RelativeLayout) findViewById(R.id.benchmark);
		benchmark.setOnClickListener(this);

		// ListView l = getListView();
		listViewForQuiz.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
		listViewForQuiz.setStackFromBottom(true);
		// listViewForQuiz.setAdapter(new ListAdapterNew(this, listForListId,
		// listForOptionOne, listForOptionTwo, listForListAnswer,
		// listForListSno));

		back_arrow = (ImageButton) findViewById(R.id.arrow);
		back_arrow.setOnClickListener(this);

		new_form = (Button) findViewById(R.id.new_form);
		new_form.setOnClickListener(this);

		pref = StorePref.getInstance(this);
		userID = pref.getUserid();
		Log.e("user ID", "" + userID);

		animMove = AnimationUtils.loadAnimation(getApplicationContext(),
				R.anim.move);

		// hit Admin Question Web API
		statusis = new ConnectionStatus(this);
		pdialog = new TransparentProgressDialog(this, R.drawable.loader);
		pdialog.show();
		isConnected = statusis.isConnected();
		if (isConnected) {
			try {
				new HttpGetRespone(adminQuestionURL + listForCategoryIdAdd,
						this, 1).execute();
				new HttpGetRespone(listCategoryURL, this, 2).execute();
				new HttpGetRespone(userQuestionListURL + userID, this, 4)
						.execute();
				Log.e("admin question url:- ", " " + adminQuestionURL
						+ listForCategoryIdAdd);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {

			if (pdialog.isShowing())
				pdialog.dismiss();
			alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle("StashApp");
			alertDialog.setMessage("No Internet Connection Available");
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			alertDialog.show();
		}

	}

	public class ListAdapterNew extends BaseAdapter {

		private Activity activity;
		ArrayList<String> idAdap;
		ArrayList<String> opt1Adap;
		ArrayList<String> opt2Adap;
		ArrayList<String> ansAdap;
		ArrayList<Integer> countAdap;
		private LayoutInflater inflater = null;

		public ListAdapterNew(Activity a, ArrayList<String> id,
				ArrayList<String> opt1, ArrayList<String> opt2,
				ArrayList<String> ans, ArrayList<Integer> coun) {

			activity = a;
			idAdap = id;
			opt1Adap = opt1;
			opt2Adap = opt2;
			ansAdap = ans;
			countAdap = coun;
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			Log.d("------------------------------------------", ""
					+ listForListId.size());
			int count;
			if (listForListId.size() <= 7) {
				count = listForListId.size();
			} else {
				count = listForListId.size() - 1;
			}
			return count;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub

			View vi = convertView;
			if (convertView == null)
				vi = inflater.inflate(R.layout.quiz_manager_list_layout, null);
			// imgForCheck = (ImageView) vi.findViewById(R.id.imgForCheck);
			TextView textForCount = (TextView) vi
					.findViewById(R.id.txtForSerialNo);

			final EditText textForOption1 = (EditText) vi
					.findViewById(R.id.txtForOption1);
			final EditText textForOption2 = (EditText) vi
					.findViewById(R.id.txtForOption2);

			ImageView chkBox = (ImageView) vi.findViewById(R.id.imgForCheckbox);
			ImageView chkBox2 = (ImageView) vi
					.findViewById(R.id.imgForCheckbox2);
			final ImageView chk = (ImageView) vi.findViewById(R.id.imgForCheck);
			final ImageView chk2 = (ImageView) vi
					.findViewById(R.id.imgForCheck2);
			chkBox.setVisibility(View.GONE);
			chkBox2.setVisibility(View.GONE);
			// chk.setVisibility(View.VISIBLE);
			// chk2.setVisibility(View.VISIBLE);

			int size = countAdap.size();
			if (position == size - 1 && listForListSno.size() <= 7) {
				textForOption1.setEnabled(true);
				textForOption2.setEnabled(true);
				textForOption1.setText("");
				textForOption2.setText("");
				textForOption1.setHint("Enter New.....");
				textForOption2.setHint("Enter New.....");
				textForCount.setText(countAdap.get(position) + ".");
				chkBox.setVisibility(View.VISIBLE);
				chkBox2.setVisibility(View.VISIBLE);
				chk.setVisibility(View.GONE);
				chk2.setVisibility(View.GONE);

				chkBox.setOnClickListener(new OnClickListener() {

					Boolean status = false;

					@Override
					public void onClick(View v) {

						option_one = textForOption1.getText().toString();
						option_two = textForOption2.getText().toString();
						answer = textForOption1.getText().toString();
						if (option_one.equals(null) || option_one.equals("")
								|| option_two.equals("")
								|| option_two.equals(null)) {
							Toast.makeText(getApplicationContext(),
									"Enter Both Options", Toast.LENGTH_LONG)
									.show();
						} else {

							if (status == false) {
								chk.setVisibility(View.VISIBLE);
								chk2.setVisibility(View.GONE);
								pdialog.show();
								RunRequestAdd();
								status = true;
							} else {
								chk.setVisibility(View.GONE);
								chk2.setVisibility(View.VISIBLE);
								status = false;
							}
						}
					}
				});

				chkBox2.setOnClickListener(new OnClickListener() {
					Boolean status = false;

					@Override
					public void onClick(View v) {
						option_one = textForOption1.getText().toString();
						option_two = textForOption2.getText().toString();
						answer = textForOption2.getText().toString();
						if (option_one.equals(null) || option_one.equals("")
								|| option_two.equals("")
								|| option_two.equals(null)) {
							Toast.makeText(getApplicationContext(),
									"Enter Both Options", Toast.LENGTH_LONG)
									.show();
						} else {
							if (status == false) {
								chk.setVisibility(View.GONE);
								chk2.setVisibility(View.VISIBLE);
								Log.d("", "bench mark" + benchMarkValue
										+ "size of list" + listForListId.size());
								pdialog.show();
								RunRequestAdd();
								status = true;
							} else {
								chk.setVisibility(View.VISIBLE);
								chk2.setVisibility(View.GONE);
								status = false;
							}
						}
					}
				});

			} else {

				if (listForListAnswer.get(position).equals(
						listForOptionOne.get(position))) {
					chk.setVisibility(View.VISIBLE);
					chk2.setVisibility(View.GONE);
				} else {
					chk.setVisibility(View.GONE);
					chk2.setVisibility(View.VISIBLE);
				}
				textForOption1.setEnabled(false);
				textForOption2.setEnabled(false);
				textForCount.setText(countAdap.get(position) + ".");
				textForOption1.setText(opt1Adap.get(position));
				textForOption2.setText(opt2Adap.get(position));
				chkBox.setVisibility(View.GONE);
				chkBox2.setVisibility(View.GONE);
			}
			//
			// textForOption1
			// .setOnFocusChangeListener(new OnFocusChangeListener() {
			//
			// @Override
			// public void onFocusChange(View v, boolean hasFocus) {
			// if (hasFocus) {
			// Toast.makeText(getApplicationContext(),
			// "focus", Toast.LENGTH_LONG).show();
			// LinearLayout sp = (LinearLayout) findViewById(R.id.skip_pass);
			// sp.setVisibility(View.GONE);
			// } else {
			// Toast.makeText(getApplicationContext(),
			// "focus else", Toast.LENGTH_LONG).show();
			// }
			//
			// }
			// });

			return vi;
		}
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.arrow:
			if (listForListId.size() < 6) {
				Intent back = new Intent(Quiz_manager.this, HomeScreen.class);
				startActivity(back);
				finish();
			} else {
				Intent back = new Intent(Quiz_manager.this, Setting.class);
				startActivity(back);
				finish();
			}

			break;

		case R.id.new_form:
			// status = true;
			initiatePopupWindow();
			break;

		case R.id.btnForcancel:
			pwindo.dismiss();
			break;
		case R.id.edit_btn:
			Intent edit_quiz = new Intent(getApplicationContext(),
					Edit_quiz_manager.class);
			// if (benchMarkValue.equals(null) || benchMarkValue.equals(" "))
			// ;
			// benchMarkValue = "4";
			edit_quiz.putExtra("keyForBenchValue", benchMarkValue);
			startActivity(edit_quiz);

			finish();
			break;

		case R.id.btnForWindowClose:
			pwindo2.dismiss();
			break;

		case R.id.benchmark:
			if (listForListId.size() < 6) {
				Toast.makeText(
						getApplicationContext(),
						"Please Add valid Number of questions to set Benchmark",
						5000).show();
			} else {
				initiatePopupWindow2();
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void response(String response, int pageid) {
		System.out.println("respnse hereee= " + response);

		if (pageid == 2) {
			if (pdialog.isShowing()) {
				pdialog.dismiss();
			}
			try {
				JSONObject jsonResponse = new JSONObject(response);

				JSONArray jsonMainNode = jsonResponse.optJSONArray("list");
				for (int i = 0; i < jsonMainNode.length(); i++) {
					JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
					String catName = jsonChildNode.optString("cat_name");
					String catId = jsonChildNode.optString("id");

					listForListCategory.add(catName);
					listForCategoryId.add(catId);
					// listForCategoryIdAdd.add("");
					Log.e("category nam----------->>", "" + listForListCategory);
				}

				listForCategoryId.add("");
				// to display all category check box
				// uncomment these three line of code

				int size_of_list = listForListCategory.size();
				listForListCategory.add(size_of_list++, "All");
				Log.e("category name----------->>", "" + listForListCategory);

			} catch (JSONException e) {
				if (pdialog.isShowing()) {
					pdialog.dismiss();

				}
				e.printStackTrace();
			}
			// if (status == true)
			// initiatePopupWindow();
		}
		if (pageid == 4) {
			if (pdialog.isShowing()) {
				pdialog.dismiss();
			}
			try {
				listForListAnswer.clear();
				listForOptionOne.clear();
				listForOptionTwo.clear();
				listForListId.clear();
				JSONObject jsonResponse = new JSONObject(response);
				int count = 1;
				JSONArray jsonMainNode = jsonResponse.optJSONArray("list");
				for (int i = 0; i < jsonMainNode.length(); i++) {
					JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
					option_one_for_list = jsonChildNode.optString("option1");
					option_two_for_list = jsonChildNode.optString("option2");
					answer_for_list = jsonChildNode.optString("answer");
					id_for_list = jsonChildNode.optString("id");
					benchMarkValue = jsonChildNode.optInt("benchmark_value");
					listForListAnswer.add(answer_for_list);
					listForOptionOne.add(option_one_for_list);
					listForOptionTwo.add(option_two_for_list);
					listForListId.add(id_for_list);
					listForListSno.add(count);

					count++;
				}
				if (listForListId.size() <= 7) {
					listForOptionOne.add(" ");
					listForOptionTwo.add(" ");
					listForListAnswer.add("");
					listForListId.add("");
					listForListSno.add(count);
				}
				bnchmrk = listForListId.size();

				if (benchMarkValue < 3 && bnchmrk - 1 < 5) {
					System.out.println("first condition" + ", value: "
							+ benchMarkValue + ", mark: " + bnchmrk);
					txtForBenchMark.setText("0/" + (bnchmrk - 1));
				} else if (benchMarkValue < 3 && bnchmrk - 1 <= 6) {
					System.out.println("second condition");
					txtForBenchMark.setText("3/" + (bnchmrk - 1));
					bnchValue = 3;
					bnchmrk = bnchmrk - 1;
					setCheckBenchMark();
				} else if (benchMarkValue <= 3 && bnchmrk - 1 == 7) {
					System.out.println("third condition");
					txtForBenchMark.setText("4/" + (bnchmrk - 1));
					bnchValue = 4;
					bnchmrk = bnchmrk - 1;
					setCheckBenchMark();
				} else {
					System.out.println("forth condition");
					txtForBenchMark.setText("" + benchMarkValue + "/"
							+ (bnchmrk - 1));
				}
				adapter();

			} catch (JSONException e) {
				if (pdialog.isShowing()) {
					pdialog.dismiss();
				}
				e.printStackTrace();
			}
			// if (status == true)
			// initiatePopupWindow();
		}

	}

	@Override
	public void error(String error) {
		// TODO Auto-generated method stub

	}

	private PopupWindow pwindo, pwindo2;

	private void initiatePopupWindow2() {
		try {
			// We need to get the instance of the LayoutInflater
			LayoutInflater inflater = (LayoutInflater) Quiz_manager.this
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View layout = inflater.inflate(R.layout.bench_mark_popup,
					(ViewGroup) findViewById(R.id.popup_element));
			// layout.getBackground().setAlpha(210);

			RelativeLayout black = (RelativeLayout) layout
					.findViewById(R.id.black);
			black.getBackground().setAlpha(210);
			DisplayMetrics metrics = this.getResources().getDisplayMetrics();
			int width = metrics.widthPixels;
			int height = metrics.heightPixels;
			pwindo2 = new PopupWindow(layout, width, height, true);
			pwindo2.showAtLocation(layout, Gravity.CENTER, 0, 0);
			// final HttpResponseListener listner = null;
			btnForCloseWindow = (Button) layout
					.findViewById(R.id.btnForWindowClose);
			btnForCloseWindow.setOnClickListener(this);

			final EditText edtForBnchMark = (EditText) layout
					.findViewById(R.id.edtForBnchMark);

			// edtForBnchMark.setImeOptions(EditorInfo.IME_ACTION_DONE);

			Button btnForSetBnchmark = (Button) layout
					.findViewById(R.id.btnForSetBnchmark);
			btnForSetBnchmark.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						bnchValue = Integer.parseInt(edtForBnchMark.getText()
								.toString());

						if (bnchmrk < 7) {
							bnchmrk = bnchmrk;
						}
						if (bnchmrk >= 8) {
							bnchmrk = bnchmrk - 1;
						}
						if (bnchValue <= 3 || bnchValue > bnchmrk
								|| edtForBnchMark.getText() == null) {
							Toast.makeText(getApplicationContext(),
									"Enter Valid Input", Toast.LENGTH_LONG)
									.show();
						} else {
							// pref.setBenchMark(bnchValue);
							pdialog.show();
							Log.d("", "" + bnchValue + "............."
									+ bnchmrk);
							setBenchMark();
						}

					} catch (NumberFormatException e) {
						e.printStackTrace();
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initiatePopupWindow() {
		try {
			// We need to get the instance of the LayoutInflater
			LayoutInflater inflater = (LayoutInflater) Quiz_manager.this
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View layout = inflater.inflate(R.layout.popup_lauout,
					(ViewGroup) findViewById(R.id.popup_element));
			// layout.getBackground().setAlpha(210);
			RelativeLayout black = (RelativeLayout) layout
					.findViewById(R.id.black);
			black.getBackground().setAlpha(210);
			DisplayMetrics metrics = this.getResources().getDisplayMetrics();
			int width = metrics.widthPixels;
			int height = metrics.heightPixels;
			pwindo = new PopupWindow(layout, width, height - 120, true);
			pwindo.showAtLocation(layout, Gravity.TOP, 0, 0);
			// final HttpResponseListener listner = null;

			gridForChk = (GridView) layout.findViewById(R.id.grid_view_for_chk);
			// gridForChk.setAdapter(new gridAdapter(this));
			RunRequestNext();
			txtForOptionOne = (TextView) layout
					.findViewById(R.id.txtForOption1);
			txtForOptionTwo = (TextView) layout
					.findViewById(R.id.txtForOption2);
			txtForOptionOne.setText(option_one);
			txtForOptionTwo.setText(option_two);
			btnForNext = (Button) layout.findViewById(R.id.btnForNext);
			btnForNext.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					pdialog.show();
					RunRequestNext();
				}
			});
			Log.e("List Size", "" + listForListCategory.size());
			adapter = new gridAdapter(this, listForListCategory);
			gridForChk.setAdapter(adapter);

			popup_cancel = (Button) layout.findViewById(R.id.btnForcancel);
			popup_cancel.setOnClickListener(this);

			Button btnForAdd = (Button) layout.findViewById(R.id.btnForAdd);
			btnForAdd.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {

					if (listForListSno.size() >= 8) {
						Toast.makeText(getApplicationContext(),
								"Sorry, your question list is full",
								Toast.LENGTH_LONG).show();
					} else {
						pdialog.show();
						RunRequestAdd();
					}
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public class gridAdapter extends BaseAdapter {

		private Activity activity;
		// private ArrayList<String> data;
		private ArrayList<String> category_list;
		private LayoutInflater inflater = null;

		public gridAdapter(Activity a, ArrayList<String> category) {
			activity = a;
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			category_list = category;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listForListCategory.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View vi = convertView;
			if (convertView == null)
				vi = inflater.inflate(R.layout.grid_view_layout, null);

			final CheckBox chkForGrid = (CheckBox) vi.findViewById(R.id.chkFor);
			chkForGrid.setText(category_list.get(position));

			if (position == listForListCategory.size() - 1) {
				chkForGrid.setChecked(true);
				chkForGrid.setTextColor(green); // / also uncomment this code
												// for
				// the proper workin of all category of qoestion
				for (int i = 0; i < listForCategoryId.size(); i++) {
					listForCategoryIdAdd.add(listForCategoryId.get(i));
				}
				status = true;
				Log.e("in for loop", "" + listForCategoryIdAdd);
			}

			chkForGrid
					.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						@Override
						public void onCheckedChanged(CompoundButton buttonView,
								boolean isChecked) {

							if (position == listForListCategory.size() - 1) {

								if (listForCategoryIdAdd.size() != 0) {
									listForCategoryIdAdd.clear();
								}
								// chkAllChk();
								if (chkForGrid.isChecked()) {
									for (int i = 0; i < listForCategoryId
											.size(); i++) {
										listForCategoryIdAdd
												.add(listForCategoryId.get(i));
									}
									status = true;
								} else {
									// chkForGrid.setChecked(false);

									for (int a = 0; a < listForCategoryId
											.size(); a++) {
										listForCategoryIdAdd
												.remove(listForCategoryId
														.get(a));
									}
									status = false;
								}
								Log.e(" if condition", ""
										+ listForCategoryIdAdd + "   status:- "
										+ status);
							} else {
								if (chkForGrid.isChecked()) {
									unclearAllcheck();
									if (status == true) {
										listForCategoryIdAdd.clear();
										status = false;
									}
									chkForGrid.setTextColor(green);
									listForCategoryIdAdd.add(listForCategoryId
											.get(position));

								} else {
									chkForGrid.setTextColor(Color.GRAY);
									listForCategoryIdAdd
											.remove(listForCategoryId
													.get(position));
								}
							}
						}
					});
			return vi;
		}

		private void unclearAllcheck() {
			for (int i = 0; i < getCount(); i++) {
				RelativeLayout itemLayout = (RelativeLayout) gridForChk
						.getChildAt(i);
				CheckBox checkbox = (CheckBox) itemLayout
						.findViewById(R.id.chkFor);
				if (i == getCount() - 1) {
					checkbox.setChecked(false);
				}
			}

		}

		public void chkAllChk() {
			for (int i = 0; i < getCount(); i++) {
				RelativeLayout itemLayout = (RelativeLayout) gridForChk
						.getChildAt(i);
				CheckBox checkbox = (CheckBox) itemLayout
						.findViewById(R.id.chkFor);

				if (i != getCount() - 1)
					checkbox.setChecked(true);
				// checkbox.setClickable(false);

			}
		}

	}

	public void RunRequestNext() {
		Log.e(" next url:-  ", "" + adminQuestionURL + listForCategoryIdAdd);

		String url = adminQuestionURL + listForCategoryIdAdd;
		url = url.replace(" ", "%20");

		Request r = (Request) new Request(url) {
			protected void onSuccess(Transport transport) {

				try {
					JSONObject response = transport.getResponseJson();
					if (response == null) {
						if (pdialog.isShowing()) {
							pdialog.dismiss();

						}
						Toast.makeText(getApplicationContext(), "Server Error",
								5000).show();
					} else {
						if (pdialog.isShowing()) {
							pdialog.dismiss();
						}
						JSONArray jsonMainNode = response.optJSONArray("list");
						for (int i = 0; i < jsonMainNode.length(); i++) {
							JSONObject jsonChildNode = jsonMainNode
									.getJSONObject(i);
							option_one = jsonChildNode.optString("option1");
							option_two = jsonChildNode.optString("option2");
							answer = jsonChildNode.optString("answer");

							// txtForOptionOne.setText(option_one);
							// txtForOptionTwo.setText(option_two);

							Log.e("option1", "" + option_one);
							Log.e("option2", "" + option_two);
							Log.e("answer", "" + answer);
						}
						txtForOptionOne.setText(option_one);
						txtForOptionTwo.setText(option_two);
					}
				} catch (JSONException e) {

					if (pdialog.isShowing()) {
						pdialog.dismiss();

					}
					e.printStackTrace();
				}
			}

			// Optional callback override.
			@Override
			protected void onError(IOException ex) {
				Toast.makeText(getApplicationContext(), "Server Error: " + ex,
						5000).show();
			}

			// Optional callback override.
			@Override
			protected void onFailure(Transport transport) {
				Toast.makeText(getApplicationContext(),
						"Server Error: " + transport, 5000).show();

			}
		}.execute("GET");
		r.accept(Request.CTYPE_JSON);
		r.setContentType(Request.CTYPE_JSON);
	}

	public void RunRequestAdd() {

		String url = add_question_answerURL + userID + "&option1=" + option_one
				+ "&option2=" + option_two + "&answer=" + answer;
		url = url.replace(" ", "%20");
		System.out.println("URL ----------- = " + url);

		Request add = (Request) new Request(url) {
			@Override
			protected void onSuccess(Transport transport) {
				if (pdialog.isShowing()) {
					pdialog.dismiss();
				}
				try {
					JSONObject response = transport.getResponseJson();
					JSONObject response2 = response.getJSONObject("Response");
					System.out.println("response = " + response2);
					String success = response2.optString("resultText");
					Log.e("responce string", "" + success);
					if (success.equals("Success")) {
						Toast.makeText(getApplicationContext(),
								"Save successfuly", 5000).show();
						// new HttpGetRespone(userQuestionListURL + userID,
						// Quiz_manager.this, 4).execute();
						refreshActivity();
					}

				} catch (Exception e) {
					if (pdialog.isShowing()) {
						pdialog.dismiss();
					}
					e.printStackTrace();
				}
			}
		}.execute("GET");
		add.accept(Request.CTYPE_JSON);
		add.setContentType(Request.CTYPE_JSON);
	}

	public void setBenchMark() {
		String url = "http://peapodapp.com/webservices/androidapi.php?type=setbenchmark&userid="
				+ userID
				+ "&benchmark_value="
				+ bnchValue
				+ "&benchmark_total=" + bnchmrk;
		url = url.replace(" ", "%20");
		System.out.println("URL ----------- = " + url);
		Request add = (Request) new Request(url) {
			@Override
			protected void onSuccess(Transport transport) {
				if (pdialog.isShowing()) {
					pdialog.dismiss();
				}
				try {
					JSONObject response = transport.getResponseJson();
					JSONObject response2 = response.getJSONObject("Response");
					JSONObject r3 = response2.getJSONObject("returnCode");
					System.out.println("response = " + response2);
					String success = r3.optString("resultText");
					Log.e("responce string", "" + success
							+ "............................" + success);
					if (success.equals("Success")) {
						Toast.makeText(getApplicationContext(),
								"Save successfuly", 5000).show();
						refreshActivity();
					}
				} catch (Exception e) {
					if (pdialog.isShowing()) {
						pdialog.dismiss();
					}
					e.printStackTrace();
				}
			}
		}.execute("GET");
		add.accept(Request.CTYPE_JSON);
		add.setContentType(Request.CTYPE_JSON);
	}

	public void setCheckBenchMark() {

		String url = "http://peapodapp.com/webservices/androidapi.php?type=setbenchmark&userid="
				+ userID
				+ "&benchmark_value="
				+ bnchValue
				+ "&benchmark_total=" + bnchmrk;
		url = url.replace(" ", "%20");
		System.out.println("URL ----------- = " + url);
		Request add = (Request) new Request(url) {
			@Override
			protected void onSuccess(Transport transport) {
				if (pdialog.isShowing()) {
					pdialog.dismiss();
				}
				try {
					JSONObject response = transport.getResponseJson();
					JSONObject response2 = response.getJSONObject("Response");
					JSONObject r3 = response2.getJSONObject("returnCode");
					System.out.println("response = " + response2);
					String success = r3.optString("resultText");
					Log.e("responce string", "" + success
							+ "............................" + success);
					if (success.equals("Success")) {

					}
				} catch (Exception e) {
					if (pdialog.isShowing()) {
						pdialog.dismiss();
					}
					e.printStackTrace();
				}
			}
		}.execute("GET");
		add.accept(Request.CTYPE_JSON);
		add.setContentType(Request.CTYPE_JSON);

	}

	public void adapter() {
		ListAdapterNew adap = new ListAdapterNew(this, listForListId,
				listForOptionOne, listForOptionTwo, listForListAnswer,
				listForListSno);
		listViewForQuiz.setAdapter(adap);
	}

	public void refreshActivity() {
		Intent refresh = new Intent(Quiz_manager.this, Quiz_manager.class);
		startActivity(refresh);
		finish();
	}

	@Override
	public void onBackPressed() {

	}
}
