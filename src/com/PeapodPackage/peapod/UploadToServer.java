package com.PeapodPackage.peapod;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import se.tonyivanov.android.ajax.Request;
import se.tonyivanov.android.ajax.Transport;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class UploadToServer {

	TextView messageText;
	Button uploadButton;
	int serverResponseCode = 0;
	ProgressDialog dialog = null;
	String fileNameForUpload;
	Context context = null;

	String filePath = "";

	/********** File Path *************/
	String uploadFilePath = "";

	public UploadToServer(Context context) {

		this.context = context;

	}

	public int uploadFile(String sourceFileUri) {

		// dialog = ProgressDialog.show(context, "", "Uploading file...", true);
		Log.d("sourceFileUrisourceFileUri", sourceFileUri);

		String fileName = sourceFileUri;

		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;

		File sourceFile = new File(sourceFileUri);

		String name = sourceFile.getName();
		fileNameForUpload = name;
		Log.e("file name in upload method ==================", ""
				+ fileNameForUpload);

		if (!sourceFile.isFile()) {
			if (dialog.isShowing())
				dialog.dismiss();

			Log.e("uploadFile", "Source File not exist :" + sourceFileUri);

			Toast.makeText(context, "Source File not exist.", Toast.LENGTH_LONG)
					.show();

			return 0;

		} else {
			try {

				// open a URL connection to the Servlet
				FileInputStream fileInputStream = new FileInputStream(
						sourceFile);
				URL url = new URL(
						"http://peapodapp.com/webservices/androidapi.php?type=uploadimage");

				// Open a HTTP connection to the URL
				conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true); // Allow Inputs
				conn.setDoOutput(true); // Allow Outputs
				conn.setUseCaches(false); // Don't use a Cached Copy
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Connection", "Keep-Alive");
				conn.setRequestProperty("ENCTYPE", "multipart/form-data");
				conn.setRequestProperty("Content-Type",
						"multipart/form-data;boundary=" + boundary);
				conn.setRequestProperty("uploaded_file", fileName);

				dos = new DataOutputStream(conn.getOutputStream());

				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
						+ fileName + "\"" + lineEnd);

				dos.writeBytes(lineEnd);

				// create a buffer of maximum size
				bytesAvailable = fileInputStream.available();

				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];

				// read file and write it into form...
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				while (bytesRead > 0) {

					dos.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				}

				// send multipart form data necesssary after file data...
				dos.writeBytes(lineEnd);
				dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

				// Responses from the server (code and message)
				serverResponseCode = conn.getResponseCode();
				String serverResponseMessage = conn.getResponseMessage();

				InputStreamReader isr = new InputStreamReader(
						conn.getInputStream());
				BufferedReader br = new BufferedReader(isr);
				StringBuilder sb = new StringBuilder();
				String line = null;
				while ((line = br.readLine()) != null) {
					System.out.println(line);
					sb.append(line);
				}

				isr.close();
				br.close();

				JSONObject json = new JSONObject(sb.toString());
				filePath = json.optString("filepath");

				Log.i("uploadFile", "HTTP Response is : "
						+ serverResponseMessage + ": " + serverResponseCode);

				if (serverResponseCode == 200) {
					// Toast.makeText(context, "File Upload Completed.",
					// Toast.LENGTH_LONG).show();
					Log.e("message", "image upload complete");
				}

				// close the streams //
				fileInputStream.close();
				dos.flush();
				dos.close();

			} catch (MalformedURLException ex) {

				// dialog.dismiss();
				ex.printStackTrace();

				Toast.makeText(context, "MalformedURLException",
						Toast.LENGTH_SHORT).show();

				Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
			} catch (Exception e) {

				// dialog.dismiss();
				e.printStackTrace();
				// Toast.makeText(context, "Got Exception : see logcat ",
				// Toast.LENGTH_SHORT).show();

				Log.e("Upload file to server Exception",
						"Exception : " + e.getMessage(), e);
			}
			// dialog.dismiss();
			return serverResponseCode;

		} // End else block
	}

	public String getFilePath() {
		return filePath;
	}

	public String getImageName() {
		return fileNameForUpload;
	}
}