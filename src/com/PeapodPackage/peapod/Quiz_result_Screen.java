package com.PeapodPackage.peapod;

import java.io.IOException;

import org.json.JSONObject;

import com.Peapod.Connections.StorePref;
import com.Peapod.Connections.TransparentProgressDialog;

import se.tonyivanov.android.ajax.Request;
import se.tonyivanov.android.ajax.Transport;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Quiz_result_Screen extends Activity implements OnClickListener {

	int getScore, getSize;
	RelativeLayout forShowThumb, showThumbDown;
	LinearLayout keepSearching;
	TextView txtForShowScore, txtForShowNname, txtForShowScoreFail;
	TransparentProgressDialog pdialog;
	StorePref pref;
	String friendID;
	String userID;
	String benchValuel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.quiz_result_screen);
		pdialog = new TransparentProgressDialog(this, R.drawable.loader);

		forShowThumb = (RelativeLayout) findViewById(R.id.in_center);
		showThumbDown = (RelativeLayout) findViewById(R.id.in_centerNew);
		txtForShowScore = (TextView) findViewById(R.id.txtForShowScore);
		txtForShowScoreFail = (TextView) findViewById(R.id.txtForShowScoreFail);
		txtForShowNname = (TextView) findViewById(R.id.txt);
		keepSearching = (LinearLayout) findViewById(R.id.skip_pass);
		keepSearching.setOnClickListener(this);

		pref = StorePref.getInstance(getApplicationContext());
		userID = pref.getUserid();

		getScore = getIntent().getIntExtra("keyForScore", 0);
		getSize = getIntent().getIntExtra("keyForSize", 0);
		friendID = getIntent().getStringExtra("keyForFriendId");
		benchValuel = getIntent().getStringExtra("keyForbenchValue");
		Log.e("bench", "bench: " + benchValuel);

		txtForShowScore.setText("" + getScore + "/" + getSize);
		txtForShowScoreFail.setText("" + getScore + "/" + getSize);

		if (getScore < Integer.parseInt(benchValuel)) {
			forShowThumb.setVisibility(View.GONE);
			showThumbDown.setVisibility(View.VISIBLE);
			deleteQuizQuestionFail();
		} else {
			forShowThumb.setVisibility(View.VISIBLE);
			showThumbDown.setVisibility(View.GONE);
			sendRequest();
		}

	}

	@Override
	public void onBackPressed() {

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.skip_pass:
			Intent home = new Intent(this, HomeScreen.class);
			startActivity(home);
			finish();
			break;

		default:
			break;
		}
	}

	public void sendRequest() {

		String url = "http://peapodapp.com/webservices/androidapi.php?type=sendfreindrequest&userid="
				+ userID
				+ "&freindid="
				+ friendID
				+ "&correct_answer="
				+ getScore + "&total_ques=" + getSize;
		url = url.replace(" ", "%20");
		System.out.println("URL ----------- = " + url);
		Request add = (Request) new Request(url) {
			@Override
			protected void onSuccess(Transport transport) {
				if (pdialog.isShowing()) {
					pdialog.dismiss();
				}
				try {
					JSONObject response = transport.getResponseJson();
					JSONObject response2 = response.getJSONObject("Response");

					System.out.println("response = " + response2);
					String success = response2.optString("resultText");
					Log.e("responce string", "" + success
							+ "............................" + success);
					if (success.equals("Success")) {
						Toast.makeText(getApplicationContext(),
								"Request Sent successfuly", 5000).show();
						// refreshActivity();
					}
				} catch (Exception e) {
					if (pdialog.isShowing()) {
						pdialog.dismiss();
					}
					e.printStackTrace();
				}
			}
		}.execute("GET");
		add.accept(Request.CTYPE_JSON);
		add.setContentType(Request.CTYPE_JSON);
	}

	public void deleteQuizQuestionFail() {

		String url = "http://peapodapp.com/webservices/androidapi.php?type=delete_quizquestion_answer_onfail&userid="
				+ userID + "&freindid=" + friendID;
		Log.e("url here:- ", "" + url);

		Request detete_questios = (Request) new Request(url) {

			@Override
			protected void onSuccess(Transport transport) {

				try {
					JSONObject response = transport.getResponseJson();
					JSONObject result = response.optJSONObject("Response");
					String success = result.getString("result");
					Log.e("success", "" + success);
					if (success.equals("0")) {

					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}

			@Override
			protected void onFailure(Transport transport) {
				// TODO Auto-generated method stub
				Log.e("on faliur", "on failur:- " + transport);
			}

			@Override
			protected void onError(IOException ex) {
				// TODO Auto-generated method stub
				ex.printStackTrace();
			}
		}.execute("GET");
	}

}
