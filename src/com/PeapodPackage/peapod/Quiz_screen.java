package com.PeapodPackage.peapod;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.Peapod.Connections.ConnectionStatus;
import com.Peapod.Connections.HttpGetRespone;
import com.Peapod.Connections.HttpResponseListener;
import com.Peapod.Connections.StorePref;
import com.Peapod.Connections.TransparentProgressDialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Quiz_screen extends Activity implements HttpResponseListener,
		OnClickListener {

	RelativeLayout center;
	ConnectionStatus statusis;
	AlertDialog alertDialog;
	boolean isConnected, check = false;
	StorePref pref;
	String benchValue;
	TransparentProgressDialog pdialog;
	TextView option_one, option_two, txtForQuizResult;
	ImageView imgForEndQuiz;
	String getUserIDforQuestion, checkAnswer, userID, questionID, status;
	int green;
	ArrayList<String> listForOption1 = new ArrayList<String>();
	ArrayList<String> listForOption2 = new ArrayList<String>();
	ArrayList<String> listForAnswers = new ArrayList<String>();
	ArrayList<String> listForUserID = new ArrayList<String>();
	ArrayList<String> listForQuestionID = new ArrayList<String>();
	int position = 0, count = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.quiz_screen);

		center = (RelativeLayout) findViewById(R.id.center_layout);
		center.getBackground().setAlpha(100);
		green = getApplicationContext().getResources().getColor(R.color.green);

		pref = StorePref.getInstance(getApplicationContext());
		userID = pref.getUserid();

		pdialog = new TransparentProgressDialog(this, R.drawable.loader);
		imgForEndQuiz = (ImageView) findViewById(R.id.pass);
		imgForEndQuiz.setOnClickListener(this);
		option_one = (TextView) findViewById(R.id.option_one);
		option_two = (TextView) findViewById(R.id.option_two);
		option_one.setOnClickListener(this);
		option_two.setOnClickListener(this);
		txtForQuizResult = (TextView) findViewById(R.id.txtForQuizResultShow);
		txtForQuizResult.setTextColor(green);
		getUserIDforQuestion = getIntent().getStringExtra("keyForUserID");

		statusis = new ConnectionStatus(this);
		pdialog = new TransparentProgressDialog(this, R.drawable.loader);
		pdialog.show();
		isConnected = statusis.isConnected();
		if (isConnected) {
			try {
				new HttpGetRespone(
						"http://peapodapp.com/webservices/androidapi.php?type=displayuserquestions&userid="
								+ getUserIDforQuestion, this, 1).execute();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {

			if (pdialog.isShowing())
				pdialog.dismiss();
			alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle("Warning");
			alertDialog.setMessage("No Internet Connection Available");
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			alertDialog.show();
		}

	}

	@Override
	public void response(String response, int page_id) {
		if (page_id == 1) {
			if (pdialog.isShowing())
				pdialog.dismiss();
			try {
				JSONObject obj = new JSONObject(response);
				JSONArray objArray = obj.optJSONArray("list");
				Log.e("size of array", "" + objArray.length());
				for (int i = 0; i < objArray.length(); i++) {
					JSONObject getJson = objArray.getJSONObject(i);

					String opt1 = getJson.optString("option1");
					String opt2 = getJson.optString("option2");
					String ans = getJson.optString("answer");
					String userID = getJson.optString("userid");
					String Q_id = getJson.optString("id");
					benchValue = getJson.optString("benchmark_value");

					listForAnswers.add(ans);
					listForOption1.add(opt1);
					listForOption2.add(opt2);
					listForUserID.add(userID);
					listForQuestionID.add(Q_id);
					Log.e(",.,.,.,.,.,.,", "" + opt1 + "......" + opt2
							+ ",,,,,,,,,," + ans + "............" + userID);
					Log.e("list================--",
							""
									+ listForAnswers
									+ "====================="
									+ listForAnswers.size()
									+ "--------------------------q id----------------------"
									+ listForQuestionID);

				}
				option_one.setText(listForOption1.get(position));
				option_two.setText(listForOption2.get(position));
				txtForQuizResult.setText("0/" + listForAnswers.size());

			} catch (Exception e) {
				e.printStackTrace();
				if (pdialog.isShowing())
					pdialog.dismiss();
			}

		}

		if (page_id == 2) {
			if (pdialog.isShowing()) {
				pdialog.dismiss();
			}
			try {
				JSONObject jsonResponse = new JSONObject(response);
				JSONObject jsonResponse2 = jsonResponse
						.getJSONObject("Response");
				String status = jsonResponse2.getString("result");
				if (status.equals("0")) {
					if (position == listForAnswers.size()) {
						Intent result = new Intent(this,
								Quiz_result_Screen.class);
						result.putExtra("keyForScore", count);
						result.putExtra("keyForSize", listForAnswers.size());
						result.putExtra("keyForFriendId", getUserIDforQuestion);
						result.putExtra("keyForbenchValue", benchValue);
						startActivity(result);
						finish();
					}

				}
			} catch (Exception e) {
				e.printStackTrace();
				if (pdialog.isShowing()) {
					pdialog.dismiss();
				}
			}
		}
	}

	@Override
	public void error(String error) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.option_one:

			checkAnswer = option_one.getText().toString();
			if (checkAnswer.equals(listForAnswers.get(position))) {
				questionID = listForQuestionID.get(position);
				position++;
				count++;
				status = "correct";
				submitResult();
				if (position < listForAnswers.size()) {
					option_one.setText(listForOption1.get(position));
					option_two.setText(listForOption2.get(position));

				}
				txtForQuizResult.setText("" + count + "/"
						+ listForAnswers.size());

			} else {
				questionID = listForQuestionID.get(position);
				position++;
				status = "wrong";
				submitResult();
				if (position < listForAnswers.size()) {
					option_one.setText(listForOption1.get(position));
					option_two.setText(listForOption2.get(position));

				}

				txtForQuizResult.setText("" + count + "/"
						+ listForAnswers.size());

			}

			// if (position == listForAnswers.size()) {
			// Intent result = new Intent(this, Quiz_result_Screen.class);
			// result.putExtra("keyForScore", count);
			// result.putExtra("keyForSize", listForAnswers.size());
			// startActivity(result);
			// finish();
			// }

			break;

		case R.id.option_two:

			checkAnswer = option_two.getText().toString();
			if (checkAnswer.equals(listForAnswers.get(position))) {
				questionID = listForQuestionID.get(position);
				position++;
				count++;
				status = "correct";
				submitResult();
				if (position < listForAnswers.size()) {
					option_one.setText(listForOption1.get(position));
					option_two.setText(listForOption2.get(position));

				}
				txtForQuizResult.setText("" + count + "/"
						+ listForAnswers.size());

			} else {
				questionID = listForQuestionID.get(position);
				position++;
				status = "wrong";
				submitResult();
				if (position < listForAnswers.size()) {
					option_one.setText(listForOption1.get(position));
					option_two.setText(listForOption2.get(position));

				}
				txtForQuizResult.setText("" + count + "/"
						+ listForAnswers.size());
			}
			break;

		case R.id.pass:
			Intent home = new Intent(this, HomeScreen.class);
			startActivity(home);
			finish();
			break;

		default:
			break;
		}

	}

	public void submitResult() {
		pdialog.show();
		Log.e("url=============================================================",
				"http://www.peapodapp.com/webservices/androidapi.php?type=quizquestion_answer&userid="
						+ userID
						+ "&freindid="
						+ getUserIDforQuestion
						+ "&questionid="
						+ questionID
						+ "&answer_status="
						+ status);
		new HttpGetRespone(
				"http://www.peapodapp.com/webservices/androidapi.php?type=quizquestion_answer&userid="
						+ userID
						+ "&freindid="
						+ getUserIDforQuestion
						+ "&questionid="
						+ questionID
						+ "&answer_status="
						+ status, this, 2).execute();
	}

	@Override
	public void onBackPressed() {

	}
}
