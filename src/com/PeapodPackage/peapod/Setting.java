package com.PeapodPackage.peapod;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Setting extends Activity implements OnClickListener {

	private TextView app_setting, quiz_manager, visibility, txtForShare,
			txtForMyProfile, contact;
	private Button backBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting);
		app_setting = (TextView) findViewById(R.id.AppSetting);
		app_setting.setOnClickListener(this);
		backBtn = (Button) findViewById(R.id.back_btn);
		backBtn.setOnClickListener(this);
		quiz_manager = (TextView) findViewById(R.id.QuizManager);
		quiz_manager.setOnClickListener(this);
		visibility = (TextView) findViewById(R.id.visibility);
		visibility.setOnClickListener(this);
		txtForShare = (TextView) findViewById(R.id.shared);
		txtForShare.setOnClickListener(this);
		txtForMyProfile = (TextView) findViewById(R.id.myProfile);
		txtForMyProfile.setOnClickListener(this);
		contact = (TextView) findViewById(R.id.contact);
		contact.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.AppSetting:
			Intent app_setting_page = new Intent(Setting.this,
					App_setting.class);
			startActivity(app_setting_page);
			break;

		case R.id.back_btn:
			Intent home = new Intent(Setting.this, HomeScreen.class);
			startActivity(home);
			finish();
			break;

		case R.id.QuizManager:
			Intent quiz_manager_page = new Intent(Setting.this,
					Quiz_manager.class);
			startActivity(quiz_manager_page);
			finish();
			break;

		case R.id.visibility:
			Intent visibility_page = new Intent(Setting.this,
					Visibility_Setting.class);
			startActivity(visibility_page);
			finish();
			break;

		case R.id.myProfile:
			Intent profile_page = new Intent(Setting.this, MyProfile.class);
			startActivity(profile_page);
			break;

		case R.id.contact:
			Intent i = new Intent(Setting.this, contactpeapod.class);
			startActivity(i);
			break;
		case R.id.shared:
			Intent intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.putExtra(Intent.EXTRA_EMAIL,
					new String[] { "write email address" });
			intent.putExtra(Intent.EXTRA_SUBJECT, "Download This App!");
			intent.putExtra(Intent.EXTRA_TEXT, "link of app ");
			try {
				startActivity(Intent.createChooser(intent, "Send App..."));
			} catch (android.content.ActivityNotFoundException ex) {
				Toast.makeText(getApplicationContext(),
						"There are no email clients installed.",
						Toast.LENGTH_SHORT).show();
			}
			break;

		default:
			break;
		}

	}

	@Override
	public void onBackPressed() {

	}

}
