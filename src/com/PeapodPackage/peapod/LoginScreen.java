package com.PeapodPackage.peapod;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import se.tonyivanov.android.ajax.Request;
import se.tonyivanov.android.ajax.Transport;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.Toast;

import com.Peapod.Connections.ConnectionStatus;
import com.Peapod.Connections.GPSTracker;
import com.Peapod.Connections.HttpGetRespone;
import com.Peapod.Connections.HttpResponseListener;
import com.Peapod.Connections.ImageLoader;
import com.Peapod.Connections.JsonParser;
import com.Peapod.Connections.StorePref;
import com.Peapod.Connections.TransparentProgressDialog;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;

public class LoginScreen extends Activity implements OnClickListener,
		HttpResponseListener {

	private ImageView fb_login, dot1, dot2, dot3, dot4;
	String success, get_uid, imageNameString, imageURL;
	String register_url = "http://peapodapp.com/webservices/androidapi.php?type=sociallogin&email=";
	String access_token, facebook_image_url;
	String status, imageURLString;
	ConnectionStatus statusis;
	AlertDialog alertDialog;
	boolean isConnected;
	long expires;
	Bitmap bitmap;

	int imageArraySize;
	GPSTracker tracker;
	double latitude, longitude;

	TransparentProgressDialog pdialog;
	StorePref store_pref;
	// Your Facebook APP ID
	// private static String APP_ID = "1525869704319713";
	private static String APP_ID = "755137707918865";

	// private static String APP_ID = "1419084038348868";

	// private String registrationURL =
	// "http://peapodapp.com/webservices/androidapi.php?type=sociallogin&email=";
	// Instance of Facebook Class
	private Facebook facebook = new Facebook(APP_ID);
	private AsyncFacebookRunner mAsyncRunner;
	// private SharedPreferences fb_pref;
	private int serverResponseCode = 0;
	public ImageLoader imageLoader;
	private ViewPager pager;
	DetailOnPageChangeListener listener;
	String[] imageUrls = {
			"http://citemenu.com/Loyalty/logoimg/loyality_card_barcode_img_new.png",
			"http://citemenu.com/Loyalty/logoimg/loyality_card_barcode_img_new.png" };

	public Integer[] images_array = { R.drawable.main_image,
			R.drawable.main_image, R.drawable.main_image, R.drawable.main_image };

	private ArrayList<String> listToGetImagesURL = new ArrayList<String>();
	private ArrayList<String> listToGetAlbumId = new ArrayList<String>();
	private ArrayList<String> listForImageUrl = new ArrayList<String>();

	String name;
	// getting email of the user
	String email_get;
	String first_name;
	String last_name;
	String link;
	String gender;
	String id;
	String bday;
	String atheletes;
	String teams;
	String bio_data;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_screen);

		fb_login = (ImageView) findViewById(R.id.facebook_login);
		fb_login.setOnClickListener(this);

		tracker = new GPSTracker(this);

		store_pref = StorePref.getInstance(this);
		pdialog = new TransparentProgressDialog(this, R.drawable.loader);
		mAsyncRunner = new AsyncFacebookRunner(facebook);

		imageLoader = new ImageLoader(getApplicationContext());

		dot1 = (ImageView) findViewById(R.id.dot1);
		dot2 = (ImageView) findViewById(R.id.dot2);
		dot3 = (ImageView) findViewById(R.id.dot3);
		dot4 = (ImageView) findViewById(R.id.dot4);
		dot1.setImageResource(R.drawable.dot_fill);

		// get images From API
		statusis = new ConnectionStatus(this);
		pdialog = new TransparentProgressDialog(this, R.drawable.loader);
		pdialog.show();
		isConnected = statusis.isConnected();
		if (isConnected) {
			try {
				new HttpGetRespone(
						"http://peapodapp.com/webservices/androidapi.php?type=mainimages",
						this, 1).execute();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {

			if (pdialog.isShowing())
				pdialog.dismiss();
			alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle("StashApp");
			alertDialog.setMessage("No Internet Connection Available");
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			alertDialog.show();
		}

		pager = (ViewPager) findViewById(R.id.pager);

		listener = new DetailOnPageChangeListener();
		pager.setOnPageChangeListener(listener);

		printKeyHash(LoginScreen.this);

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		isConnected = statusis.isConnected();
		if (isConnected) {

			final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

			if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						LoginScreen.this);
				builder.setCancelable(false).setPositiveButton("ok",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								startActivity(new Intent(
										android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
							}
						});

				AlertDialog diag = builder.create();
				diag.setTitle("GPS?");
				diag.setMessage("Turn on your GPS");
				diag.show();
			} else {
				if (tracker.canGetLocation()) {
					latitude = tracker.getLatitude();
					longitude = tracker.getLongitude();

					Log.e("Latitude =================== ", "" + latitude
							+ "  longitude ========" + longitude);

					// new HttpGetRespone(Constants.HOST + Constants.MYSTASH
					// + "&userid=" + userid, this, 1).execute();
				} else {
					Toast.makeText(LoginScreen.this, "Not getting location",
							Toast.LENGTH_LONG).show();
				}
			}
		} else {
			if (pdialog.isShowing())
				pdialog.dismiss();
			alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle("Stash");
			alertDialog.setMessage("No Internet Connection Available");
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			alertDialog.show();
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.facebook_login:
			loginToFacebook();
			break;

		default:
			break;
		}
	}

	@SuppressWarnings("deprecation")
	public void loginToFacebook() {

		// fb_pref = getPreferences(MODE_PRIVATE);
		//
		// access_token = fb_pref.getString("access_token", null);
		// expires = fb_pref.getLong("access_expires", 0);
		//
		// if (access_token != null) {
		// facebook.setAccessToken(access_token);
		// // getInfromation();
		// Log.d("FB Sessions", "" + facebook.isSessionValid());
		// }
		// if (expires != 0) {
		// facebook.setAccessExpires(expires);
		// }
		//
		// if (!facebook.isSessionValid()) {
		// Intent Home_screen = new Intent(getApplicationContext(),
		// HomeScreen.class);
		// startActivity(Home_screen);
		//
		// } else {

		facebook.authorize(LoginScreen.this, new String[] { "email",
				"user_about_me", "publish_stream", "user_birthday",
				"user_photos", "user_interests" }, new DialogListener() {
			@Override
			public void onComplete(Bundle values) {
				// TODO Auto-generated method stub
				// SharedPreferences.Editor editor = fb_pref.edit();
				// editor.putString("access_token",
				// facebook.getAccessToken());
				// editor.putLong("access_expires",
				// facebook.getAccessExpires());
				// editor.commit();
				Log.e("Access Token:- ",
						"Access Token Here:- " + facebook.getAccessToken());
				try {
					getInfromation();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// getInfromation2();
				// testMethod();
				// getFullInformation();

			}

			@Override
			public void onFacebookError(FacebookError e) {
				// TODO Auto-generated method stub
				Toast.makeText(LoginScreen.this, "here -- " + e.toString(),
						Toast.LENGTH_SHORT).show();
				System.out.println("error is = " + e.toString());
			}

			@Override
			public void onError(DialogError e) {
				// TODO Auto-generated method stub
				Toast.makeText(LoginScreen.this, "on Error", Toast.LENGTH_SHORT)
						.show();
			}

			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
				Toast.makeText(LoginScreen.this, "Cancel", Toast.LENGTH_SHORT)
						.show();
			}

		});

	}

	@SuppressWarnings("deprecation")
	public void getInfromation() throws JSONException {
		// TODO Auto-generated method stub

		mAsyncRunner.request("me/albums", new RequestListener() {

			@Override
			public void onMalformedURLException(MalformedURLException e,
					Object state) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onIOException(IOException e, Object state) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onFileNotFoundException(FileNotFoundException e,
					Object state) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onFacebookError(FacebookError e, Object state) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onComplete(String response, Object state) {
				System.out.println("Albums Response Here:-" + response);

				String json = response;

				try {
					JSONObject profile = new JSONObject(json);
					JSONArray data = profile.getJSONArray("data");

					if (data.length() > 0) {
						for (int i = 0; i < data.length(); i++) {
							JSONObject getData = data.getJSONObject(i);
							String name = getData.optString("name");
							String album_id = getData.optString("id");
							listToGetAlbumId.add(album_id);
							System.out.println("listToGetAlbumId HERE:- "
									+ listToGetAlbumId);
							System.out.println("Album Id Here:- " + album_id);
							System.out.println("Album Name Here:- " + name);
						}
						getFacebookAlbumImages();
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		mAsyncRunner.request("me", new RequestListener() {
			String email = null;

			@Override
			public void onComplete(String response, Object state) {
				Log.d("Profile", response);

				String json = response;

				try {

					JSONObject profile = new JSONObject(json);
					// getting name of the user
					/*
					 * JSONArray arrayFriend = profile.getJSONArray("data");
					 * System.out.println("Profile======"+
					 * arrayFriend.toString());
					 */

					name = profile.optString("name");
					Log.e("facebook name= ", name);
					// getting email of the user
					email = profile.optString("email");
					first_name = profile.optString("first_name");
					last_name = profile.optString("last_name");
					link = profile.optString("link");
					gender = profile.optString("gender");
					id = profile.optString("id");
					bio_data = profile.optString("bio");
					bday = profile.optString("birthday");
					// bday = profile.getString("birthday");
					// atheletes = profile.getString("favorite_athletes");
					// teams = profile.getString("favorite_teams");

					email_get = email;

					facebook_image_url = "https://graph.facebook.com/" + id
							+ "/picture?type=large";

					// if (bday.equals(null)) {
					// bday = "Not Available";
					// }
					// if (atheletes.equals(null)) {
					// bday = "Not Available";
					// }
					// if (teams.equals(null)) {
					// bday = "Not Available";
					// }

					Log.e("--this is bday of user ---------------------------------------------------------",
							bday + bio_data);
					if (bday.equals(null) || bday.equals("")
							|| bio_data.equals(null) || bio_data.equals("")) {
						// bday = "";
						// bio_data = "";
						Log.e("-in if condition ---------------------------------------------------------",
								bday + bio_data);
						store_pref.setImageUrl(facebook_image_url);
						store_pref.setUserName(first_name);
						store_pref.setUserAge(bday);
						store_pref.setUserBio(bio_data);
					} else {
						store_pref.setImageUrl(facebook_image_url);
						store_pref.setUserName(first_name);
						store_pref.setUserAge(bday);
						store_pref.setUserBio(bio_data);
					}

					Log.e("Facebook user id here:- ", "" + id);
					Log.e("birth date ======>>>>>>>>>>>>>>>>", "" + bday
							+ "--------------------------bio----------"
							+ bio_data);
					Log.e("name data>>>>>>>>>>>>>>>>", "" + name);
					Log.e("bio data>>>>>>>>>>>>>>>>", "" + last_name);
					Log.e("link data>>>>>>>>>>>>>>>>", "" + link);
					Log.e("gender data>>>>>>>>>>>>>>>>", "" + gender);
					Log.e("image url>>>>>>>>>>>>>>>>", "" + facebook_image_url);
					// Log.e("athelets data>>>>>>>>>>>>>>>>", "" + atheletes);
					// Log.e("teams data>>>>>>>>>>>>>>>>", "" + teams);

					runOnUiThread(new Runnable() {

						@Override
						public void run() {

							// Toast.makeText(getApplicationContext(),
							// "Name: " + name + "\nEmail: " + email,
							// Toast.LENGTH_LONG).show();
							// new LoadImage().execute(facebook_image_url);

							LoginWithFacebook(email.replace(" ", "%20"),
									first_name.replace(" ", "%20"), "123456",
									facebook_image_url, id, bday, gender,
									latitude, longitude, bio_data);
						}

					});

				} catch (JSONException e) {
					e.printStackTrace();
					Log.e("JSONException", "" + e);
					// LoginWithFacebook(email.replace(" ", "%20"),
					// first_name.replace(" ", "%20"), "123456",
					// facebook_image_url, id, bday, gender, latitude,
					// longitude, bio_data);

				}
			}

			@Override
			public void onIOException(IOException e, Object state) {
				Log.e("IOException", "" + e);

			}

			@Override
			public void onFileNotFoundException(FileNotFoundException e,
					Object state) {
				Log.e("FileNotFoundException", "" + e);
			}

			@Override
			public void onMalformedURLException(MalformedURLException e,
					Object state) {
				Log.e("MalformedURLException", "" + e);
			}

			@Override
			public void onFacebookError(FacebookError e, Object state) {
				Log.e("FacebookError", "" + e);
			}
		});

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		facebook.authorizeCallback(requestCode, resultCode, data);

	}

	public void LoginWithFacebook(String email, String first_name,
			String password2, String facebook_image_url2, String id2,
			String string, String gender2, double latitude2, double longitude2,
			String bio_data2) {
		new AsyncTask<String, Integer, String>() {

			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();
				pdialog.show();
			}

			@Override
			protected String doInBackground(String... params) {
				JsonParser parser = new JsonParser();
				try {
					// JSONObject obj = new JSONObject("Response");
					JSONObject obj = parser
							.getJSONfromUrl("http://peapodapp.com/webservices/androidapi.php?type=sociallogin&email="
									+ email_get
									+ "&name="
									+ name.replace(" ", "%20")
									+ "&password=123456&profilepicurl="
									+ facebook_image_url
									+ "&profileid="
									+ id
									+ "&dob="
									+ bday
									+ "&gender="
									+ gender
									+ "&lat="
									+ latitude
									+ "&longt="
									+ longitude
									+ "&bio="
									+ bio_data.replace(" ", "%20"));

					System.out
							.print("url ====================================="
									+ "http://peapodapp.com/webservices/androidapi.php?type=sociallogin&email="
									+ email_get + "&name="
									+ name.replace(" ", "%20")
									+ "&password=123456&profilepicurl="
									+ facebook_image_url + "&profileid=" + id
									+ "&dob=" + bday + "&gender=" + gender
									+ "&lat=" + latitude + "&longt="
									+ longitude + "&bio=" + bio_data);
					JSONObject response = obj.getJSONObject("Response");
					// get_uid = response.getString("userid");
					System.out.println("response = " + response);
					JSONObject obj2 = response.getJSONObject("returnCode");
					success = obj2.optString("result");
					if (success.equals("0")) {
						get_uid = response.optString("userid");
					}
					System.out.println("user id = " + get_uid);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(String result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);
				if (pdialog.isShowing()) {
					pdialog.dismiss();
					try {
						if (success.equals("0")) {
							Toast.makeText(LoginScreen.this,
									"Login Succesfully", Toast.LENGTH_LONG)
									.show();
							store_pref.setUserid(get_uid);
							store_pref.setImageUrl(facebook_image_url);
							if (imageArraySize > 0) {
								for (int i = 0; i < listForImageUrl.size(); i++) {
									try {
										imageURL = URLEncoder.encode(
												listForImageUrl.get(i)
														.toString(), "utf-8");
									} catch (UnsupportedEncodingException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									saveImagesUrlToServer();
								}

							}

							Intent in = new Intent(LoginScreen.this,
									HomeScreen.class);
							in.putExtra("keyForImageUrl", facebook_image_url);
							in.putExtra("keyForname", name);
							in.putExtra("keyForEmail", email_get);
							in.putExtra("keyForgender", gender);
							startActivity(in);
							finish();
						} else {
							Toast.makeText(LoginScreen.this,
									"Incorrect username or password..",
									Toast.LENGTH_LONG).show();
						}
					} catch (NullPointerException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}
		}.execute();
	}

	private class ImagePagerAdapter extends PagerAdapter {

		private ArrayList<String> images;
		private LayoutInflater inflater;

		ImagePagerAdapter(ArrayList<String> listToGetImagesURL) {
			this.images = listToGetImagesURL;
			inflater = getLayoutInflater();
		}

		@Override
		public void destroyItem(View container, int position, Object object) {
			((ViewPager) container).removeView((View) object);
		}

		@Override
		public void finishUpdate(View container) {
		}

		@Override
		public int getCount() {
			return images.size();
		}

		@Override
		public Object instantiateItem(View view, int position) {
			final View imageLayout = inflater.inflate(
					R.layout.item_pager_image, null);
			final ImageView imageView = (ImageView) imageLayout
					.findViewById(R.id.image);
			imageLoader.DisplayImage(listToGetImagesURL.get(position),
					imageView);

			((ViewPager) view).addView(imageLayout, 0);
			return imageLayout;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view.equals(object);
		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {
		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View container) {
		}
	}

	public class DetailOnPageChangeListener extends
			ViewPager.SimpleOnPageChangeListener {

		private int currentPage;

		@Override
		public void onPageSelected(int position) {

			if (position == 0) {
				dot1.setImageResource(R.drawable.dot_fill);
				dot2.setImageResource(R.drawable.dot_empty);
				dot3.setImageResource(R.drawable.dot_empty);
				dot4.setImageResource(R.drawable.dot_empty);
			}
			if (position == 1) {
				dot1.setImageResource(R.drawable.dot_empty);
				dot2.setImageResource(R.drawable.dot_fill);
				dot3.setImageResource(R.drawable.dot_empty);
				dot4.setImageResource(R.drawable.dot_empty);
			}
			if (position == 2) {
				dot1.setImageResource(R.drawable.dot_empty);
				dot2.setImageResource(R.drawable.dot_empty);
				dot3.setImageResource(R.drawable.dot_fill);
				dot4.setImageResource(R.drawable.dot_empty);
			}
			if (position == 3) {
				dot1.setImageResource(R.drawable.dot_empty);
				dot2.setImageResource(R.drawable.dot_empty);
				dot3.setImageResource(R.drawable.dot_empty);
				dot4.setImageResource(R.drawable.dot_fill);
			}

			currentPage = position;
		}

		public int getCurrentPage() {
			return currentPage;
		}
	}

	@Override
	public void onBackPressed() {
	}

	// facebook get user information
	// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	// @SuppressWarnings("unused")
	// private String buildUserInfoDisplay(GraphUser user) {
	// StringBuilder userInfo = new StringBuilder("");
	//
	// // Example: typed access (name)
	// // - no special permissions required
	// userInfo.append(String.format("Name: %s\n\n", user.getName()));
	//
	// // Example: typed access (birthday)
	// // - requires user_birthday permission
	// userInfo.append(String.format("Birthday: %s\n\n", user.getBirthday()));
	//
	// // Example: partially typed access, to location field,
	// // name key (location)
	// // - requires user_location permission
	// userInfo.append(String.format("Location: %s\n\n", user.getLocation()
	// .getProperty("name")));
	//
	// // Example: access via property name (locale)
	// // - no special permissions required
	// userInfo.append(String.format("Locale: %s\n\n",
	// user.getProperty("locale")));
	//
	// // Example: access via key for array (languages)
	// // - requires user_likes permission
	// JSONArray languages = (JSONArray) user.getProperty("languages");
	// if (languages.length() > 0) {
	// ArrayList<String> languageNames = new ArrayList<String>();
	// for (int i = 0; i < languages.length(); i++) {
	// JSONObject language = languages.optJSONObject(i);
	// // Add the language name to a list. Use JSON
	// // methods to get access to the name field.
	// languageNames.add(language.optString("name"));
	// }
	// userInfo.append(String.format("Languages: %s\n\n",
	// languageNames.toString()));
	// }
	//
	// return userInfo.toString();
	// }
	//
	@Override
	public void response(String response, int pageid) {

		System.out.println("respnse hereee= " + response);
		String full_iamge_path = null;
		if (pageid == 1) {
			if (pdialog.isShowing()) {
				pdialog.dismiss();

			}
			try {
				JSONObject jsonResponse = new JSONObject(response);

				// status = jsonResponse.optString("Response");
				Log.e("status.................", "" + status);

				JSONArray jsonMainNode = jsonResponse.optJSONArray("list");
				for (int i = 0; i < jsonMainNode.length(); i++) {
					JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
					imageURLString = jsonChildNode.optString("imgurl");
					imageNameString = jsonChildNode.optString("imgname");
					full_iamge_path = imageURLString + imageNameString;
					listToGetImagesURL.add(full_iamge_path);
					Log.e("images url", "" + full_iamge_path);
				}
				pager.setAdapter(new ImagePagerAdapter(listToGetImagesURL));
			} catch (JSONException e) {
				if (pdialog.isShowing()) {
					pdialog.dismiss();

				}
				e.printStackTrace();
			}
			// pager.setAdapter(new ImagePagerAdapter(listToGetImagesURL));
		}
	}

	@Override
	public void error(String error) {
		// TODO Auto-generated method stub
	}

	private class LoadImage extends AsyncTask<String, String, Bitmap> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// pDialog = new ProgressDialog(MainActivity.this);
			// pDialog.setMessage("Loading Image ....");
			// pDialog.show();
		}

		protected Bitmap doInBackground(String... args) {
			try {
				bitmap = BitmapFactory.decodeStream((InputStream) new URL(
						args[0]).getContent());
			} catch (Exception e) {
				e.printStackTrace();
			}
			return bitmap;
		}

		protected void onPostExecute(Bitmap image) {
			if (image != null) {
				// img.setImageBitmap(image);
				// pDialog.dismiss();
				try {
					new ImageUploadTask().execute();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				// pDialog.dismiss();
				Toast.makeText(LoginScreen.this,
						"Image Does Not exist or Network Error",
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	class ImageUploadTask extends AsyncTask<Void, Void, String> {
		private String webAddressToPost = "http://peapodapp.com/webservices/androidapi.php?type=uploadimage";

		// private ProgressDialog dialog;
		// private ProgressDialog dialog = new ProgressDialog(LoginScreen.this);

		@Override
		protected void onPreExecute() {
			// dialog.setMessage("Uploading...");
			// dialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			try {
				HttpClient httpClient = new DefaultHttpClient();
				HttpContext localContext = new BasicHttpContext();
				HttpPost httpPost = new HttpPost(webAddressToPost);

				MultipartEntity entity = new MultipartEntity(
						HttpMultipartMode.BROWSER_COMPATIBLE);

				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				bitmap.compress(CompressFormat.JPEG, 100, bos);
				byte[] data = bos.toByteArray();
				String file = Base64.encodeBytes(data);
				Log.e("file>>>>>>>>>>>>>>>>>>", "" + file);
				entity.addPart("uploaded", new StringBody(file));
				entity.addPart("someOtherStringToSend", new StringBody(
						"your string here"));

				httpPost.setEntity(entity);
				HttpResponse response = httpClient.execute(httpPost,
						localContext);
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(
								response.getEntity().getContent(), "UTF-8"));

				String sResponse = reader.readLine();
				return sResponse;
			} catch (Exception e) {
				// something went wrong. connection with the server error
				Log.e("upload error :-", "" + e);
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// dialog.dismiss();
			Toast.makeText(getApplicationContext(), "file uploaded",
					Toast.LENGTH_LONG).show();
		}
	}

	@SuppressWarnings("deprecation")
	public void getFacebookAlbumImages() {
		mAsyncRunner.request("/" + listToGetAlbumId.get(0).toString()
				+ "/photos", new RequestListener() {

			@Override
			public void onMalformedURLException(MalformedURLException e,
					Object state) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onIOException(IOException e, Object state) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onFileNotFoundException(FileNotFoundException e,
					Object state) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onFacebookError(FacebookError e, Object state) {
				// // TODO Auto-generated method stub

			}

			//
			@Override
			public void onComplete(String response, Object state) {
				Log.e("-response of user photos here----------------------------",
						"" + response);

				String json = response;
				try {
					JSONObject profile = new JSONObject(json);

					JSONArray data = profile.getJSONArray("data");

					for (int i = 0; i < data.length(); i++) {
						JSONObject getdata = data.optJSONObject(i);
						JSONArray images = getdata.getJSONArray("images");

						imageArraySize = images.length();

						if (images.length() == 0) {
							// continue;
						} else {
							for (int j = 0; j < 1; j++) {
								JSONObject source = images.optJSONObject(j);
								String url = source.optString("source");
								listForImageUrl.add(url);

							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void saveImagesUrlToServer() {

		if (imageURL.equals("")) {

			imageURL = "http://peapodapp.com//webservices/images/logoicon.png";
		}

		Log.d("imageurlllllllllllllllllllllllllllllllll", imageURL);

		String url = "http://peapodapp.com/webservices/androidapi.php?type=userprofileimages&userid="
				+ get_uid + "&imageurl=" + imageURL;

		System.out.println("save image server url here:- " + url);
		Request imagesURL = (Request) new Request(url) {

			@Override
			protected void onSuccess(Transport transport) {

				JSONObject response = transport.getResponseJson();
				System.out.println(" images response here:- --------------- "
						+ response);

				try {
					JSONObject getResponse = response.getJSONObject("Response");
					JSONObject getResult = getResponse
							.getJSONObject("returnCode");
					String successnew = getResult.getString("resultText");
					if (successnew.equals("Success")) {

					} else {
						Toast.makeText(getApplicationContext(),
								"An error occur. Please try again later",
								Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			@Override
			protected void onError(IOException ex) {
				Toast.makeText(getApplicationContext(), "Eorror occur: " + ex,
						Toast.LENGTH_LONG).show();
			}

			@Override
			protected void onFailure(Transport transport) {
				Toast.makeText(getApplicationContext(),
						"An error occur. Please try again later",
						Toast.LENGTH_LONG).show();
			}

		}.execute("POST");
		imagesURL.accept(Request.CTYPE_JSON);
		imagesURL.setContentType(Request.CTYPE_JSON);

	}

	public static String printKeyHash(Activity context) {
		PackageInfo packageInfo;
		String key = null;
		try {
			// getting application package name, as defined in manifest
			String packageName = context.getApplicationContext()
					.getPackageName();

			// Retriving package info
			packageInfo = context.getPackageManager().getPackageInfo(
					packageName, PackageManager.GET_SIGNATURES);

			Log.e("Package Name=", context.getApplicationContext()
					.getPackageName());

			for (Signature signature : packageInfo.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				// key = new String(Base64.encode(md.digest(), 0));

				key = new String(Base64.encodeBytes(md.digest()));
				Log.d("Key Hash=", key);
			}
		} catch (NameNotFoundException e1) {
			Log.e("Name not found", e1.toString());
		} catch (NoSuchAlgorithmException e) {
			Log.e("No such an algorithm", e.toString());
		} catch (Exception e) {
			Log.e("Exception", e.toString());
		}

		return key;
	}

}