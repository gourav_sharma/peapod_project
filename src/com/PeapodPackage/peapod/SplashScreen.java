package com.PeapodPackage.peapod;

import com.Peapod.Connections.StorePref;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class SplashScreen extends Activity implements AnimationListener {

	private static long Sleep_Time = 2;
	private static String TAG = SplashScreen.class.getName();
	StorePref pref;
	String user_id;
	int check;
	private ImageView imgLogoIcon;

	// Animation
	Animation animMove;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.splashscreen);

		imgLogoIcon = (ImageView) findViewById(R.id.logo_icon);

		pref = StorePref.getInstance(this);
		user_id = pref.getUserid();
		// check = pref.getCheck();

		animMove = AnimationUtils.loadAnimation(getApplicationContext(),
				R.anim.move);

		// set animation listener
		imgLogoIcon.startAnimation(animMove);
		animMove.setAnimationListener(this);

	}

	private class IntentLauncher extends Thread {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			try {
				Thread.sleep(Sleep_Time * 1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				Log.i(TAG, e.getMessage());
			}
			//
			// Log.e("Check", "out" + check);
			// Log.e("out", "out");
			if (user_id.equals("") || user_id.equals(null)) {

				// pref.setCheck(0);
				// pref.setUserid("");
				Log.e("user id msg1", "if.. login ..." + user_id);
				Intent in = new Intent(SplashScreen.this, LoginScreen.class);
				SplashScreen.this.startActivity(in);
				SplashScreen.this.finish();

			} else {
				Log.e("user id msg2", "else.. home....." + user_id);
				Intent in = new Intent(SplashScreen.this, HomeScreen.class);
				SplashScreen.this.startActivity(in);
				SplashScreen.this.finish();
			}

		}

	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		System.out.println("Home presed");
		if (keyCode == KeyEvent.KEYCODE_HOME) {
			System.out.println("Home presed  in side");
			finish();
			System.exit(0);
		}
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
			System.exit(0);
		}
		return false;
	}

	@Override
	public void onBackPressed() {

	}

	@Override
	public void onAnimationStart(Animation animation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAnimationEnd(Animation animation) {
		// TODO Auto-generated method stub
		IntentLauncher in = new IntentLauncher();
		in.start();

	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub

	}

}
