package com.PeapodPackage.peapod;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import se.tonyivanov.android.ajax.Request;
import se.tonyivanov.android.ajax.Transport;

import com.Peapod.Connections.ConnectionStatus;
import com.Peapod.Connections.HttpGetRespone;
import com.Peapod.Connections.HttpResponseListener;
import com.Peapod.Connections.StorePref;
import com.Peapod.Connections.TransparentProgressDialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Edit_quiz_manager extends Activity implements OnClickListener,
		HttpResponseListener {

	String questionID = null, option_one, option_two;
	private ListView listViewForQuiz;
	public GridView gridForChk;
	StorePref pref;
	Boolean chkCount = false;
	ConnectionStatus statusis;
	AlertDialog alertDialog;
	CheckBox chkForAll;
	boolean isConnected;
	TransparentProgressDialog pdialog;
	private String answer;
	ImageButton back;
	private String option_one_for_list;
	private String option_two_for_list;
	private String answer_for_list;
	private String id_for_list;
	int benchValue;

	private String userID;
	int green;

	private ArrayList<String> listForOptionOne = new ArrayList<String>();
	private ArrayList<String> listForOptionTwo = new ArrayList<String>();
	private ArrayList<String> listForListAnswer = new ArrayList<String>();
	private ArrayList<String> listForListId = new ArrayList<String>();
	private ArrayList<Integer> listForListSno = new ArrayList<Integer>();
	private String userQuestionListURL = "http://peapodapp.com/webservices/androidapi.php?type=userquestionslist&userid=";

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_quiz_manager);

		green = getApplicationContext().getResources().getColor(R.color.green);

		listViewForQuiz = (ListView) findViewById(R.id.list_for_quiz);
		back = (ImageButton) findViewById(R.id.arrow);
		back.setOnClickListener(this);

		benchValue = getIntent().getIntExtra("keyForBenchValue", 0);
		System.out.println("bnch value......................." + benchValue);

		pref = StorePref.getInstance(this);
		userID = pref.getUserid();
		Log.e("user ID", "" + userID);

		// hit Admin Question Web API
		statusis = new ConnectionStatus(this);
		pdialog = new TransparentProgressDialog(this, R.drawable.loader);
		pdialog.show();
		isConnected = statusis.isConnected();
		if (isConnected) {
			try {
				new HttpGetRespone(userQuestionListURL + userID, this, 1)
						.execute();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {

			if (pdialog.isShowing())
				pdialog.dismiss();
			alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle("StashApp");
			alertDialog.setMessage("No Internet Connection Available");
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			alertDialog.show();
		}

	}

	public class ListAdapterNew extends BaseAdapter {

		private Activity activity;
		ArrayList<String> idAdap;
		ArrayList<String> opt1Adap;
		ArrayList<String> opt2Adap;
		ArrayList<String> ansAdap;
		ArrayList<Integer> countAdap;
		private LayoutInflater inflater = null;

		public ListAdapterNew(Activity a, ArrayList<String> id,
				ArrayList<String> opt1, ArrayList<String> opt2,
				ArrayList<String> ans, ArrayList<Integer> coun) {

			activity = a;
			idAdap = id;
			opt1Adap = opt1;
			opt2Adap = opt2;
			ansAdap = ans;
			countAdap = coun;
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listForListId.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub

			View vi = convertView;
			if (convertView == null)
				vi = inflater.inflate(R.layout.edit_quiz_list_layout, null);

			final ImageView chk = (ImageView) vi.findViewById(R.id.imgForCheck);
			final ImageView chk2 = (ImageView) vi
					.findViewById(R.id.imgForCheck2);

			EditText txtForOpt1 = (EditText) vi
					.findViewById(R.id.txtForOption1);
			EditText txtForOpt2 = (EditText) vi
					.findViewById(R.id.txtForOption2);
			TextView forSnoNew = (TextView) vi
					.findViewById(R.id.txtForSerialNo);
			forSnoNew.setText("" + countAdap.get(position));
			txtForOpt1.setText(opt1Adap.get(position));
			txtForOpt2.setText(opt2Adap.get(position));

			if (listForListAnswer.get(position).equals(
					listForOptionOne.get(position))) {
				// answer = opt1Adap.get(position);
				chk.setVisibility(View.VISIBLE);
				chk2.setVisibility(View.GONE);
			} else {
				chk.setVisibility(View.GONE);
				chk2.setVisibility(View.VISIBLE);
				// answer = opt2Adap.get(position);
			}

			ImageView chkBox = (ImageView) vi.findViewById(R.id.imgForCheckbox);
			ImageView chkBox2 = (ImageView) vi
					.findViewById(R.id.imgForCheckbox2);
			final EditText textForOption1 = (EditText) vi
					.findViewById(R.id.txtForOption1);
			final EditText textForOption2 = (EditText) vi
					.findViewById(R.id.txtForOption2);

			textForOption1.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {

				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// chkCount = true;
					chk.setVisibility(View.GONE);
					chk2.setVisibility(View.GONE);

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			textForOption2.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub

				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// chkCount = true;
					chk.setVisibility(View.GONE);
					chk2.setVisibility(View.GONE);
				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			chkBox.setOnClickListener(new OnClickListener() {

				Boolean status = false;

				@Override
				public void onClick(View v) {

					option_one = textForOption1.getText().toString();
					option_two = textForOption2.getText().toString();
					answer = textForOption1.getText().toString();
					questionID = idAdap.get(position);
					// Toast.makeText(
					// getApplicationContext(),
					// "1." + option_one + "2." + option_two + "ans:"
					// + answer + "ID." + questionID,
					// Toast.LENGTH_LONG).show();
					if (option_one.equals(null) || option_one.equals("")
							|| option_two.equals("") || option_two.equals(null)) {
						Toast.makeText(getApplicationContext(),
								"Enter Both Options", Toast.LENGTH_LONG).show();
					} else {
						if (status == false) {
							chk.setVisibility(View.VISIBLE);
							chk2.setVisibility(View.GONE);
							chkCount = true;
							// pdialog.show();
							// RunRequestUpdate();
							status = true;
						} else {
							chk.setVisibility(View.GONE);
							chk2.setVisibility(View.VISIBLE);
							status = false;
						}
					}
				}
			});

			chkBox2.setOnClickListener(new OnClickListener() {
				Boolean status = false;

				@Override
				public void onClick(View v) {

					option_one = textForOption1.getText().toString();
					option_two = textForOption2.getText().toString();
					answer = textForOption2.getText().toString();
					questionID = idAdap.get(position);

					if (option_one.equals(null) || option_one.equals("")
							|| option_two.equals("") || option_two.equals(null)) {
						Toast.makeText(getApplicationContext(),
								"Enter Both Options", Toast.LENGTH_LONG).show();
					} else {
						if (status == false) {
							chk.setVisibility(View.GONE);
							chk2.setVisibility(View.VISIBLE);
							chkCount = true;
							// pdialog.show();
							// RunRequestUpdate();
							status = true;
						} else {
							chk.setVisibility(View.VISIBLE);
							chk2.setVisibility(View.GONE);
							status = false;
						}
					}
				}
			});

			ImageView imgForSave = (ImageView) vi.findViewById(R.id.imgForSave);
			imgForSave.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					option_one = textForOption1.getText().toString();
					option_two = textForOption2.getText().toString();
					// answer = listForListAnswer.get(position);
					questionID = idAdap.get(position);
					Log.d("....................", "1. " + option_one + "2. "
							+ option_two + "ans. " + answer + "count. "
							+ chkCount);

					if (chkCount == false) {
						Toast.makeText(getApplicationContext(),
								"Make changes & Enter detail properly",
								Toast.LENGTH_LONG).show();
					} else {
						if (option_one.equals(null) || option_one.equals("")
								|| option_two.equals("")
								|| option_two.equals(null)
								|| chk.getVisibility() == View.GONE
								&& chk2.getVisibility() == View.GONE) {
							Toast.makeText(
									getApplicationContext(),
									"Enter Both Options And Select an answer"
											+ "..............."
											+ chk.getVisibility(),
									Toast.LENGTH_LONG).show();
						} else {
							//
							// Toast.makeText(
							// getApplicationContext(),
							// "1." + option_one + "2." + option_two
							// + "ans:" + answer + "ID."
							// + questionID, Toast.LENGTH_LONG)
							// .show();
							pdialog.show();
							RunRequestUpdate();
							// status = true;
						}
					}
				}
			});
			ImageView imgForDelete = (ImageView) vi
					.findViewById(R.id.imgForDelete);

			imgForDelete.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					pdialog.show();
					questionID = idAdap.get(position);

					// if (benchValue > listForListId.size() - 1) {
					// benchValue = listForListId.size() - 1;
					int sizeList = listForListId.size() - 1;
					if (sizeList < 5) {
						benchValue = 0;
						System.out.println("    bnch mark in if :-----  "
								+ benchValue);
						setBenchMark();
					} else {
						benchValue = benchValue;
						System.out.println("    bnch mark in else :-----  "
								+ benchValue);
						setBenchMark();
					}

					// }
					delete_question();
				}
			});

			return vi;
		}
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.arrow:
			Intent back = new Intent(Edit_quiz_manager.this, Quiz_manager.class);
			startActivity(back);
			finish();
			break;
		default:
			break;
		}
	}

	@Override
	public void response(String response, int pageid) {
		System.out.println("respnse hereee= " + response);

		if (pageid == 1) {
			if (pdialog.isShowing()) {
				pdialog.dismiss();
			}
			try {
				listForListAnswer.clear();
				listForOptionOne.clear();
				listForOptionTwo.clear();
				listForListId.clear();
				JSONObject jsonResponse = new JSONObject(response);
				int count = 1;
				JSONArray jsonMainNode = jsonResponse.optJSONArray("list");
				for (int i = 0; i < jsonMainNode.length(); i++) {
					JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
					option_one_for_list = jsonChildNode.optString("option1");
					option_two_for_list = jsonChildNode.optString("option2");
					answer_for_list = jsonChildNode.optString("answer");
					id_for_list = jsonChildNode.optString("id");

					listForListAnswer.add(answer_for_list);
					listForOptionOne.add(option_one_for_list);
					listForOptionTwo.add(option_two_for_list);
					listForListId.add(id_for_list);

					listForListSno.add(count);
					count++;

					// txtForOptionOne.setText(option_one);
					// txtForOptionTwo.setText(option_two);
					Log.e("id", "" + id_for_list);
					Log.e("option1", "" + option_one_for_list);
					Log.e("option2", "" + option_two_for_list);
					Log.e("answer", "" + answer);
				}
				adapter();

			} catch (JSONException e) {
				if (pdialog.isShowing()) {
					pdialog.dismiss();

				}
				e.printStackTrace();
			}
			// if (status == true)
			// initiatePopupWindow();
		}

	}

	@Override
	public void error(String error) {
		// TODO Auto-generated method stub

	}

	public void adapter() {
		ListAdapterNew adap = new ListAdapterNew(this, listForListId,
				listForOptionOne, listForOptionTwo, listForListAnswer,
				listForListSno);
		listViewForQuiz.setAdapter(adap);
	}

	@Override
	public void onBackPressed() {

	}

	public void delete_question() {
		String url = "http://peapodapp.com/webservices/androidapi.php?type=deluserques&userid="
				+ userID + "&quesid=" + questionID;
		url = url.replace(" ", "%20");
		System.out.println("URL ----------- = " + url);
		Request add = (Request) new Request(url) {
			@Override
			protected void onSuccess(Transport transport) {
				if (pdialog.isShowing()) {
					pdialog.dismiss();
				}
				try {
					JSONObject response = transport.getResponseJson();
					JSONObject response2 = response.getJSONObject("Response");
					System.out.println("response = " + response2);
					String success = response2.optString("resultText");
					Log.e("responce string", "" + success);
					if (success.equals("Success")) {
						Toast.makeText(getApplicationContext(),
								"Deleted successfuly", 5000).show();
						// new HttpGetRespone(userQuestionListURL + userID,
						// Quiz_manager.this, 4).execute();
						refreshActivity();
					}

				} catch (Exception e) {
					if (pdialog.isShowing()) {
						pdialog.dismiss();
					}
					e.printStackTrace();
				}
			}
		}.execute("GET");
		add.accept(Request.CTYPE_JSON);
		add.setContentType(Request.CTYPE_JSON);
	}

	public void RunRequestUpdate() {

		// "1&option1=abc&option2=def&answer=abc"

		String url = "http://peapodapp.com/webservices/androidapi.php?type=updateuserques&quesid="
				+ questionID
				+ "&option1="
				+ option_one
				+ "&option2="
				+ option_two + "&answer=" + answer;
		url = url.replace(" ", "%20");
		System.out.println("URL ----------- = " + url);
		Request add = (Request) new Request(url) {
			@Override
			protected void onSuccess(Transport transport) {
				if (pdialog.isShowing()) {
					pdialog.dismiss();
				}
				try {
					JSONObject response = transport.getResponseJson();
					JSONObject response2 = response.getJSONObject("Response");
					System.out.println("response = " + response2);
					String success = response2.optString("resultText");
					Log.e("responce string", "" + success);
					if (success.equals("Success")) {
						Toast.makeText(getApplicationContext(),
								"Save successfuly", 5000).show();
						// new HttpGetRespone(userQuestionListURL + userID,
						// Quiz_manager.this, 4).execute();
						refreshActivity();
					}

				} catch (Exception e) {
					if (pdialog.isShowing()) {
						pdialog.dismiss();
					}
					e.printStackTrace();
				}
			}
		}.execute("GET");
		add.accept(Request.CTYPE_JSON);
		add.setContentType(Request.CTYPE_JSON);
	}

	@Override
	protected void onDestroy() {
		chkCount = true;
		super.onDestroy();
	}

	public void refreshActivity() {
		Intent refresh = new Intent(Edit_quiz_manager.this,
				Edit_quiz_manager.class);
		startActivity(refresh);
		finish();
	}

	public void setBenchMark() {

		String url = "http://peapodapp.com/webservices/androidapi.php?type=setbenchmark&userid="
				+ userID
				+ "&benchmark_value="
				+ benchValue
				+ "&benchmark_total=" + listForListId.size();
		url = url.replace(" ", "%20");
		System.out.println("URL ----------- = " + url);
		Request add = (Request) new Request(url) {
			@Override
			protected void onSuccess(Transport transport) {
				if (pdialog.isShowing()) {
					pdialog.dismiss();
				}
				try {
					JSONObject response = transport.getResponseJson();
					JSONObject response2 = response.getJSONObject("Response");
					JSONObject r3 = response2.getJSONObject("returnCode");
					System.out.println("response = " + response2);
					String success = r3.optString("resultText");
					Log.e("responce string", "" + success
							+ "............................" + success);
					if (success.equals("Success")) {
						Toast.makeText(getApplicationContext(),
								"Save successfuly", 5000).show();
						// refreshActivity();
					}
				} catch (Exception e) {
					if (pdialog.isShowing()) {
						pdialog.dismiss();
					}
					e.printStackTrace();
				}
			}
		}.execute("GET");
		add.accept(Request.CTYPE_JSON);
		add.setContentType(Request.CTYPE_JSON);
	}
}
