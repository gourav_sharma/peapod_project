package com.PeapodPackage.peapod;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.Peapod.Connections.ConnectionStatus;
import com.Peapod.Connections.HttpGetRespone;
import com.Peapod.Connections.HttpResponseListener;
import com.Peapod.Connections.StorePref;
import com.Peapod.Connections.TransparentProgressDialog;
import com.Peapod.Connections.WindowedSeekBar;
import com.PeapodPackage.peapod.Quiz_manager.gridAdapter;
import com.PeapodPackage.peapod.RangeSeekBar.OnRangeSeekBarChangeListener;

public class Visibility_Setting extends Activity implements OnClickListener,
		OnSeekBarChangeListener, HttpResponseListener {

	TextView minAge, maxAge;
	WindowedSeekBar RangeSeekBar;
	double floor = 10.5;
	double ceiling = 30.0;

	LinearLayout layout;

	private ToggleButton toggle_ButtonNew;
	private SeekBar search_range, spinner1, spinner2;
	// private Spinner spinner1, spinner2;
	int item, green, visibilityStatus, item2, getItemValue;
	private String userId, get_intrestedIn, getVisibility, intrestedIn;
	int getAgeTo;
	int getAgeFrom;
	int getRange;
	int progressChanged = 0;
	int progressChangedMinAge;
	int progressChangedMaxAge;
	int count = 0;
	int getx, gety;
	private Button backBtn;
	private ImageView chkFormale, chkForFemale, tickMale, tickFemale;
	private TextView txtForShowRange, txtformale, txtforfemale,
			txtForShowIntrested, txtsetVisibility, txtForShowVisibility,
			txtforFrom, txtForTo;
	public Boolean status = false, maleSelected = false,
			femaleSelected = false;
	// List<Integer> list2;
	// List<Integer> list;
	// ArrayAdapter<Integer> dataAdapter2;
	// ArrayAdapter<Integer> dataAdapter;
	ConnectionStatus statusis;
	public gridAdapter adapter;
	AlertDialog alertDialog;
	boolean isConnected;
	TransparentProgressDialog pdialog;

	private StorePref pref;
	private String getSettingURL = "http://peapodapp.com/webservices/androidapi.php?type=getusersettings&userid=";
	private String setIntrestedinURL = "http://peapodapp.com/webservices/androidapi.php?type=setusersettings_1&userid=";
	private String setVisibilityURL = "http://peapodapp.com/webservices/androidapi.php?type=setusersettings_3&userid=";
	private String setSearchRangeURL = "http://peapodapp.com/webservices/androidapi.php?type=setusersettings_2&userid=";
	private String setAgeRangeURL = "http://peapodapp.com/webservices/androidapi.php?type=setusersettings_4&userid=";

	RangeSeekBar<Integer> seekBar;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.visibolity_setting);
		minAge = (TextView) findViewById(R.id.txtForShowsetAgeMin1);
		maxAge = (TextView) findViewById(R.id.txtForShowsetAgeMax1);

		layout = (LinearLayout) findViewById(R.id.layoutliner);

		seekBar = new RangeSeekBar<Integer>(18, 60, Visibility_Setting.this);

		seekBar.setOnRangeSeekBarChangeListener(new OnRangeSeekBarChangeListener<Integer>() {

			@Override
			public void onRangeSeekBarValuesChanged(
					com.PeapodPackage.peapod.RangeSeekBar<?> bar,
					Integer minValue, Integer maxValue) {
				// TODO Auto-generated method stub

				minAge.setText("From: " + minValue);
				maxAge.setText("To: " + maxValue);
				progressChangedMinAge = minValue;
				progressChangedMaxAge = maxValue;
				updateAgeRange();

			}

		});

		// RangeSeekBar = (WindowedSeekBar) findViewById(R.id.windowedseekbar);
		//
		// RangeSeekBar.setSeekBarChangeListener(new SeekBarChangeListener() {
		//
		// @Override
		// public void SeekBarValueChanged(int Thumb1Value, int thumblX,
		// int Thumb2Value, int thumbrX, int width, int thumbY) {
		// // TODO Auto-generated method stub
		//
		// minAge.setText("From: " + Thumb1Value);
		// maxAge.setText("To: " + Thumb2Value);
		// progressChangedMinAge = Thumb1Value;
		// progressChangedMaxAge = Thumb2Value;
		// updateAgeRange();
		// }
		// });
		layout.addView(seekBar);
		green = getApplicationContext().getResources().getColor(R.color.green);

		pref = StorePref.getInstance(getApplicationContext());
		userId = pref.getUserid();
		Log.d("user id-----------------------", "" + userId);

		spinner1 = (SeekBar) findViewById(R.id.spinner1);
		spinner2 = (SeekBar) findViewById(R.id.spinner2);
		search_range = (SeekBar) findViewById(R.id.seekBarForRange);
		search_range.setOnSeekBarChangeListener(this);

		txtForShowRange = (TextView) findViewById(R.id.txtForShowRange);
		txtformale = (TextView) findViewById(R.id.txtForMale);
		txtforfemale = (TextView) findViewById(R.id.txtForFemale);
		txtForShowIntrested = (TextView) findViewById(R.id.txtForShowintrested);
		txtsetVisibility = (TextView) findViewById(R.id.txtsetVisibility);
		txtForShowVisibility = (TextView) findViewById(R.id.txtForShowVisibility);
		// txtForShowsetAge = (TextView) findViewById(R.id.txtForShowsetAge);
		txtforFrom = (TextView) findViewById(R.id.txtForShowsetAge);
		txtForTo = (TextView) findViewById(R.id.txtForShowsetAgeMax);
		// txtForsetAge = (TextView) findViewById(R.id.txtForShowsetAgeMax);

		chkForFemale = (ImageView) findViewById(R.id.chkForFemale);
		chkFormale = (ImageView) findViewById(R.id.chkForMale);
		chkForFemale.setOnClickListener(this);
		chkFormale.setOnClickListener(this);

		tickFemale = (ImageView) findViewById(R.id.chkTickForFemale);
		tickMale = (ImageView) findViewById(R.id.chkTickForMale);

		toggle_ButtonNew = (ToggleButton) findViewById(R.id.ToggleBtn);
		backBtn = (Button) findViewById(R.id.back_btn);
		backBtn.setOnClickListener(this);

		// tv1 = (TextView) findViewById(R.id.textView1);
		// tv2 = (TextView) findViewById(R.id.textView2);
		// swtt = (SeekBarWithTwoThumb) findViewById(R.id.myseekbar);
		// swtt.setSeekBarChangeListener(this);
		toggle_ButtonNew
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {

						if (isChecked) {
							txtForShowVisibility.setText("ON");
							txtsetVisibility.setTextColor(green);
							visibilityStatus = 1;

							if (count != 0)
								updateVisibilitySetting();
						} else {
							txtForShowVisibility.setText("OFF");
							txtsetVisibility.setTextColor(Color.BLACK);
							visibilityStatus = 0;
							if (count != 0)
								updateVisibilitySetting();
						}
					}
				});

		// hit Admin Question Web API
		statusis = new ConnectionStatus(this);
		pdialog = new TransparentProgressDialog(this, R.drawable.loader);
		pdialog.show();
		isConnected = statusis.isConnected();
		if (isConnected) {
			try {
				new HttpGetRespone(getSettingURL + userId, this, 1).execute();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {

			if (pdialog.isShowing())
				pdialog.dismiss();
			alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle("Peapod");
			alertDialog.setMessage("No Internet Connection Available");
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			alertDialog.show();
		}

		search_range.setMax(180);

		// RangeSeekBar.setSelectedMinValue(18);
		// RangeSeekBar.setSelectedMaxValue(100);

		// rangeSeekBar.setSelectedMinValue(20);
		// rangeSeekBar.setSelectedMaxValue(88);
		spinner2.setMax(100);

		spinner1.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// updateAgeRange();
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// if (progress < 18) {
				// spinner1.setProgress(18);
				// } else {
				// progress = getAgeTo;
				// progressChangedMinAge = progress;
				// }
				// txtforFrom.setText("" + progressChangedMinAge);
			}
		});

		spinner2.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// updateAgeRange();
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {

				// progress = getAgeFrom;
				// progressChangedMaxAge = progress;
				//
				// if (progress <= progressChangedMinAge) {
				// spinner2.setProgress(progressChangedMinAge + 1);
				// } else {
				// progressChangedMaxAge = progress;
				// }
				//
				// txtForTo.setText("" + progressChangedMaxAge);
				// spinner1.setMax(progressChangedMaxAge - 1);

			}
		});

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.chkForFemale:
			if (status == false) {
				tickFemale.setVisibility(View.VISIBLE);
				txtforfemale.setTextColor(green);
				femaleSelected = true;
				if (femaleSelected == true && maleSelected == false) {
					txtForShowIntrested.setText("Female");
					intrestedIn = "Female";
				} else if (femaleSelected == false && maleSelected == true) {
					txtForShowIntrested.setText("Male");
					intrestedIn = "Male";
				} else if (femaleSelected == true && maleSelected == true) {
					txtForShowIntrested.setText("Both");
					intrestedIn = "Both";
				} else if (maleSelected == false && femaleSelected == false) {
					txtForShowIntrested.setText("Default Both");
					intrestedIn = "Both";
				}
				status = true;
				updateIntrestesIn();
			} else {
				tickFemale.setVisibility(View.GONE);
				txtforfemale.setTextColor(Color.BLACK);
				femaleSelected = false;
				if (femaleSelected == true && maleSelected == false) {
					txtForShowIntrested.setText("Female");
					intrestedIn = "Female";
				} else if (femaleSelected == false && maleSelected == true) {
					txtForShowIntrested.setText("Male");
					intrestedIn = "Male";
				} else if (femaleSelected == true && maleSelected == true) {
					txtForShowIntrested.setText("Both");
					intrestedIn = "Both";
				} else if (maleSelected == false && femaleSelected == false) {
					txtForShowIntrested.setText("Default Both");
					intrestedIn = "Both";
				}
				status = false;
				updateIntrestesIn();
			}
			break;

		case R.id.chkForMale:
			if (status == false) {
				tickMale.setVisibility(View.VISIBLE);
				txtformale.setTextColor(green);
				maleSelected = true;
				if (femaleSelected == true && maleSelected == false) {
					txtForShowIntrested.setText("Female");
					intrestedIn = "Female";
				} else if (femaleSelected == false && maleSelected == true) {
					txtForShowIntrested.setText("Male");
					intrestedIn = "Male";
				} else if (femaleSelected == true && maleSelected == true) {
					txtForShowIntrested.setText("Both");
					intrestedIn = "Both";
				} else if (maleSelected == false && femaleSelected == false) {
					txtForShowIntrested.setText("Default Both");
					intrestedIn = "Both";
				}
				status = true;
				updateIntrestesIn();
			} else {
				tickMale.setVisibility(View.GONE);
				txtformale.setTextColor(Color.BLACK);
				maleSelected = false;
				if (femaleSelected == true && maleSelected == false) {
					txtForShowIntrested.setText("Female");
					intrestedIn = "Female";
				} else if (femaleSelected == false && maleSelected == true) {
					txtForShowIntrested.setText("Male");
					intrestedIn = "Male";
				} else if (femaleSelected == true && maleSelected == true) {
					txtForShowIntrested.setText("Both");
					intrestedIn = "Both";
				} else if (maleSelected == false && femaleSelected == false) {
					txtForShowIntrested.setText("Default Both");
					intrestedIn = "Both";
				}
				status = false;
				updateIntrestesIn();
			}
			break;
		case R.id.back_btn:
			if (visibilityStatus == 1) {
				Intent back = new Intent(Visibility_Setting.this, Setting.class);
				startActivity(back);
				finish();
			} else {
				Intent back = new Intent(Visibility_Setting.this,
						HomeScreen.class);
				startActivity(back);
				finish();
			}

			break;

		default:
			break;
		}

	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {

		switch (seekBar.getId()) {
		case R.id.seekBarForRange:

			progressChanged = progress;
			if (progressChanged < 24)
				progressChanged = 25;
			txtForShowRange.setText(progressChanged + " Km");
			Log.e("search range", "" + progressChanged);
			break;

		// case R.id.SeekBarForMinAge:
		// progressChangedMinAge = progress;
		// if (progressChangedMinAge < 18)
		// progressChangedMinAge = 18;
		// txtForShowsetAge.setText(progressChangedMinAge + " - "
		// + progressChangedMaxAge);
		// break;
		//
		// case R.id.SeekBarForMaxAge:
		// progressChangedMaxAge = progress;
		// if (progressChangedMaxAge < progressChangedMinAge)
		// progressChangedMaxAge = progressChangedMinAge + 1;
		// txtForShowsetAge.setText(progressChangedMinAge + " - "
		// + progressChangedMaxAge);
		// break;

		default:
			break;
		}

	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {

		switch (seekBar.getId()) {
		case R.id.seekBarForRange:
			updateSearchRange();
			break;

		default:
			break;
		}

	}

	@Override
	public void response(String response, int pageid) {
		System.out.println("respnse hereee= " + response);

		if (pageid == 1) {
			if (pdialog.isShowing()) {
				pdialog.dismiss();

			}
			try {
				JSONObject jsonResponse = new JSONObject(response);

				JSONArray jsonMainNode = jsonResponse.optJSONArray("list");

				if (jsonMainNode.length() == 0) {
					Log.e("no data", "no data");
				} else {

					for (int i = 0; i < jsonMainNode.length(); i++) {
						JSONObject jsonChildNode = jsonMainNode
								.getJSONObject(i);
						get_intrestedIn = jsonChildNode
								.optString("interested_in");
						getAgeFrom = jsonChildNode.optInt("age_from");
						getAgeTo = jsonChildNode.optInt("age_to");
						getRange = jsonChildNode.optInt("range");
						getVisibility = jsonChildNode.optString("visibility");
					}

					if (get_intrestedIn.equals("male")
							|| get_intrestedIn.equals("Male")) {
						tickMale.setVisibility(View.VISIBLE);
						txtForShowIntrested.setText(get_intrestedIn);
						txtformale.setTextColor(green);
						maleSelected = true;
					} else if (get_intrestedIn.equals("female")
							|| get_intrestedIn.equals("Female")) {
						tickFemale.setVisibility(View.VISIBLE);
						txtForShowIntrested.setText(get_intrestedIn);
						txtforfemale.setTextColor(green);
						femaleSelected = true;
					} else if (get_intrestedIn.equals("both")
							|| get_intrestedIn.equals("Both")) {
						tickFemale.setVisibility(View.VISIBLE);
						tickMale.setVisibility(View.VISIBLE);
						txtForShowIntrested.setText(get_intrestedIn);
						txtforfemale.setTextColor(green);
						txtformale.setTextColor(green);
						femaleSelected = true;
						maleSelected = true;
					}

					if (getVisibility.equals("0")) {
						txtForShowVisibility.setText("OFF");
						toggle_ButtonNew.setChecked(false);
						txtsetVisibility.setTextColor(Color.BLACK);
					} else if (getVisibility.equals("1")) {
						txtForShowVisibility.setText("ON");
						txtsetVisibility.setTextColor(green);
						toggle_ButtonNew.setChecked(true);
					}
					search_range.setProgress(getRange);

					// RangeSeekBar.setLeft(getAgeFrom);
					seekBar.setSelectedMinValue(getAgeFrom);
					seekBar.setSelectedMaxValue(getAgeTo);
					minAge.setText("From: " + getAgeFrom);
					maxAge.setText("To: " + getAgeTo);

					spinner1.setProgress(getAgeFrom);
					spinner2.setProgress(getAgeTo);
					spinner1.setMax(getAgeTo);

					txtforFrom.setText("" + getAgeFrom);
					txtForTo.setText("" + getAgeTo);
					txtForShowRange.setText("" + getRange + " Km");
					count++;
					Log.d("apivaluessss", "first range" + getRange
							+ " get age from " + getAgeFrom + " get age to "
							+ getAgeTo);
				}

			} catch (JSONException e) {
				if (pdialog.isShowing()) {
					pdialog.dismiss();

				}
				e.printStackTrace();
			}
		}

		if (pageid == 2) {
			if (pdialog.isShowing()) {
				pdialog.dismiss();
			}
			try {

				JSONObject jsonResponse = new JSONObject(response);
				JSONObject response2 = jsonResponse.getJSONObject("Response");
				System.out.println("response = " + response2);
				JSONObject response3 = response2.getJSONObject("returnCode");
				String success = response3.optString("resultText");

				// JSONObject response = obj.getJSONObject("Response");
				// // get_uid = response.getString("userid");
				// System.out.println("response = " + response);
				// JSONObject obj2 = response.getJSONObject("returnCode");

				Log.e("responce string", "" + success);
				if (success.equals("Success")) {
					Toast.makeText(getApplicationContext(), "Save successfuly",
							5000).show();
				}

			} catch (Exception e) {
				if (pdialog.isShowing()) {
					pdialog.dismiss();

				}
				e.printStackTrace();
			}

		}

		if (pageid == 3) {
			if (pdialog.isShowing()) {
				pdialog.dismiss();
			}
			try {

				JSONObject jsonResponse = new JSONObject(response);
				JSONObject response2 = jsonResponse.getJSONObject("Response");
				System.out.println("response = " + response2);
				JSONObject response3 = response2.getJSONObject("returnCode");
				String success = response3.optString("resultText");

				// JSONObject response = obj.getJSONObject("Response");
				// // get_uid = response.getString("userid");
				// System.out.println("response = " + response);
				// JSONObject obj2 = response.getJSONObject("returnCode");

				Log.e("responce string", "" + success);
				if (success.equals("Success")) {
					Toast.makeText(getApplicationContext(), "Save successfuly",
							5000).show();
				}

			} catch (Exception e) {
				if (pdialog.isShowing()) {
					pdialog.dismiss();

				}
				e.printStackTrace();
			}

		}

		if (pageid == 4) {
			if (pdialog.isShowing()) {
				pdialog.dismiss();
			}
			try {

				JSONObject jsonResponse = new JSONObject(response);
				JSONObject response2 = jsonResponse.getJSONObject("Response");
				System.out.println("response = " + response2);
				JSONObject response3 = response2.getJSONObject("returnCode");
				String success = response3.optString("resultText");

				// JSONObject response = obj.getJSONObject("Response");
				// // get_uid = response.getString("userid");
				// System.out.println("response = " + response);
				// JSONObject obj2 = response.getJSONObject("returnCode");

				Log.e("responce string", "" + success);
				if (success.equals("Success")) {
					Toast.makeText(getApplicationContext(), "Save successfuly",
							5000).show();
				}

			} catch (Exception e) {
				if (pdialog.isShowing()) {
					pdialog.dismiss();

				}
				e.printStackTrace();
			}

		}

		if (pageid == 5) {
			if (pdialog.isShowing()) {
				pdialog.dismiss();
			}
			try {

				JSONObject jsonResponse = new JSONObject(response);
				JSONObject response2 = jsonResponse.getJSONObject("Response");
				System.out.println("response = " + response2);
				JSONObject response3 = response2.getJSONObject("returnCode");
				String success = response3.optString("resultText");

				// JSONObject response = obj.getJSONObject("Response");
				// // get_uid = response.getString("userid");
				// System.out.println("response = " + response);
				// JSONObject obj2 = response.getJSONObject("returnCode");

				Log.e("responce string", "" + success);

				if (success.equals("Success")) {
					Toast.makeText(getApplicationContext(), "Save successfuly",
							Toast.LENGTH_SHORT).show();

				}

			} catch (Exception e) {
				if (pdialog.isShowing()) {
					pdialog.dismiss();

				}
				e.printStackTrace();
			}

		}
	}

	@Override
	public void error(String error) {
		// TODO Auto-generated method stub

	}

	@SuppressWarnings("deprecation")
	public void updateIntrestesIn() {
		// hit Admin Question Web API
		statusis = new ConnectionStatus(this);
		pdialog = new TransparentProgressDialog(this, R.drawable.loader);
		pdialog.show();
		isConnected = statusis.isConnected();
		if (isConnected) {
			try {
				new HttpGetRespone(setIntrestedinURL + userId
						+ "&interested_in=" + intrestedIn, this, 2).execute();
				Log.e("intrested url>>>>>>>>>>>>>>>>>>>>", ""
						+ setIntrestedinURL + userId + "&interested_in="
						+ intrestedIn);

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {

			if (pdialog.isShowing())
				pdialog.dismiss();
			alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle("Peapod");
			alertDialog.setMessage("No Internet Connection Available");
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			alertDialog.show();
		}
	}

	@SuppressWarnings("deprecation")
	public void updateVisibilitySetting() {
		// hit Admin Question Web API
		statusis = new ConnectionStatus(this);
		pdialog = new TransparentProgressDialog(this, R.drawable.loader);
		pdialog.show();
		isConnected = statusis.isConnected();
		if (isConnected) {
			try {
				new HttpGetRespone(setVisibilityURL + userId + "&visibility="
						+ visibilityStatus, this, 3).execute();
				Log.e("intrested >>>>>>>>>>>>>>>>>>>>", "" + visibilityStatus);

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {

			if (pdialog.isShowing())
				pdialog.dismiss();
			alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle("Peapod");
			alertDialog.setMessage("No Internet Connection Available");
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			alertDialog.show();
		}
	}

	@SuppressWarnings("deprecation")
	public void updateSearchRange() {
		// hit Admin Question Web API
		statusis = new ConnectionStatus(this);
		pdialog = new TransparentProgressDialog(this, R.drawable.loader);
		pdialog.show();
		isConnected = statusis.isConnected();
		if (isConnected) {
			try {
				new HttpGetRespone(setSearchRangeURL + userId + "&range="
						+ progressChanged, this, 4).execute();
				Log.e("intrested >>>>>>>>>>>>>>>>>>>>", "" + progressChanged);

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {

			if (pdialog.isShowing())
				pdialog.dismiss();
			alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle("Peapod");
			alertDialog.setMessage("No Internet Connection Available");
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			alertDialog.show();
		}
	}

	@SuppressWarnings("deprecation")
	public void updateAgeRange() {
		// hit Admin Question Web API
		statusis = new ConnectionStatus(this);
		pdialog = new TransparentProgressDialog(this, R.drawable.loader);
		pdialog.show();
		isConnected = statusis.isConnected();
		if (isConnected) {
			try {
				new HttpGetRespone(setAgeRangeURL + userId + "&age_from="
						+ progressChangedMinAge + "&age_to="
						+ progressChangedMaxAge, this, 5).execute();
				Log.e("intrested >>>>>>>>>>>>>>>>>>>>", "" + item
						+ "------------------" + item2);
				Log.d("apiseekkkkkkkkkkkkkkkkkbar", setAgeRangeURL + userId
						+ "&age_from=" + progressChangedMinAge + "&age_to="
						+ progressChangedMaxAge);

			} catch (Exception e) {
				e.printStackTrace();
			}
			if (pdialog.isShowing()) {
				pdialog.dismiss();
			}
		} else {

			if (pdialog.isShowing())
				pdialog.dismiss();
			alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle("Peapod");
			alertDialog.setMessage("No Internet Connection Available");
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			alertDialog.show();
		}

	}

	@Override
	public void onBackPressed() {

	}

}