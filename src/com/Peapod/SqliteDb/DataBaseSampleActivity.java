package com.Peapod.SqliteDb;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

public class DataBaseSampleActivity {

	/** for database */
	static final String DataBaseName = "PeapodDB";

	/** for employee table */
	static final String PeapodChatTable = "PeapodChatTable";

	/** for employee Column */
	static final String ColID = "Id";
	static final String ColSenderId = "SenderId";
	static final String ColRecieveId = "RecieveId";
	static final String ColMessage = "Message";
	static final String ColCreated = "Created";

	public static final int DATABASE_VERSION = 2;

	// private static final String KEY_ROWID = "_id";

	private static final String PEAPOD_CHAT_TABLE_CREATE = "Create table "
			+ PeapodChatTable + "(" + ColID + " INT(100) ," + ColSenderId
			+ " VARCHAR(100) ," + ColRecieveId + " INT(100) ," + ColMessage
			+ " VARCHAR(255) , " + ColCreated + " VARCHAR(50))";

	private final Context context;
	private DatabaseHelper DBHelper;
	private SQLiteDatabase db;

	public DataBaseSampleActivity(Context ctx) {
		Log.i("test****", "**test***");
		this.context = ctx;
		DBHelper = new DatabaseHelper(context);
	}

	private static class DatabaseHelper extends SQLiteOpenHelper {
		public DatabaseHelper(Context context) {
			super(context, DataBaseName, null, DATABASE_VERSION);
			Log.i("context", "context");
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			db.execSQL(PEAPOD_CHAT_TABLE_CREATE);
			Log.e("********on create result****", "table created");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			Log.w("tag", "Upgrading database from version " + oldVersion
					+ " to " + newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS " + PeapodChatTable);
			onCreate(db);
		}

	};

	public DataBaseSampleActivity open() throws SQLException {
		db = DBHelper.getWritableDatabase();
		Log.i("open", "message");
		return this;
	}

	public void close() {
		DBHelper.close();
	}

	// public long insert(Integer empid, String empname, Integer empage, String
	// empdept) {
	public long insert(Integer id, String sender_id, Integer recv_id,
			String msg, String date) {
		Log.i("**** suruchitest **** ", "*** test ***");
		ContentValues initialValues = new ContentValues();

		// initialValues.put(ColEmpID, empid);
		initialValues.put(ColID, id);
		initialValues.put(ColSenderId, sender_id);
		initialValues.put(ColRecieveId, recv_id);
		initialValues.put(ColMessage, msg);
		initialValues.put(ColCreated, date);
		Log.e("data inserted ", "data inserted");

		return db.insert(PeapodChatTable, null, initialValues);
	}

	public Cursor getChatMessages(String userid, String recv_id) {
		// String qua = "select * from " + PeapodChatTable + " where "
		// + ColSenderId + "=? and " + ColRecieveId + "=?";
		String qua = "select *  from " + PeapodChatTable + " where (("
				+ ColSenderId + "=" + userid + " and " + ColRecieveId + "="
				+ recv_id + ") or (" + ColSenderId + "=" + recv_id + " and "
				+ ColRecieveId + "=" + userid + "))";
		Cursor mCursor = db.rawQuery(qua, null);
		// Cursor mCursor = db.query(PeapodChatTable, null, null, null, null,
		// null, null);
		return mCursor;
	}

	public boolean deleteEmpList(String frndID) {
		Toast.makeText(context, "deleted", 2000).show();
		Log.e("deleted", "deleted");
		return db.delete(PeapodChatTable, ColRecieveId + " = " + frndID, null) > 0;
	}

	// public boolean updateEmplist(String empname, Integer rowid) {
	//
	// ContentValues initialValues = new ContentValues();
	// // Log.i("#####  "+rowid,""+empname+" "+empage+" "+empdept);
	//
	// // initialValues.put(ColEmpID, rowid);
	// initialValues.put(ColEmpName, empname);
	//
	// try {
	// int b = db.update(EmployeTable, initialValues, ColEmpID + " = "
	// + rowid, null);
	// Log.i("update", "up " + rowid + "  ddd   " + b);
	// return true;
	// } catch (Exception e) {
	// Log.d("asdfasdfsadfasdf", "_--___--__--_=-_");
	// return false;
	// }
	// }
}