package com.Peapod.Connections;

import android.content.Context;
import android.content.SharedPreferences;

public class StorePref {
	public static final String UserID = "user_id";
	public static final String Check = "check";
	public static final String n_status = "n_statuss";
	public static final String Image_url = "img_url";
	public static final String User_name = "user_name";
	public static final String User_age = "user_age";
	public static final String User_Bio = "user_bio";

	public static final String My_lat = "my_lat";
	public static final String My_long = "my_long";

	public static final String MessageLegnth = "message_length";

	// public static final String Bench_Mark = "bench_mark";

	private static StorePref preferences = null;

	private SharedPreferences pref = null;

	public StorePref(Context context) {
		pref = context
				.getSharedPreferences("e_koll_pref", Context.MODE_PRIVATE);
	}

	public static StorePref getInstance(Context context) {
		if (preferences == null) {
			preferences = new StorePref(context);
		}
		return preferences;
	}

	public void setUserid(String user_id) {
		pref.edit().putString(UserID, user_id).commit();
	}

	public String getUserid() {
		return pref.getString(UserID, "");
	}

	public void setUserAge(String user_age) {
		pref.edit().putString(User_age, user_age).commit();
	}

	public String getUserAge() {
		return pref.getString(User_age, "");
	}

	public void setUserBio(String user_bio) {
		pref.edit().putString(User_Bio, user_bio).commit();
	}

	public String getUserBio() {
		return pref.getString(User_Bio, "");
	}

	public void setMyLat(float lat) {
		pref.edit().putFloat(My_lat, lat).commit();
	}

	public double getMyLat() {
		return pref.getFloat(My_lat, 0);
	}

	public void setMyLong(float longi) {
		pref.edit().putFloat(My_long, longi).commit();
	}

	public double getMyLong() {
		return pref.getFloat(My_long, 0);
	}

	// Store facebook access token
	public void setUserName(String user_name) {
		pref.edit().putString(User_name, user_name).commit();
	}

	public String getUserName() {
		return pref.getString(User_name, "");
	}

	//
	// for store image url
	public void setImageUrl(String img_url) {
		pref.edit().putString(Image_url, img_url).commit();
	}

	public String getImageUrl() {
		return pref.getString(Image_url, "");
	}

	public void setMessageLenght(int length) {
		pref.edit().putInt(MessageLegnth, length).commit();
	}

	public int getMessageLength() {
		return pref.getInt(MessageLegnth, 0);
	}

}
