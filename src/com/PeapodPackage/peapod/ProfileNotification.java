package com.PeapodPackage.peapod;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONObject;

import se.tonyivanov.android.ajax.Request;
import se.tonyivanov.android.ajax.Transport;

import com.Peapod.Connections.ImageLoader;
import com.Peapod.Connections.StorePref;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

public class ProfileNotification extends Activity implements OnClickListener {

	private ImageButton doneBtn;
	private ImageView pea_image, view_profile_img;
	private ImageView firstImg, secondImg, thirdImg, forthImg;
	String image, name, userID, bio;
	ImageLoader imgLoader;
	double dist;
	String activeTime;
	TextView nameTxt;
	Boolean status = false;
	TextView txtForResult;
	Boolean imgStatus = false;
	TextView txtForActiveTime, txtForBio, txtForDistance;
	StorePref pref;
	String friendId;
	int correctAns = 0;
	private ArrayList<String> listForOPtion1 = new ArrayList<String>();
	private ArrayList<String> listForOPtion2 = new ArrayList<String>();
	private ArrayList<String> listForAnswerStatus = new ArrayList<String>();
	private ArrayList<String> listForAnswer = new ArrayList<String>();

	private ArrayList<String> listForImagesFour = new ArrayList<String>();

	private ArrayList<String> listForUserImages = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profile_notification);

		doneBtn = (ImageButton) findViewById(R.id.done_btn);
		doneBtn.setOnClickListener(this);
		view_profile_img = (ImageView) findViewById(R.id.view_profile_img);
		view_profile_img.setOnClickListener(this);

		pref = StorePref.getInstance(getApplicationContext());
		userID = pref.getUserid();
		// friendId = getIntent().getStringExtra("keyForFriendId");
		nameTxt = (TextView) findViewById(R.id.txt1);
		txtForActiveTime = (TextView) findViewById(R.id.txtForActiveTime);
		txtForBio = (TextView) findViewById(R.id.txtForBio);
		txtForDistance = (TextView) findViewById(R.id.txtDistance);
		imgLoader = new ImageLoader(getApplicationContext());
		pea_image = (ImageView) findViewById(R.id.pea_image);
		firstImg = (ImageView) findViewById(R.id.firstImage);
		secondImg = (ImageView) findViewById(R.id.secondImage);
		thirdImg = (ImageView) findViewById(R.id.thirdImage);
		forthImg = (ImageView) findViewById(R.id.forthImage);
		friendId = getIntent().getStringExtra("keyForFrndId");

		txtForDistance.setText("" + dist + " miles");
		txtForActiveTime.setText("In the pod " + activeTime);
		getProfile();
		getResult();

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.done_btn:
			finish();
			break;

		case R.id.view_profile_img:
			initiatePopupWindow2();
			break;

		case R.id.view_profile_imgpop:
			pwindo2.dismiss();
			break;

		default:
			break;
		}
	}

	private PopupWindow pwindo2;

	private void initiatePopupWindow2() {
		try {
			// We need to get the instance of the LayoutInflater
			LayoutInflater inflater = (LayoutInflater) ProfileNotification.this
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View layout = inflater.inflate(R.layout.view_result_popup,
					(ViewGroup) findViewById(R.id.popup));
			// layout.getBackground().setAlpha(210);
			DisplayMetrics metrics = this.getResources().getDisplayMetrics();
			int width = metrics.widthPixels;
			int height = metrics.heightPixels;
			pwindo2 = new PopupWindow(layout, width, height, true);
			pwindo2.showAtLocation(layout, Gravity.CENTER_HORIZONTAL, 0, 0);

			txtForResult = (TextView) layout.findViewById(R.id.txtForResult);

			ImageView imgforclose = (ImageView) layout
					.findViewById(R.id.view_profile_imgpop);
			imgforclose.setOnClickListener(this);

			ListView resultList = (ListView) layout
					.findViewById(R.id.listForResult);

			resultList.setAdapter(new ResultListAdapter(this));
			txtForResult.setText("" + correctAns + "/" + listForAnswer.size());
			// final HttpResponseListener listner = null;

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public class ResultListAdapter extends BaseAdapter {

		Activity activity;
		LayoutInflater inflater;

		public ResultListAdapter(Activity a) {
			activity = a;
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listForOPtion1.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View vi = convertView;
			if (convertView == null)
				vi = inflater.inflate(R.layout.quiz_manager_list_layout, null);

			TextView textForCount = (TextView) vi
					.findViewById(R.id.txtForSerialNo);

			final EditText textForOption1 = (EditText) vi
					.findViewById(R.id.txtForOption1);
			final EditText textForOption2 = (EditText) vi
					.findViewById(R.id.txtForOption2);

			ImageView chkBox = (ImageView) vi.findViewById(R.id.imgForCheckbox);
			ImageView chkBox2 = (ImageView) vi
					.findViewById(R.id.imgForCheckbox2);
			final ImageView chk = (ImageView) vi.findViewById(R.id.imgForCheck);
			final ImageView chk2 = (ImageView) vi
					.findViewById(R.id.imgForCheck2);
			chkBox.setVisibility(View.GONE);
			chkBox2.setVisibility(View.GONE);
			chk.setVisibility(View.GONE);
			chk2.setVisibility(View.GONE);
			textForOption1.setEnabled(false);
			textForOption2.setEnabled(false);

			if (listForAnswer.get(position)
					.equals(listForOPtion1.get(position))) {
				if (listForAnswerStatus.get(position).equals("correct")) {
					chk.setVisibility(View.VISIBLE);
					chk2.setVisibility(View.GONE);
					chk.setImageResource(R.drawable.right);
				} else {
					chk.setVisibility(View.VISIBLE);
					chk2.setVisibility(View.GONE);
					chk.setImageResource(R.drawable.close);
				}
			} else {
				if (listForAnswerStatus.get(position).equals("wrong")) {

					chk.setVisibility(View.GONE);
					chk2.setVisibility(View.VISIBLE);
					chk2.setImageResource(R.drawable.close);
				} else {
					chk.setVisibility(View.GONE);
					chk2.setVisibility(View.VISIBLE);
					chk2.setImageResource(R.drawable.right);
				}

			}
			textForOption1.setText(listForOPtion1.get(position));
			textForOption2.setText(listForOPtion2.get(position));
			textForCount.setText("" + (position + 1) + ". ");

			return vi;
		}

	}

	public void getResult() {

		String url = "http://peapodapp.com/webservices/androidapi.php?type=listquestion_results&userid="
				+ userID + "&freindid=" + friendId;
		System.out.println("url,,,," + url);
		Request result = (Request) new Request(url) {

			@Override
			protected void onSuccess(Transport transport) {

				try {
					JSONObject response = transport.getResponseJson();
					System.out.println("response here" + response);
					if (response == null)
						Toast.makeText(getApplicationContext(),
								"No response from server", Toast.LENGTH_LONG)
								.show();
					else {
						listForAnswer.clear();
						listForAnswerStatus.clear();
						listForOPtion1.clear();
						listForOPtion2.clear();
						JSONArray getArray = response.getJSONArray("list");

						for (int i = 0; i < getArray.length(); i++) {
							JSONObject data = getArray.optJSONObject(i);

							String option1 = data.optString("option1");
							String option2 = data.optString("option2");
							String answerStaus = data
									.optString("answer_status");
							String answer = data.optString("answer");

							if (answerStaus.equals("correct")) {
								correctAns++;
							}

							listForAnswerStatus.add(answerStaus);
							listForAnswer.add(answer);
							listForOPtion1.add(option1);
							listForOPtion2.add(option2);

						}

					}

				} catch (Exception e) {
					// TODO: handle exception
				}

			}

		}.execute("GET");
	}

	public void getProfile() {
		String url = "http://www.peapodapp.com/webservices/androidapi.php?type=getprofile&userid="
				+ friendId;
		System.out.println("url,,,," + url);
		Request result = (Request) new Request(url) {

			@Override
			protected void onSuccess(Transport transport) {

				try {
					JSONObject response = transport.getResponseJson();
					System.out.println("response here" + response);
					if (response == null)
						Toast.makeText(getApplicationContext(),
								"No response from server", Toast.LENGTH_LONG)
								.show();
					else {
						JSONArray getArray = response.getJSONArray("list");

						for (int i = 0; i < getArray.length(); i++) {
							JSONObject data = getArray.optJSONObject(i);

							String name = data.optString("fullname");
							String profilePic = data.optString("profilepic");
							String bio = data.optString("bio");
							String createdDate = data.optString("created");
							String currentDate = data.optString("displaydate");

							SimpleDateFormat formatter = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss");
							Date dateForcreatedTime = formatter
									.parse(createdDate);
							Date dateForCurrentDate = formatter
									.parse(currentDate);

							Log.e("difference of two dates:-   ",
									"  difference :--  "
											+ getTimeDiff(dateForCurrentDate,
													dateForcreatedTime)
											+ "currernt:=  " + currentDate
											+ "  created:- " + createdDate);

							String di = getTimeDiff(dateForCurrentDate,
									dateForcreatedTime);

							String[] activTimeArray = di.split(",");

							Integer hours = Integer.parseInt(activTimeArray[0]);
							Integer mins = Integer.parseInt(activTimeArray[1]);

							// if (hours == 0) {
							// txtForActiveTime.setText("In the pod " + mins
							// + " mins ago");
							// } else if (hours > 23) {
							// hours = hours / 24;
							// txtForActiveTime.setText("In the pod " + hours
							// + " days ago");
							// } else {
							// txtForActiveTime.setText("In the pod " + hours
							// + " hours ago");
							// }

							if (hours == 0) {
								txtForActiveTime.setText("In the pod " + mins
										+ " mins ago");
							} else if (hours >= 24 && hours < 168) {
								int days = hours / 24;
								txtForActiveTime.setText("In the pod " + days
										+ " days ago");
							} else if (hours >= 168 && hours < 720) {
								int week = hours / 168;
								txtForActiveTime.setText("In the pod " + week
										+ " week ago");
							} else {
								txtForActiveTime.setText("In the pod " + hours
										+ " hours ago");
							}

							Log.d("", " hours:-  " + hours + "   mins:- "
									+ mins);

							double lat = data.optDouble("lat");
							double longt = data.optDouble("longt");

							double myLat = pref.getMyLat();
							double myLong = pref.getMyLong();

							txtForDistance.setText(" "
									+ distance(myLat, myLong, lat, longt, 'k')
									+ " miles");

							String str = name;

							int index = str.indexOf(" ");

							if (index != -1) {
								str = str.substring(0, index);
							}

							nameTxt.setText(str);
							txtForBio.setText("Bio: " + bio);
							imgLoader.DisplayImage(profilePic, pea_image);
							JSONArray getImagesArray = data

							.getJSONArray("images");
							listForImagesFour.clear();
							for (int j = 0; j < getImagesArray.length(); j++) {

								JSONObject img = getImagesArray
										.optJSONObject(j);
								String imgUrl = img.optString("imageurl");
								listForImagesFour.add(imgUrl);

							}

							imgLoader.DisplayImage(listForImagesFour.get(0),
									firstImg);
							imgLoader.DisplayImage(listForImagesFour.get(1),
									secondImg);
							imgLoader.DisplayImage(listForImagesFour.get(2),
									thirdImg);
							imgLoader.DisplayImage(listForImagesFour.get(3),
									forthImg);

						}

					}

				} catch (Exception e) {
					// TODO: handle exception
				}

			}

		}.execute("GET");

	}

	private double distance(double lat1, double lon1, double lat2, double lon2,
			char unit) {
		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2))
				+ Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2))
				* Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		if (unit == 'K') {
			dist = dist * 1.609344;
		} else if (unit == 'N') {
			dist = dist * 0.8684;
		}
		dist = dist * 0.621371;
		dist = Math.round(dist * 100.0) / 100.0;
		return (dist);
	}

	private double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
	/* :: This function converts radians to decimal degrees : */
	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
	private double rad2deg(double rad) {
		return (rad * 180 / Math.PI);
	}

	public String getTimeDiff(Date dateOne, Date dateTwo) {
		String diff;
		long timeDiff = Math.abs(dateOne.getTime() - dateTwo.getTime());
		diff = String.format(
				"%d,%d",
				TimeUnit.MILLISECONDS.toHours(timeDiff),
				TimeUnit.MILLISECONDS.toMinutes(timeDiff)
						- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
								.toHours(timeDiff)));
		return diff;
	}
}
